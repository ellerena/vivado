/******************************************************************************
*
* Copyright (C) 2019 - 2020 E.Llerena, Inc.  All rights reserved.
*
******************************************************************************/

#include <stdio.h>
#include <sleep.h>
#include "InfoModule.h"
#include "time_interval.h"

#include <os_i2c.h>
#include "command_handlers.h"
#include "command_handlers.h"
#include "gpio.h"
#include "spi.h"
#include "spisnif.h"
#include "uart.h"

#define CRLF               "\n"
#define SW_VER             "0.0.0 "
#define TST_BYTE_SZ        (16) //*1024)               /* CDMA transfer size (BTT) */

extern int SetupInterruptSystem(void);

volatile unsigned int gFlags = 0;

void enable_caches()
{
}

void disable_caches()
{
}

void init_platform()
{
    /*
     * If you want to run this example outside of SDK,
     * uncomment one of the following two lines and also #include "ps7_init.h"
     * or #include "ps7_init.h" at the top, depending on the target.
     * Make sure that the ps7/psu_init.c and ps7/psu_init.h files are included
     * along with this example source files for compilation.
     */
    /* ps7_init();*/
    /* psu_init();*/
    enable_caches();
}

void cleanup_platform()
{
    disable_caches();
}

int main()
{
   u32 i;
   char s[] = "\nCora Z7 v. "SW_VER __DATE__ " " __TIME__ CRLF;

   init_platform();
   gpio_init();
   TSTPRNS(uart_Initialize(), "UArt");
   TSTPRNS(SetupInterruptSystem(), "IntC");
   TSTPRNS(uart_IrqInit(), "UartInt");
   usleep(500);
   TSTPRNS(spi_Initialize(), "Spi");

   OS_i2c_master_init();

   PRNF("%s\n", s);
   PRNF("HW build %08x: %08x %08x" CRLF ">", BR_IN32(REG_INFO_HASH), BR_IN32(REG_INFO_DATE), BR_IN32(REG_INFO_TIME));

   BR_OUT32(REG_1_TIM, 0x80);
   while (1)
   {
      if (gFlags) {
         if (GFL(F_UART)) {
            CommandProcessor((char*)acCommand);
            GFLC(F_UART);
            PRNF("%s\n>", acCommand);
         }
         if (GFL(F_PB)) {
            if(PB_0_ON) {
               gpio_ledrotate(LED0);
            }

            if(PB_1_ON) {
               gpio_ledrotate(LED1);
            }

            debounce(i, PB_ANY_ON);
            GFLC(F_PB);
         }
      }
   }

   cleanup_platform();
   return 0;
}

