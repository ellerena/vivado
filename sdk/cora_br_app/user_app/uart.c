/*
 * uart.c
 *
 *  Created on: Mar 19, 2020
 *      Author: eddie
 */

#include "uart.h"

#ifdef FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#endif

extern volatile unsigned int gFlags;

XUartPs Uart0;

u8 acCommand[BUFFER_SIZE];                   /* buffer used to move commands and arguments around */
u8 *acArgs = &acCommand[POINTER_TO_ARGS];    /* pointer to the arguments section in the buffer */

void uart_irqHandler(void *data)
{
    char a;
    static u8 *buffer = acCommand;

    a = BR_IN32(PSUART_REG_RX);      /* read data form UART buffer */
    *buffer++ = a;                   /* append rcvd char to buffer in ram */

    if (a == '\r')                   /* <return> ends the uart command entry */
    {
        buffer = acCommand;          /* return pointer to begining of buffer */
        gFlags |= F_UART;            /* rise a flag to tell main() */
#ifdef FREERTOS
    extern TaskHandle_t htask_mainproc;
    portYIELD_FROM_ISR(xTaskResumeFromISR(htask_mainproc));
#endif
    }

    BR_OUT32(PSUART_REG_ISR, BR_IN32(PSUART_REG_ISR));
}

int uart_Initialize(void)
{
    XUartPs_Config *UartCnf;
    int stat;

    UartCnf = XUartPs_LookupConfig(XPAR_XUARTPS_0_DEVICE_ID);
    TSTRLN(NULL == UartCnf);
    stat = XUartPs_CfgInitialize(&Uart0, UartCnf, UartCnf->BaseAddress);
    TSTRLN(stat);

    return 0;
}

int uart_IrqInit(void)
{
    XUartPs_SetFifoThreshold(&Uart0, 1u);
    XUartPs_SetHandler(&Uart0, (XUartPs_Handler)uart_irqHandler, NULL);
    XUartPs_SetInterruptMask(&Uart0, XUARTPS_IXR_RXOVR);
    XUartPs_SetOperMode(&Uart0, XUARTPS_OPER_MODE_NORMAL);

    return 0;
}

