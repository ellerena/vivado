/*
by Eddie Llerena
March - 2012

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */

/***** include here your font specific header *****/
#include "fnt_5x7.h"
/*******    end of font  specific header    *******/
/****** include your display specific header ******/
#include "SSD1306.h"
/*******   end of display specific header   *******/

#include "disp_txt_library.h"
// TODO #include "ntp.h"           /* required to display current date/time */

int16_t curx = 0;           /* current x position */
int16_t cury = 0;           /* current y position */
uint16_t txthue = WHITE;    /* text hue color */
uint16_t txtbgd = BLACK;    /* text background color */
uint8_t txtsz = 1;          /* text magnification */

/* font table (256 chars max) */
static const uint8_t font[][5] = {FONT_MAP_COMPLETE};

void display_Init(void)
{
   /* direct execution commands */
   SSD1306_Init();
   display_invert(false);

   display_setTextSize(1);
   display_setTextColor(WHITE, BLACK);

   /* show current date/time */
   display_locate(3, 46);
//TODO   update_time_str(OLED_TIME);
   display_puts(__DATE__ " " __TIME__);

   display();
   display_clear();
}

/**
   @brief  Print one byte/character to display
   @param  c  The 8-bit ascii character to write
*/
void display_putc(uint8_t c)
{
   /* for '\n' do a CRLF */
   if (c == '\n') {
      curx = 0;
      cury += txtsz * FNT_H;
      return;
   }

#ifdef PRINTABLE_FONTS_ONLY
   c -= FONT_MAP_PRINTABLE_OFFSET;
#endif

#ifdef TRIMLEADSPACES
   /* when wrapping line, discard first space */
   if (c == ' ' && curx == 0)
      return;
#endif

   /* build character from font table */
   for(uint8_t i = 0; i < FNT_W_CHAR; i++ ) { /* build column by column */

      uint8_t line = font[c][i];              /* each byte represents one pixel column */

      for(uint8_t j = 0; j < FNT_H; j++, line >>= 1) {

         if(line & 1) {                /* if pixel is foreground, set it */

            if(txtsz == 1)
               display_drawPixel(curx + i, cury + j, txthue);
            else
               display_fillRect(curx + i * txtsz, cury + j * txtsz, txtsz, txtsz, txthue);

         }
         else if(txtbgd != txthue) { /* if pixel is background, clear it */

            if(txtsz == 1)
               display_drawPixel(curx + i, cury + j, txtbgd);
            else
               display_fillRect(curx + i * txtsz, cury + j * txtsz, txtsz, txtsz, txtbgd);

         }
      }
   }

#if 0    /* this code may not be needed if we clear the screen regularly */
   /* we may need to clear the last column to show separation between characters */
   if(txtbgd != txthue) {
     if(txtsz == 1)
        display_drawVLine(curx + 5, cury, 8, txtbgd);
     else
        display_fillRect(curx + 5 * txtsz, cury, txtsz, 8 * txtsz, txtbgd);
   }
#endif

   /* advance to next char position in this line */
   curx += txtsz * FNT_W;

   /* chars outside width limit are kept right */
   if( curx > ((uint16_t)disp_w + txtsz * FNT_W) )
      curx = disp_w;

#ifdef DISPTXTWRAP
   /* if line is already full, do a CR and LF */
   if ((curx + (txtsz * FNT_W_CHAR)) > disp_w)
   {
      curx = 0;                        /* carriage return */
      cury += txtsz * FNT_H;           /* line feed */
      if( cury > ((uint16_t)disp_h + txtsz * FNT_H) )
         cury = disp_h;
   }
#endif
}

/**
 * @brief   print custom char (dimension: 7x5 or 8x5 pixel)
 *          (assume character will fit
 * */
void display_customChar(const uint8_t *c)
{
   for(uint8_t i = 0; i < FNT_W_CHAR; i++ ) {   /* build column by column */

      uint8_t line = *c++;                      /* each byte represents one pixel column */
      for(uint8_t j = 0; j < FNT_H; j++, line >>= 1) {

         if(line & 1) {              /* if pixel is foreground, set it */
            if(txtsz == 1)
               display_drawPixel(curx + i, cury + j, txthue);
            else
               display_fillRect(curx + i * txtsz, cury + j * txtsz, txtsz, txtsz, txthue);
         }
         else if(txtbgd != txthue) { /* if pixel is background, clear it */
            if(txtsz == 1)
               display_drawPixel(curx + i, cury + j, txtbgd);
            else
               display_fillRect(curx + i * txtsz, cury + j * txtsz, txtsz, txtsz, txtbgd);
         }
      }
   }

   /* we may need to clear the last column to show separation between characters */
   if(txtbgd != txthue) {
      if(txtsz == 1)
         display_drawVLine(curx + FNT_W_CHAR, cury, FNT_H, txtbgd);
      else
         display_fillRect(curx + FNT_W_CHAR * txtsz, cury, txtsz, FNT_H * txtsz, txtbgd);
   }

   /* advance to next char position in this line */
   curx += txtsz * FNT_W;

   /* chars outside width limit are kept right */
   if( curx > ((uint16_t)disp_w + txtsz * FNT_W) )
      curx = disp_w;

#ifdef DISPTXTWRAP
   /* if line is already full, do a CR and LF */
   if ((curx + (txtsz * FNT_W_CHAR)) > disp_w)
   {
      curx = 0;                        /* carriage return */
      cury += txtsz * FNT_H;           /* line feed */
      if( cury > ((uint16_t)disp_h + txtsz * FNT_H) )
         cury = disp_h;
   }
#endif
}

/*!
   @brief   Draw a single character
   @param    x   Bottom left corner x coordinate
   @param    y   Bottom left corner y coordinate
   @param    c   The 8-bit font-indexed character (likely ascii)
   @param    color 16-bit 5-6-5 Color to draw chraracter with
   @param    bg 16-bit 5-6-5 Color to fill background with (if same as color, no background)
   @param    size  Font magnification level, 1 is 'original' size
 */
void display_drawChar(uint16_t x, uint16_t y, uint8_t c,
      uint16_t color, uint16_t bg, uint8_t size)
{
   uint16_t prev_x     = curx,
            prev_y     = cury,
            prev_color = txthue,
            prev_bg    = txtbgd;
   uint8_t  prev_size  = txtsz;

   /* draw a character with desired location, color and size */
   display_locate(x, y);
   display_setTextSize(size);
   display_setTextColor(color, bg);
   display_putc(c);

   /* restore original position, colors and size */
   curx     = prev_x;
   cury     = prev_y;
   txthue   = prev_color;
   txtbgd = prev_bg;
   txtsz    = prev_size;
}

/*!
   @brief  Set text cursor location
   @param  x    X coordinate in pixels
   @param  y    Y coordinate in pixels
 */
void display_locate(int16_t x, int16_t y) {
   curx = x;
   cury = y;
}

/*!
   @brief  Get text cursor X location
   @returns X coordinate in pixels
 */
uint16_t display_getCursorX(void) {
   return curx;
}

/*!
   @brief  Get text cursor Y location
   @returns Y coordinate in pixels
 */
uint16_t display_getCursorY(void) {
   return cury;
}

/*!
   @brief   Set text 'magnification' size. Each increase in s makes 1 pixel that much bigger.
   @param   s  Desired text size. 1 is default 6x8, 2 is 12x16, 3 is 18x24, etc
 */
void display_setTextSize(uint8_t s) {
   txtsz = (s > 0) ? s : 1;
}

/*!
   @brief   Set text font color with custom background color
   @param   c   16-bit 5-6-5 Color to draw text with
   @param   b   16-bit 5-6-5 Color to draw background/fill with
 */
void display_setTextColor(uint16_t c, uint16_t b) {
   txthue = c;
   txtbgd = b;
}

/*!
   @brief   Get width of the display, accounting for the current rotation
   @returns Width in pixels
 */
uint16_t display_getWidth(void) {
   return disp_w;
}

/*!
   @brief   Get height of the display, accounting for the current rotation
   @returns Height in pixels
 */
uint16_t display_getHeight(void) {
   return disp_h;
}

/*!
   @brief   Given 8-bit red, green and blue values, return a 'packed'
            16-bit color value in '565' RGB format (5 bits red, 6 bits
            green, 5 bits blue). This is just a mathematical operation,
            no hardware is touched.
   @param   red    8-bit red brightnesss (0 = off, 255 = max).
   @param   green  8-bit green brightnesss (0 = off, 255 = max).
   @param   blue   8-bit blue brightnesss (0 = off, 255 = max).
   @return  'Packed' 16-bit color value (565 format).
 */
uint16_t display_color565(uint8_t red, uint8_t green, uint8_t blue) {
   return ((uint16_t)(red & 0xF8) << 8) | ((uint16_t)(green & 0xFC) << 3) | (blue >> 3);
}

// end of code.
