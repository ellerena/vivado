/**
   @file      BR_gpioc
   @author      Ellerena Inc.
   @version   0.0.0.0
   @date
   @note      Under development<BR>
   @brief      STM communication module

*************************************************************************
   @copyright

      Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

      THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
      ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
      THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
      PARTICULAR PURPOSE.

      IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
      INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
      (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
      OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
      LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
      OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "gpio.h"

#ifdef FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#endif

extern volatile unsigned int gFlags;

void led_set(u32 led, u32 color)
{
   u32 temp;

   temp = LED_STATE;
   BITMC(temp, LED_W, led);
   BITMS(temp, color, led);
   BR_OUT32(REG_LEDS, temp);
}

void led_flick(u32 led, u32 color)
{
    volatile u32 old, new;

    new = old = LED_STATE;
    BITMC(new, LED_W, led);
    BITMS(new, color, led);
    BR_OUT32(REG_LEDS, new);
    for (new = 0; new < 500; new++);
    BR_OUT32(REG_LEDS, old);
}

void gpio_init(void)
{
   PB_SET_DIR;               /* set PUSH button ports as input */
   LED_SET_DIR;              /* set LED pins as output */
   led_set(LED0, LED_OFF);
   led_set(LED1, LED_OFF);

   PB_IRQENA;
   PB_IRQENAGLB;
}

void gpio_irqHandler(void *data)
{
    GFLS(F_PB);
    PB_IRQCLR;

#ifdef FREERTOS
    extern TaskHandle_t htask_mainproc;
    portYIELD_FROM_ISR(xTaskResumeFromISR(htask_mainproc));
#endif
}

///////////////////////////////////////////////////////////////////
// DEBUGGING FUNCTIONS - TODO: remove for release
///////////////////////////////////////////////////////////////////
void gpio_ledrotate(uint32_t led)
{
   uint32_t temp;

   temp = LED_STATE;                    // read led bits
   temp += (1 << led);                  // isolate led bits
   BITMG(temp, LED_W, led);             // restore led value
   led = LED_STATE & (~(LED_W << led));
   BITMS(led, temp, 0);                 // set new value
   BR_OUT32(REG_LEDS, led);             // load to register
}

