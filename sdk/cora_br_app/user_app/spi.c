/******************************************************************************
*
* Copyright (C) 2018 Eddie Llerena, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/***************************** Include Files *********************************/
#include "xspips.h"         /* SPI device driver */
#include "spi.h"

/************************** Constant Definitions *****************************/
/**************************** Type Definitions *******************************/
/***************** Macros (Inline Functions) Definitions *********************/
/************************** Function Prototypes ******************************/
/************************** Variable Definitions *****************************/
XSpiPs Spi0;                /* The instance of the SPI device */

int spi_Initialize(void)
{
    int Status;
    XSpiPs_Config *cnf; /* Pointer to Configuration data */

    cnf = XSpiPs_LookupConfig(XPAR_PS7_SPI_0_DEVICE_ID);
    TSTRLN (cnf == NULL);
    Status = XSpiPs_CfgInitialize(&Spi0, cnf, cnf->BaseAddress);
    TSTRLN (Status);

    XSpiPs_Reset(&Spi0);
    TSTRLN (!(Spi0.IsReady));

    /* Set the Spi device as a master and in loopback mode. */
    Status = XSpiPs_SetOptions(&Spi0, XSPIPS_MASTER_OPTION
                | XSPIPS_CLK_ACTIVE_LOW_OPTION
                | XSPIPS_FORCE_SSELECT_OPTION);
//                | XSPIPS_CLK_PHASE_1_OPTION);
    TSTRLN (Status);

    XSpiPs_SetClkPrescaler(&Spi0, XSPIPS_CLK_PRESCALE_8);  /* 166.66 / 8 = 21MHz */
//    XSpiPs_SetSlaveSelect(&Spi0, 0);       /* 0, 1 */

    return XST_SUCCESS;
}

int spi_TxRx(u8 * src, u8 * dst, u32 len)
{
   XSpiPs_PolledTransfer (&Spi0, src, dst, len);

   return 0;
}

