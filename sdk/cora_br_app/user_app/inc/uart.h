/*
 * uart.h
 *
 *  Created on: Mar 19, 2020
 *      Author: eddie
 */

#ifndef INC_UART_H_
#define INC_UART_H_

#include "xuartps.h"

#include "../inc/regs.h"

#define POINTER_TO_ARGS     (2)             /* index of the location of the 'arguments' in the buffer array */
#define BUFFER_SIZE         (40)            /* max number of bytes that can be received in a command entry */
#define TASK                acCommand[0]    /* variable to hold the requested 'task' */
#define MODE                acCommand[1]    /* variable to hold the 'mode' for the TASK*/
#define F_UART              (1 << 20)

extern u8 acCommand[];
extern u8 *acArgs;

int uart_Initialize(void);
int uart_IrqInit(void);

#endif /* INC_UART_H_ */
