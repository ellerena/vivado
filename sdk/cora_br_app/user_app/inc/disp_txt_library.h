/*
 * GFX_Library.h
 *
 *  Created on: Jun 13, 2020
 *      Author: Eddie Llerena
 */

#ifndef DISP_TXT_LIBRARY_H_
#define DISP_TXT_LIBRARY_H_

#include <stdint.h>
#include <stdbool.h>

#define DISPTXTWRAP      /* enable text wrapping */
#define TRIMLEADSPACES   /* trim lead space when printing a line */

//*************************** API ***************************//
void display_Init(void);
void display_putc(uint8_t c);
void display_customChar(const uint8_t *c);
void display_drawChar(uint16_t x, uint16_t y, uint8_t c, uint16_t color, uint16_t bg, uint8_t size);
void display_locate(int16_t x, int16_t y);
uint16_t getCursorX(void);
uint16_t getCursorY(void);
void display_setTextSize(uint8_t s);
void display_setTextColor(uint16_t c, uint16_t bg);
uint16_t display_getWidth();
uint16_t display_getHeight();
uint16_t display_color565(uint8_t red, uint8_t green, uint8_t blue);

#endif /* DISP_TXT_LIBRARY_H_ */
