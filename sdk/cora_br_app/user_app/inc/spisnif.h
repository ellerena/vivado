/*
 * spisnif.h
 *
 *  Created on: Mar 27, 2020
 *      Author: eddie
 */

#ifndef INC_SPISNIF_H_
#define INC_SPISNIF_H_

#include "SpiSniffer.h"
#include "../inc/regs.h"

#define SPIFBUF      1024

#define SPIFMO(x)    ((x) >> 24)
#define SPIFMI(x)    (((x) >> 16) & 0xff)
#define SPIFEMP(x)   ((x >> 15) & 1)
#define SPIFK(x)     ((x) & 0x7f)
#define SPIFVAL      BR_IN32(REG_SPISNIF_R0)
#define SPIF_OIK(x)  SPIFMO(x), SPIFMI(x), SPIFEMP(x), SPIFK(x)

extern u32 snif_dat[], snif_k;

#endif /* INC_SPISNIF_H_ */
