/*
 * spi.h
 *
 *  Created on: Mar 20, 2020
 *      Author: eddie
 */

#ifndef INC_SPI_H_
#define INC_SPI_H_

#include "../inc/regs.h"

#define BSIZE                   12      /* buffer size */

extern int spi_Initialize(void);
extern int spi_TxRx(u8 * src, u8 * dst, u32 len);

#endif /* INC_SPI_H_ */
