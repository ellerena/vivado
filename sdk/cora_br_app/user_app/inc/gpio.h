/**
   @file      BR_gpio.h
   @author      Ellerena Inc.
   @version   0.0.0.0
   @date
   @note      Under development<BR>
   @brief      STM communication module

*************************************************************************
   @copyright

      Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

      THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
      ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
      THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
      PARTICULAR PURPOSE.

      IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
      INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
      (INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
      OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
      LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
      OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#ifndef BR_GPIO_H_
#define BR_GPIO_H_

#include "xgpio_l.h"

#include "../inc/regs.h"

/*================================= bit definitions ==================================*/
#define F_PB                  (1 << 1)

#define PIN_INPUT             (1)
#define PIN_OUTPUT            (0)
#define LED_OFF               (0)
#define LED_B                 (1 << 0)
#define LED_G                 (1 << 1)
#define LED_R                 (1 << 2)
#define LED_W                 (LED_R + LED_G + LED_B)
#define LED_Y                 (LED_R + LED_G)
#define LED_C                 (LED_G + LED_B)
#define LED_M                 (LED_R + LED_B)
#define LED0                  (0)
#define LED1                  (3)

/*====================================== masks  ======================================*/
#define PB_ALL_MASK           (0x03)
#define PB_0                  (1 << 0)
#define PB_1                  (1 << 1)

/*====================================== macros ======================================*/
#define PB_SET_DIR            BR_OUT32(REG_BTNS_DIR, PB_ALL_MASK)
#define PB_0_ON               (BR_IN32(REG_BTNS) & PB_0)
#define PB_1_ON               (BR_IN32(REG_BTNS) & PB_1)
#define PB_ANY_ON             (BR_IN32(REG_BTNS) & PB_ALL_MASK)
#define LED_SET_DIR           BR_OUT32(REG_LEDS_DIR, 0)
#define LED_WR_DATA32(x)      BR_OUT32(REG_LEDS, (x))   /* present data on the LED (8 LSB used only) */
#define PB_STATE              BR_IN32(REG_BTNS)         /* returns state of push buttons (bit mask defined above) */
#define LED_STATE             BR_IN32(REG_LEDS)
#define PB_IRQCLR             BR_OUT32(REG_BTNS_ISR, PB_IRQSTA)
#define PB_IRQSTA             BR_IN32(REG_BTNS_ISR)
#define PB_IRQENA             BR_OUT32(REG_BTNS_IER, XGPIO_IR_MASK)
#define PB_IRQDIS             BR_OUT32(REG_BTNS_IER, 0)
#define PB_IRQENAGLB          BR_OUT32(REG_BTNS_GIE, XGPIO_GIE_GINTR_ENABLE_MASK)

/*==================================== External API ====================================*/
/**
 *   @brief   Routine to initialize the devices that are controlled via GPIO: LCD, LED, Push buttons, DIP switches.
 *         Since on the FPGA side these devices are all handled from the memory space, we
 *         perform a single initialization for them all together.
 */
extern void gpio_init(void);
extern void led_set(u32 led, u32 color);
extern void led_flick(u32 led, u32 color);

///////////////////////////////////////////////////////////////////
// DEBUGGING FUNCTIONS - TODO: remove for release
///////////////////////////////////////////////////////////////////
void gpio_ledrotate(uint32_t led);

#endif /* BR_GPIO_H_ */
