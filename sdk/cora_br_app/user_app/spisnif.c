/*
 * spisnif.c
 *
 *  Created on: Jul 4, 2019
 *      Author: eddie
 */

#include "spisnif.h"
#include "xil_printf.h"

u32 snif_dat[SPIFBUF], snif_k = 0;

void spisniff_irqHandler(void *data)
{
    uint32_t val;

    val = BR_IN32(REG_SPISNIF_R0);   /* read FIFO tail */
    if(snif_k < SPIFBUF) {
       snif_dat[snif_k++] = val;
    }
}
