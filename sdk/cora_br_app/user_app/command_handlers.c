/*
 * command_handlers.c
 *
 *  Created on: Jun 17, 2020
 *      Author: Eddie Llerena
 */

#include "command_handlers.h"

#include <os_i2c.h>
#include <os_1w.h>
#include <os_dht11.h>
#include <w1.h>
#include <stdio.h>
#include <string.h>

#include "InfoModule.h"
#include "ftdi_245_sync_fifo_if.h"
#include "time_interval.h"

#include "disp_txt_library.h"
#include "gpio.h"
#include "regs.h"
#include "spi.h"
#include "spisnif.h"
#include "SSD1306.h"

#define POINTER_TO_ARGS    (2)            /* index of the location of the 'arguments' in the buffer array */
#define BUFFER_SIZE        (52)           /* max number of bytes that can be received in a command entry */
#define TASK               buf[0]         /* variable to hold the requested 'task' */
#define MODE               buf[1]         /* variable to hold the 'mode' for the TASK*/

unsigned char c2b[] =
{ 0, 1, 2, 5, 11, 6, 13, 10, 4, 9, 3, 7, 15, 14, 12, 8 };

static unsigned int cod2bin(unsigned int in)
{
   return (c2b[in & 0xf] + (c2b[(in >> 4) & 0xf] << 4)
         + (c2b[(in >> 8) & 0xf] << 8));
}

/**
 @brief   converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
 Note: if first value is lower than 1fh (e.g. tab) then next bytes remain intact.
 @param   p contains the address of the first char in the string.
 @retval  the count of numbers comverted.
 @verbatim
 Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
 This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
 it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
 converted.
 @endverbatim
 */
static int ParseCharsToValueHex(char *p)
{
   char *q, d;
   int i, k;
   uint8_t c;

   q = p;
   k = 0;

   while (*p > 0x1f) /* Will process until a non printable character is found */
   {
      d = 0; /* initialize the 'number equivalent value' */
      i = 0; /* initialize the digit counter (assume 2 digits max per number)*/
      do
      {
         d <<= 4; /* each digit uses the first 4 bits, so on each loop we must push the others */
         c = *p++; /* read the new char */
         if ((c > 0x29) && (c < 0x3a)) /* char is between 0 and 9 */
         {
            d += (c - 0x30); /* add the char equivalen value to our total */
            i++; /* increment our digit counter */
         }
         if ((c > 0x60) && (c < 0x67)) /* char is between 0xa and 0xf */
         {
            d += (c - 0x60 + 0x9); /* add the char equivalen value to our total */
            i++; /* increment our digit counter */
         }
      } while ((i == 0) || ((*p > 0x2f) && (i < 2))); /* repeat: if no char found or: if found a space and less than 2 digits */

      if (i) /* if the digit counter is non-zero then a value has been found */
      {
         *q++ = d; /* write the number found to memory */
         k++; /* increment 'detected numbers counter' */
      }
   }

   *q = 0; /* write 0 at the end */

   return k;
}

static void DumpBytes(uint8_t * data, int len)
{
   int i = 0;
   while (len--)
   {
      PRNF("%02x", *data++);

      if ((15 == (i++ % 16)) || (0 == len))
      {
         PRNF(CRLF);
      }
      else
      {
         PRNF(" ");
      }
   }
}

void CommandProcessor(char * buf)
{
   char *acArgs = &buf[POINTER_TO_ARGS]; /* pointer to numeric arguments in buffer */
   int n;
   uint32_t temp;

//   display_puts(buf);
   n = ParseCharsToValueHex(acArgs);
   *(uint32_t*) buf |= 0x2020; /* simple conversion to lowercase */

   switch (TASK)
   {

      case 'c': /* communications, spi operations */
      {
         switch (MODE)
         {
            case 't':
            { /* transmit data */
               spi_TxRx((uint8_t*)acArgs, NULL, n);
               break;
            }
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      case 't': /* TIM operations */
      {
         switch (MODE)
         {
            case 'l':
            { /* print TIM result */
               BR_OUT32(REG_0_TIM, 0x1);
               BR_OUT32(REG_0_TIM, 0x0);
               BR_OUT32(REG_0_TIM, 0x2);
               BR_OUT32(REG_0_TIM, 0x0);
               for (int i = 0; i < 200; i++);
               n = cod2bin(BR_IN32(REG_0_TIM));
               PRNF("tim val:%u\t", n);
            }
            break;
            case 'p':
            { /* set REG_1_TIM low byte */
               n = inbyte();
               BR_OUT32(REG_1_TIM, n&0xff);
               break;
            }
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      case 'f':
      { /* SpiSniffer */
         switch(MODE)
         {
            case 'r':
            { /* SpiSniffer reset */
               BR_OUT32(REG_SPISNIF_R0, 1u);
               BR_OUT32(REG_SPISNIF_R0, 0u);
               snif_k = 0;
               break;
            }
            case 'g':
            { /* SpiSniffer data */
               PRNF(CRLF "No\tMO\tMI\tE CS" CRLF);
               for (int i = 0; i < snif_k; i++)
               {
                  PRNF("%04d\tx%02x\tx%02x\t%1d %04d" CRLF, i, SPIF_OIK(snif_dat[i]));
               }
               break;
            }
            case 'v':
            { /* SpiSniffer next value */
               n = SPIFVAL;
               PRNF("x%08x  MO:x%02x  MI:x%02x  E:%1d K:%4d\n>" CRLF, n, SPIF_OIK(n));
               break;
            }
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
         break;
      }

      case 's': /* system operations */
      {
         switch (MODE)
         {
            case 'i': /* fi: firmware id (welcome) */
            {
               PRNF("HW %08x %08x" CRLF, BR_IN32(REG_INFO_DATE), BR_IN32(REG_INFO_TIME));
            }
            break;
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      case 'i': /* i2c commands */
      {
         switch (MODE)
         {
            case 'a': /* ia<xx : 1 hex> address of slave */
            {
               OS_i2c_master_slv_addr((uint8_t)acArgs[0]);
            }
            break;
            case 's': /* scan slaves present in bus */
            {
               OS_i2c_master_slv_scan();
            }
            break;
            case 'w': /* write data bytes to slave */
            {
               OS_i2c_master_wr((uint8_t*)acArgs, n);
            }
            break;
            case 'r': /* ir<nn> reads nn bytes from current i2c address */
            {
               n = acArgs[0];
               OS_i2c_master_rd((uint8_t*)acArgs, n);
               DumpBytes((uint8_t*)acArgs, n);
            }
            break;
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      case 'o': /* oled operations */
      {
         switch (MODE)
         {
            case 'i': /* oi: oled initialization */
            {
               display_Init();}
            break;
            case 'c': /* oc<xxyy....: n hex> commands to send to oled */
            {
               uint8_t *p = (uint8_t*)acArgs;
               while (n--)
               {
                  ssd1306_command(*p++);
               }
            }
            break;
            case 't': /* ot<\t><....> : write text string to oled */
            {
               if (acArgs[0] == '\0')
               {
                  display_puts(&acArgs[1]);
               }
            }
            break;
            case 'p': /* op<x:8><y<:8> : position cursor at lines x, y */
            {
               display_locate(acArgs[0], acArgs[1]);
            }
            break;
            case 'z': /* oz: clear screen */
            {
               display_clear();
            }
            break;
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      case 'm': /* memory operations */
      {
         temp = REV32(*(u32*)acArgs);
         switch (MODE)
         {
            case 'w':
            { /* mw<addr:32><val:32> write <val> to memory <addr> */
               BR_OUT32(temp, REV32(*(u32*)&acArgs[4]));
               break;
            }
            case 'r':
            { /* mr<addr:32>[<num_words:8>] read <num_words> starting at <addr> */
               n = (n < 5) ? 1 : acArgs[4]; /* if num_words is not missing use 1 */
               for (int j = 0; j < n; j++, temp += 4)
               {
                  PRNF("[%08X] x%08x" CRLF, temp, BR_IN32(temp));
               }
               break;
            }
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      case '1': /* 1-wire operations */
      {
         switch (MODE)
         {
            case 'h':
            { /* 1h: set 1-wire pin high */
               OS_1w_pin_set(1);
               break;
            }
            case 'l':
            { /* 1l: set 1-wire pin high */
               OS_1w_pin_set(0);
               break;
            }
            case 's':
            { /* 1s: read 1-wire status register */
               PRNF("1w_s: %08xh\n", W1_STATUS);
               break;
            }
            case 'd':
            { /* 1d: present DHT11 data result */
               OS_dht11_show_result();
               break;
            }
            case 't':
            { /* 1t<val:32> sets the 1-wire 1/0 detector threshold value */
               OS_dht11_set_treshold(REV32(*(u32*)acArgs));
               break;
            }
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      case 'u': /* USB (FTDI) operations */
      {
         switch (MODE)
         {
            case 's':
            { /* us: read status register */
               temp = BR_IN32(REG_FT_STATUS);
               PRNF("Status: %08xh\n", temp); /* show entire status register */
               PRNF("TX Full: %d\n", (bool)(temp & 0x8000));
               PRNF("RXEmpty: %d\n", (bool)(temp & 0x80000000));
            }
            break;
            case 'r':
            { /* check Rx, print any existing data */
               while (!(BR_IN32(REG_FT_STATUS) & 0x80000000))
               {
                  PRNF("%c", BR_IN32(REG_FT_RD) & 0xff);
               }
               PRNF("\n");

            }
            break;
            case 't': /* ut<\t><....> : write text string to usb */
            if (acArgs[0] == '\0')
            {
               temp = 1;
               while (acArgs[temp] >= ' ')
               {
                  BR_OUT32(REG_FT_WR, acArgs[temp++]);
               }
            }
            break;
            default:
            {
               GFS(F_COMMERR);
            }
            break;
         }
      }
      break;

      default:
      {
         GFS(F_COMMERR);
      }
      break;
   }

   led_flick(LED0, LED_W); /* twinkle to let us know its done */

   if (GF(F_COMMERR))
   {
      GFC(F_COMMERR);
      memcpy(buf, "Unknown Command\0", 16);
   }
   else
   {
      memcpy(buf, "Ok\0", 3);
   }

}

