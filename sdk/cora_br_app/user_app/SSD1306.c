/******************************************************************************
 SSD1306.c                                   *
 SSD1306 OLED driver for CCS PIC C compiler                 *
 *
 This driver supports SPI mode and I2C mode (default).                        *
 For the SPI mode and before the include of the driver file, add:             *
 #define SSD1306_SPI_MODE                                                   *
 *
 https://simple-circuit.com/                                                  *
 *
 *******************************************************************************
 *******************************************************************************
 This is a library for our Monochrome OLEDs based on SSD1306 drivers          *
 *
 Pick one up today in the adafruit shop!                                     *
 ------> http://www.adafruit.com/category/63_98                              *
 *
 Adafruit invests time and resources providing this open source code,         *
 please support Adafruit and open-source hardware by purchasing               *
 products from Adafruit!                                                      *
 *
 Written by Limor Fried/Ladyada  for Adafruit Industries.                     *
 BSD license, check license.txt for more information                          *
 All text above, and the splash screen must be included in any redistribution *
 *******************************************************************************/

#include <os_i2c.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "disp_txt_library.h"
#include "SSD1306.h"

#define SWAP(a, b)      { uint8_t t = a; a = b; b = t; }
#define VRAM_SZ         (SSD1306_LCDWIDTH*SSD1306_LCDHEIGHT / 8)
#define SSD1306_CHARS_L 21 //((SSD1306_LCDWIDTH / 6) - 1)
#define SSD1306_LINES   8  //(SSD1306_LCDHEIGHT / 8)

extern int16_t curx, cury;

uint8_t rotation = 0; /* Display rotation (0 thru 3) */
uint8_t disp_w; /* we must use variables for the display width */
uint8_t disp_h; /* and height because they change when rotating */
uint8_t y_line = 0; /* local variable used for vertical scrolling */

/* our 'video ram'. Stores the pixel content of the entire screen */
static uint8_t vram[VRAM_SZ] =
{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0xfe, 0xfe, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0xfe, 0xfe, 0xfe, 0x00, 0xfe, 0xfe, 0xfe, 0x04, 0x0c, 0x0c, 0x38, 0x78,
      0xf0, 0xc0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff,
      0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f,
      0xff, 0xff, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x00,
      0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xff, 0xff, 0xfe,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x0f, 0x1e,
      0x1c, 0x30, 0x30, 0x20, 0x7f, 0x7f, 0x7f, 0x70, 0x77, 0x77, 0x77, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0x7f, 0x7f, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3e, 0x7e, 0x40, 0x40, 0x7e,
      0x3e, 0x00, 0x7e, 0x7e, 0x0c, 0x30, 0x7e, 0x7e, 0x00, 0x3e, 0x7e, 0x40,
      0x40, 0x7e, 0x3e, 0x00, 0x2c, 0x4e, 0x5a, 0x7a, 0x76, 0x24, 0x00, 0x00,
      0x00, 0x00, 0x7e, 0x7e, 0x12, 0x12, 0x1e, 0x0c, 0x00, 0x7e, 0x7e, 0x12,
      0x32, 0x7e, 0x4c, 0x00, 0x3c, 0x7e, 0x42, 0x42, 0x7e, 0x3c, 0x00, 0x00,
      0x00, 0x00, 0x3c, 0x7e, 0x42, 0x42, 0x7e, 0x3c, 0x00, 0x7e, 0x7e, 0x04,
      0x1c, 0x04, 0x7e, 0x7e, 0x00, 0x7e, 0x7e, 0x0c, 0x30, 0x7e, 0x7e, 0x00,
      0x66, 0x66, 0x7e, 0x7e, 0x66, 0x66, 0x00, 0x7e, 0x7e, 0x5a, 0x5a, 0x7e,
      0x24, 0x00, 0x3e, 0x7e, 0x40, 0x40, 0x7e, 0x3e, 0x00, 0x2c, 0x4e, 0x5a,
      0x7a, 0x76, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0xfc, 0x84, 0x84, 0xfc, 0x78,
      0x00, 0xfc, 0xfc, 0x08, 0x38, 0x08, 0xfc, 0xfc, 0x00, 0xfc, 0xfc, 0x18,
      0x60, 0xfc, 0xfc, 0x00, 0xfc, 0xfc, 0xb4, 0xb4, 0x84, 0x84, 0x00, 0x58,
      0x9c, 0xb4, 0xf4, 0xec, 0x48, 0x00, 0x00, 0x00, 0x00, 0xfc, 0xfc, 0x24,
      0x24, 0x3c, 0x18, 0x00, 0xfc, 0xfc, 0x24, 0x64, 0xfc, 0x98, 0x00, 0x78,
      0xfc, 0x84, 0x84, 0xfc, 0x78, 0x00, 0x00, 0x00, 0x00, 0x7c, 0xfc, 0x80,
      0x80, 0xfc, 0x7c, 0x00, 0xfc, 0xfc, 0x18, 0x60, 0xfc, 0xfc, 0x00, 0x78,
      0xfc, 0x84, 0x84, 0xfc, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff,
      0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x7c, 0x50, 0x60, 0x00, 0x18, 0x60, 0x18, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x7c, 0x54, 0x44, 0x44, 0x00, 0x7c, 0x44, 0x38, 0x00, 0x38,
      0x54, 0x54, 0x34, 0x00, 0x7c, 0x54, 0x44, 0x44, 0x00, 0x00, 0x00, 0x54,
      0x54, 0x7c, 0x28, 0x00, 0x38, 0x44, 0x44, 0x38, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff,
      0xff, 0xff, 0xff, };

void ssd1306_intro_wr(uint8_t *data, int32_t size, uint8_t command)
{
   uint8_t * p = (uint8_t*) malloc(size + sizeof(uint8_t));

   if (p)
   {
      p[0] = command;
      memcpy((void*) (&p[1]), data, size);
      OS_i2c_master_wr(p, size + sizeof(uint8_t));
      free(p);
   }
}

void ssd1306_command(uint8_t c)
{
   uint8_t comm[2] =
   { SSD1306_CO_COMMAND, c };

   OS_i2c_master_wr(comm, 2);
}

void SSD1306_Init(void)
{
   // Init sequence
   ssd1306_command(SSD1306_DISPLAYOFF);          // 0xAE
   ssd1306_command(SSD1306_SETDISPLAYCLOCKDIV);  // 0xD5
   ssd1306_command(0x80);                        // the suggested ratio 0x80

   ssd1306_command(SSD1306_SETMULTIPLEX);        // 0xA8
   ssd1306_command(SSD1306_LCDHEIGHT - 1);

   ssd1306_command(SSD1306_SETDISPLAYOFFSET);    // 0xD3
   ssd1306_command(0x0);                         // no offset
   ssd1306_command(SSD1306_SETSTARTLINE | 0x0);  // line #0
   ssd1306_command(SSD1306_CHARGEPUMP);          // 0x8D
   ssd1306_command(0x14);
   ssd1306_command(SSD1306_MEMORYMODE);          // 0x20
   ssd1306_command(0x00);                        // 0x0 act like ks0108
   ssd1306_command(SSD1306_SEGREMAP | 0x1);
   ssd1306_command(SSD1306_COMSCANDEC);

   ssd1306_command(SSD1306_SETCOMPINS);          // 0xDA
   ssd1306_command(0x12);
   ssd1306_command(SSD1306_SETCONTRAST);         // 0x81
   ssd1306_command(0xCF);

   ssd1306_command(SSD1306_SETPRECHARGE);        // 0xd9
   ssd1306_command(0xF1);
   ssd1306_command(SSD1306_SETVCOMDETECT);       // 0xDB
   ssd1306_command(0x40);
   ssd1306_command(SSD1306_DISPLAYALLON_RESUME); // 0xA4
   ssd1306_command(SSD1306_NORMALDISP);       // 0xA6

   ssd1306_command(SSD1306_DEACTIVATE_SCROLL);

   ssd1306_command(SSD1306_DISPLAYON);  //--turn on oled panel

   disp_h = SSD1306_LCDHEIGHT;
   disp_w = SSD1306_LCDWIDTH;
}

void display(void)
{
   ssd1306_command(SSD1306_COLUMNADDR);
   ssd1306_command(0);                  // Column start address (0 = reset)
   ssd1306_command(SSD1306_LCDWIDTH - 1); // Column end address (127 = reset)

   ssd1306_command(SSD1306_PAGEADDR);
   ssd1306_command(0);                  // Page start address (0 = reset)
   ssd1306_command(7);                  // Page end address

   /* write page by page (actually line by line) */
   for (int i = 0; i < VRAM_SZ; i += 16)
   {
      ssd1306_intro_wr(&vram[i], 16, SSD1306_CO_DATA_FIELD);
   }
}

/**
 * @brief   Allow to invert the display
 * @param   invert, either 0: normal or 1: invert
 * */
void display_invert(int invert)
{
   ssd1306_command(invert ? SSD1306_INVERTDISP : SSD1306_NORMALDISP);
}

// startscrollright
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollRight(uint8_t start, uint8_t stop)
{
   ssd1306_command(SSD1306_RIGHT_HORIZONTAL_SCROLL);
   ssd1306_command(0X00);
   ssd1306_command(start);
   ssd1306_command(0X00);
   ssd1306_command(stop);
   ssd1306_command(0X00);
   ssd1306_command(0XFF);
   ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// startscrollleft
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollLeft(uint8_t start, uint8_t stop)
{
   ssd1306_command(SSD1306_LEFT_HORIZONTAL_SCROLL);
   ssd1306_command(0X00);
   ssd1306_command(start);
   ssd1306_command(0X00);
   ssd1306_command(stop);
   ssd1306_command(0X00);
   ssd1306_command(0XFF);
   ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// startscrolldiagright
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollDiagRight(uint8_t start, uint8_t stop)
{
   ssd1306_command(SSD1306_SET_VERTICAL_SCROLL_AREA);
   ssd1306_command(0X00);
   ssd1306_command(SSD1306_LCDHEIGHT);
   ssd1306_command(SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL);
   ssd1306_command(0X00);
   ssd1306_command(start);
   ssd1306_command(0X00);
   ssd1306_command(stop);
   ssd1306_command(0X01);
   ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

// startscrolldiagleft
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// display_scrollright(0x00, 0x0F)
void display_startScrollDiagLeft(uint8_t start, uint8_t stop)
{
   ssd1306_command(SSD1306_SET_VERTICAL_SCROLL_AREA);
   ssd1306_command(0X00);
   ssd1306_command(SSD1306_LCDHEIGHT);
   ssd1306_command(SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL);
   ssd1306_command(0X00);
   ssd1306_command(start);
   ssd1306_command(0X00);
   ssd1306_command(stop);
   ssd1306_command(0X01);
   ssd1306_command(SSD1306_ACTIVATE_SCROLL);
}

void display_stopScroll(void)
{
   ssd1306_command(SSD1306_DEACTIVATE_SCROLL);
}

void display_dim(uint8_t level)
{
   // the range of contrast to too small to be really useful
   // it is useful to dim the display
   ssd1306_command(SSD1306_SETCONTRAST);
   ssd1306_command(level ? 0xCF : 0);
}

void display_clear(void)
{
   /* clear video ram */
   memset(vram, 0, VRAM_SZ);

   /* set cursor position to origin */
   cury = 0;
   curx = 0;

   /* reset scrolling */
   y_line = 0;
   ssd1306_command(SSD1306_SETSTARTLINE | y_line);
}

void display_puts(const char * str)
{
   /* update scrolling calculator */
   y_line += 8;

   /* if we're at bottom, scroll one line up */
   if (y_line > SSD1306_SETSTARTLINE)
   {
      y_line = SSD1306_SETSTARTLINE + (y_line & 0x3f);
      ssd1306_command(SSD1306_SETSTARTLINE | y_line);
   }

   /* print one complete line of text */
   for (int i = 0; i < SSD1306_CHARS_L; ++i)
   {
      display_putc(*str >= ' ' ? *str++ : ' ');

      /* ensure we don't pass beyond right edge */
      if (((disp_w - curx) < 6) || (curx < 6))
         break;
   }

   /* if last line is reached, wrap to the top line */
   if (cury > 56)
   {
      cury = 0;
   }

   /* transmit video ram to device */
   display();
}
/////
void display_fillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color)
{
   for (int i = x; i < x + w; i++)
      display_drawVLine(i, y, h, color);
}

void display_fillScreen(void)
{
   display_fillRect(0, 0, SSD1306_LCDWIDTH, SSD1306_LCDHEIGHT, 1);
}

void display_setRotation(uint8_t m)
{
   rotation = (m & 3);
   switch (rotation)
   {
      case 0:
      case 2:
         disp_w = SSD1306_LCDWIDTH;
         disp_h = SSD1306_LCDHEIGHT;
         break;
      case 1:
      case 3:
         disp_w = SSD1306_LCDHEIGHT;
         disp_h = SSD1306_LCDWIDTH;
         break;
   }
}

/*!
 @brief   Get rotation setting for display
 @returns 0 thru 3 corresponding to 4 cardinal rotations
 */
uint8_t display_getRotation(void)
{
   return rotation;
}

// the most basic function, set a single pixel
void display_drawPixel(uint8_t x, uint8_t y, uint8_t color)
{
   uint32_t pos;

   if ((x >= disp_w) || (y >= disp_h))
      return;

   // check rotation, move pixel around if necessary
   switch (rotation)
   {
      case 1:
         SWAP(x, y)
         ;
         x = SSD1306_LCDWIDTH - x - 1;
         break;
      case 2:
         x = SSD1306_LCDWIDTH - x - 1;
         y = SSD1306_LCDHEIGHT - y - 1;
         break;
      case 3:
         SWAP(x, y)
         ;
         y = SSD1306_LCDHEIGHT - y - 1;
         break;
   }

   /* calculate pixel position in video ram, x is the column number */
   pos = x + (uint16_t) (y / 8) * SSD1306_LCDWIDTH;

   switch (color)
   {
      case WHITE:
         vram[pos] |= (1 << (y & 7)); /* set bit */
         break;
      case BLACK:
         vram[pos] &= ~(1 << (y & 7)); /* reset bit */
         break;
      case INVERSE:
         vram[pos] ^= (1 << (y & 7)); /* invert bit */
         break;
   }
}

void display_drawHLine(uint8_t x, uint8_t y, uint8_t w, uint8_t color)
{
   bool bSwap = false;
   switch (rotation)
   {
      case 0:
         // 0 degree rotation, do nothing
         break;
      case 1:
         // 90 degree rotation, swap x & y for rotation, then invert x
         bSwap = true;
         SWAP(x, y)
         ;
         x = SSD1306_LCDWIDTH - x - 1;
         break;
      case 2:
         // 180 degree rotation, invert x and y - then shift y around for disp_h.
         x = SSD1306_LCDWIDTH - x - 1;
         y = SSD1306_LCDHEIGHT - y - 1;
         x -= (w - 1);
         break;
      case 3:
         // 270 degree rotation, swap x & y for rotation, then invert y  and adjust y for w (not to become h)
         bSwap = true;
         SWAP(x, y)
         ;
         y = SSD1306_LCDHEIGHT - y - 1;
         y -= (w - 1);
         break;
   }

   if (bSwap)
   {
      drawFastVLineInternal(x, y, w, color);
   }
   else
   {
      drawFastHLineInternal(x, y, w, color);
   }
}

void display_drawVLine(uint8_t x, uint8_t y, uint8_t h, uint8_t color)
{
   bool bSwap = false;
   switch (rotation)
   {
      case 0:
         break;
      case 1:
         // 90 degree rotation, swap x & y, then invert x and adjust x for h (now to become w)
         bSwap = true;
         SWAP(x, y)
         ;
         x = SSD1306_LCDWIDTH - x - 1;
         x -= (h - 1);
         break;
      case 2:
         // 180 degree rotation, invert x and y - then shift y around for disp_h.
         x = SSD1306_LCDWIDTH - x - 1;
         y = SSD1306_LCDHEIGHT - y - 1;
         y -= (h - 1);
         break;
      case 3:
         // 270 degree rotation, swap x & y for rotation, then invert y
         bSwap = true;
         SWAP(x, y)
         ;
         y = SSD1306_LCDHEIGHT - y - 1;
         break;
   }

   if (bSwap)
   {
      drawFastHLineInternal(x, y, h, color);
   }
   else
   {
      drawFastVLineInternal(x, y, h, color);
   }
}

void drawFastHLineInternal(uint8_t x, uint8_t y, uint8_t w, uint8_t color)
{
   // Do bounds/limit checks
   if (y >= SSD1306_LCDHEIGHT)
   {
      return;
   }

   // make sure we don't go off the edge of the display
   if ((x + w) > SSD1306_LCDWIDTH)
   {
      w = (SSD1306_LCDWIDTH - x);
   }

   // set up the pointer for  movement through the buffer
   register uint8_t *pBuf = vram;
   // adjust the buffer pointer for the current row
   pBuf += ((uint16_t) (y / 8) * SSD1306_LCDWIDTH);
   // and offset x columns in
   pBuf += x;

   register uint8_t mask = 1 << (y & 7);

   switch (color)
   {
      case WHITE:
         while (w--)
         {
            *pBuf++ |= mask;
         }
         ;
         break;
      case BLACK:
         mask = ~mask;
         while (w--)
         {
            *pBuf++ &= mask;
         }
         break;
      case INVERSE:
         while (w--)
         {
            *pBuf++ ^= mask;
         }
         break;
   }
}

void drawFastVLineInternal(uint8_t x, uint8_t __y, uint8_t __h, uint8_t color)
{

   // do nothing if we're off the left or right side of the screen
   if (x >= SSD1306_LCDWIDTH)
   {
      return;
   }

   // make sure we don't go past the disp_h of the display
   if ((__y + __h) > SSD1306_LCDHEIGHT)
   {
      __h = (SSD1306_LCDHEIGHT - __y);
   }

   // this display doesn't need ints for coordinates, use local byte registers for faster juggling
   register uint8_t y = __y;
   register uint8_t h = __h;

   // set up the pointer for fast movement through the buffer
   register uint8_t *pBuf = vram;
   // adjust the buffer pointer for the current row
   pBuf += ((uint16_t) (y / 8) * SSD1306_LCDWIDTH);
   // and offset x columns in
   pBuf += x;

   // do the first partial byte, if necessary - this requires some masking
   register uint8_t mod = (y & 7);
   if (mod)
   {
      // mask off the high n bits we want to set
      mod = 8 - mod;

      // note - lookup table results in a nearly 10% performance improvement in fill* functions
      // register uint8_t mask = ~(0xFF >> (mod));
      static uint8_t premask[8] =
      { 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };
      register uint8_t mask = premask[mod];

      // adjust the mask if we're not going to reach the end of this byte
      if (h < mod)
      {
         mask &= (0XFF >> (mod - h));
      }

      switch (color)
      {
         case WHITE:
            *pBuf |= mask;
            break;
         case BLACK:
            *pBuf &= ~mask;
            break;
         case INVERSE:
            *pBuf ^= mask;
            break;
      }

      // fast exit if we're done here!
      if (h < mod)
      {
         return;
      }

      h -= mod;

      pBuf += SSD1306_LCDWIDTH;
   }

   // write solid bytes while we can - effectively doing 8 rows at a time
   if (h >= 8)
   {
      if (color == INVERSE)
      { // separate copy of the code so we don't impact performance of the black/white write version with an extra comparison per loop
         do
         {
            *pBuf = ~(*pBuf);

            // adjust the buffer forward 8 rows worth of data
            pBuf += SSD1306_LCDWIDTH;

            // adjust h & y (there's got to be a faster way for me to do this, but this should still help a fair bit for now)
            h -= 8;
         } while (h >= 8);
      }
      else
      {
         // store a local value to work with
         register uint8_t val = (color == WHITE) ? 255 : 0;

         do
         {
            // write our value in
            *pBuf = val;

            // adjust the buffer forward 8 rows worth of data
            pBuf += SSD1306_LCDWIDTH;

            // adjust h & y (there's got to be a faster way for me to do this, but this should still help a fair bit for now)
            h -= 8;
         } while (h >= 8);
      }
   }

   // now do the final partial byte, if necessary
   if (h)
   {
      mod = h & 7;
      // this time we want to mask the low bits of the byte, vs the high bits we did above
      // register uint8_t mask = (1 << mod) - 1;
      // note - lookup table results in a nearly 10% performance improvement in fill* functions
      static uint8_t postmask[8] =
      { 0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };
      register uint8_t mask = postmask[mod];
      switch (color)
      {
         case WHITE:
            *pBuf |= mask;
            break;
         case BLACK:
            *pBuf &= ~mask;
            break;
         case INVERSE:
            *pBuf ^= mask;
            break;
      }
   }
}

// end of driver code.
