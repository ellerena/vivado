/*
 * gic.c
 *
 *  Created on: Mar 19, 2020
 *      Author: eddie
 */

#include <stdio.h>
#include "xil_io.h"
#include "xil_exception.h"
#include "xscugic.h"
#include "xgpio.h"
#include "regs.h"
#include "uart.h"

#define GIC_ID          XPAR_SCUGIC_0_DEVICE_ID
#define IRQID_SPISNIF   XPAR_FABRIC_SPISNIFFER_0_VEC_ID
#define IRQID_1WIRE     XPAR_FABRIC_WIRE1_RX_0_VEC_ID
#define IRQID_GPIO      XPAR_FABRIC_GPIO_0_VEC_ID
#define IRQID_UART0     XPAR_XUARTPS_0_INTR
#define IRQID_IRQA      61

extern void gpio_irqHandler(void *data);
extern void spisniff_irqHandler(void *data);
extern void irqA_irqHandler(void *data);
extern void uart_irqHandler(void *data, u32 event, u32 eventdata);

#ifdef FREERTOS

extern XScuGic xInterruptController;

extern int SetupInterruptSystem(void);

void vApplicationDaemonTaskStartupHook( void )
{
    SetupInterruptSystem();
}

#else

XScuGic xInterruptController;

#endif

XScuGic *pIntc = &xInterruptController; /* Instance of the Interrupt Controller */

int SetupInterruptSystem(void)
{
    int sta;

#ifndef FREERTOS
    XScuGic_Config *iCnf;

    /* get config for interrupt controller */
    iCnf = XScuGic_LookupConfig(GIC_ID);
    TSTRER (iCnf == NULL);

    /* initialize the interrupt controller driver */
    sta = XScuGic_CfgInitialize(pIntc, iCnf, iCnf->CpuBaseAddress);
    TSTRER(sta);
#endif

    /* connect the interrupt service routines to the interrupt controller */
//    sta = XScuGic_Connect(pIntc, IRQID_SPISNIF, (Xil_InterruptHandler)spisniff_irqHandler, 0);
//    TSTRER (sta);
    sta = XScuGic_Connect(pIntc, IRQID_GPIO, (Xil_InterruptHandler)gpio_irqHandler, NULL);
    TSTRER (sta);
    sta = XScuGic_Connect(pIntc, IRQID_UART0, (Xil_ExceptionHandler)uart_irqHandler, NULL);
    TSTRER (sta);
    sta = XScuGic_Connect(pIntc, IRQID_IRQA, (Xil_ExceptionHandler)irqA_irqHandler, NULL);
    TSTRER (sta);

    // set the priority of IRQ_F2P[] (highest 0xF8, lowest 0x00) and triggers for rising edge/level
    XScuGic_SetPriorityTriggerType(pIntc, IRQID_SPISNIF, 0x00, 0x1);
    XScuGic_SetPriorityTriggerType(pIntc, IRQID_GPIO, 0xA0, 0x3);
    XScuGic_SetPriorityTriggerType(pIntc, IRQID_IRQA, 0xA8, 0x3);
//    XScuGic_SetPriorityTriggerType(pIntc, IRQID_UART0, 0x08, 1);

    // enable interrupts for IRQ_F2P[]
    XScuGic_Enable(pIntc, IRQID_GPIO);
    XScuGic_Enable(pIntc, IRQID_IRQA);

    XScuGic_Enable(pIntc, IRQID_UART0);
    XScuGic_Enable(pIntc, IRQID_SPISNIF);

    // initialize the exception table and register the interrupt controller handler with the exception table
    Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
       (Xil_InterruptHandler)XScuGic_InterruptHandler, pIntc);
    Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_FIQ_INT,
       (Xil_ExceptionHandler)spisniff_irqHandler, (void*)XPAR_SCUGIC_CPU_BASEADDR);

//    Xil_ExceptionInit();
    Xil_ExceptionEnable();    /* Enable non-critical exceptions */
    Xil_ExceptionEnableMask(XIL_EXCEPTION_FIQ); /* Enable fast exceptions */

    return XST_SUCCESS;
}

