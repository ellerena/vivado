/*
 * drv_1w.c
 *
 *  Created on: Sep 11, 2021
 *      Author: ellerena
 */

/***************************** Include Files *******************************/
#include <w1.h>

/***************************** Translated APIs *****************************/

void DRV_1w_reg_wr(uint32_t reg, uint32_t val)
{
   w1_reg_wr(reg, val);
}

uint32_t DRV_1w_reg_rd(uint32_t reg)
{
   return w1_reg_rd(reg);
}

void DRV_1w_pin_set(int state)
{
   w1_set_output_pin(state);
}

int DRV_w1_get_data(void* buf)
{
   return w1_get_data(buf);
}
