/*
 * drv_i2c.c
 *
 *  Created on: Jul 10, 2020
 *      Author: eddie
 */

/***************************** Include Files *******************************/
#include <i2c.h>

/***************************** Translated APIs *****************************/

int DRV_i2c_master_init(void)
{
   return i2c_master_init();
}

int DRV_i2c_master_slv_addr(uint8_t addr)
{
   i2c_set_slave_addr(addr);
   return 0;
}

int DRV_i2c_master_slv_scan(void)
{
   i2c_scan();
   return 0;
}

int DRV_i2c_master_wr(uint8_t *data, uint32_t len)
{
   return i2c_master_wr(data, len);
}

int DRV_i2c_master_rd(uint8_t *data, uint32_t len)
{
   return i2c_master_rd(data, len);
}

/***********************************************************/
