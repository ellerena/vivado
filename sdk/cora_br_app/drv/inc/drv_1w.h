/*
 * drv_1w.h
 *
 *  Created on: Sep 11, 2021
 *      Author: ellerena
 */

#ifndef DRV_INC_DRV_1W_H_
#define DRV_INC_DRV_1W_H_

/* required includes */
#include <stdint.h>

/* main API */
extern void DRV_1w_reg_wr(uint32_t reg, uint32_t val);
extern uint32_t DRV_1w_reg_rd(uint32_t reg);
extern void DRV_1w_pin_set(int state);
extern int DRV_w1_get_data(void* buf);

#endif /* DRV_INC_DRV_1W_H_ */
