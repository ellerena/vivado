/*
 * drv_i2c.h
 *
 *  Created on: Sep 10, 2021
 *      Author: ellerena
 */

#ifndef DRV_DRV_I2C_H_
#define DRV_DRV_I2C_H_

/* required includes */
#include <stdint.h>

/* main API */
extern int DRV_i2c_master_init(void);
extern int DRV_i2c_master_slv_addr(uint8_t addr);
extern int DRV_i2c_master_slv_scan(void);
extern int DRV_i2c_master_wr(uint8_t *data, uint32_t len);
extern int DRV_i2c_master_rd(uint8_t *data, uint32_t len);

#endif /* DRV_DRV_I2C_H_ */
