/*
 * os_dht11.c
 *
 *  Created on: Sep 10, 2021
 *      Author: ellerena
 */

/***************************** Include Files *******************************/
#include <stdio.h>

#include <drv_1w.h>
#include <w1_regs.h>
#include <regs.h>

/***************************** Translated APIs *****************************/

int OS_dht11_data_rd(void * buf)
{
   return DRV_w1_get_data(buf);
}

void OS_dht11_show_result(void)
{
   uint8_t buf[4 * 2]; /* temporary buffer to store the dht11 data */
   int result;

   result = OS_dht11_data_rd((void*) buf);

   if (0 == result)
   {

      float farht;
      uint8_t * dat = buf;

      farht = dat[2] + (float) dat[1] / 100;
      farht = ((farht * 9) / 5) + 32;

      uint32_t status = DRV_1w_reg_rd(REG_W1_STA);

      PRNF("stat: %08xh\n", status);
      PRNF("read: %02x%08xh\n", dat[4], *(uint32_t*) dat);
      PRNF("chks: %3d\n", dat[0]);
      printf("%d.%02dH %d.%dC %.2fF\n", dat[4], dat[3], dat[2], dat[1], farht);
   }
   else
   {
      PRNS("DHT11 read failed\n");
   }
}

void OS_dht11_set_treshold(uint32_t tres)
{
   DRV_1w_reg_wr(REG_W1_TRESH, tres);
}
