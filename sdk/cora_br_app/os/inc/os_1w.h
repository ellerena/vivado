/*
 * os_1w.h
 *
 *  Created on: Sep 11, 2021
 *      Author: ellerena
 */

#ifndef OS_INC_OS_1W_H_
#define OS_INC_OS_1W_H_

/* required includes */
#include <stdint.h>

/* main API */
extern void OS_1w_reg_wr(uint32_t reg, uint32_t val);
extern uint32_t OS_1w_reg_rd(uint32_t reg);
extern void OS_1w_pin_set(int state);

#endif /* OS_INC_OS_1W_H_ */
