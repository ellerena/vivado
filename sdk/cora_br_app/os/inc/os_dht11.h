/*
 * os_dht11.h
 *
 *  Created on: Sep 10, 2021
 *      Author: ellerena
 */

#ifndef OS_DHT11_H_
#define OS_DHT11_H_

/* required includes */
#include <stdint.h>

/* main API */

extern int OS_dht11_data_rd(void * buf);
extern void OS_dht11_show_result(void);
extern void OS_dht11_set_treshold(uint32_t tres);

#endif /* OS_DHT11_H_ */
