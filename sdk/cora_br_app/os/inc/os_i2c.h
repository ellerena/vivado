/*
 * os_i2c.h
 *
 *  Created on: Sep 10, 2021
 *      Author: ellerena
 */

#ifndef OS_I2C_H_
#define OS_I2C_H_

/* required includes */
#include <stdint.h>

/* main API */
extern int OS_i2c_master_init(void);
extern int OS_i2c_master_wr(uint8_t *data, uint32_t len);
extern int OS_i2c_master_rd(uint8_t *data, uint32_t len);
extern int OS_i2c_master_slv_addr(uint8_t addr);
extern int OS_i2c_master_slv_scan(void);

#endif /* OS_I2C_H_ */
