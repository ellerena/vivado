/*
 * os_i2c.c
 *
 *  Created on: Jul 10, 2020
 *      Author: eddie
 */

/***************************** Include Files *******************************/
#include <drv_i2c.h>

/***************************** Translated APIs *****************************/

int OS_i2c_master_init(void)
{
   return DRV_i2c_master_init();
}

int OS_i2c_master_slv_addr(uint8_t addr)
{
   return DRV_i2c_master_slv_addr(addr);
}

int OS_i2c_master_slv_scan(void)
{
   return DRV_i2c_master_slv_scan();
}

int OS_i2c_master_wr(uint8_t *data, uint32_t len)
{
   return DRV_i2c_master_wr(data, len);
}

int OS_i2c_master_rd(uint8_t *data, uint32_t len)
{
   return DRV_i2c_master_rd(data, len);
}

/***********************************************************/
