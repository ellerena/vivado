/*
 * os_1w.c
 *
 *  Created on: Sep 11, 2021
 *      Author: ellerena
 */

/* required includes */
#include <stdint.h>

#include <drv_1w.h>

/* main API */
void OS_1w_reg_wr(uint32_t reg, uint32_t val)
{
   DRV_1w_reg_wr(reg, val);
}

uint32_t OS_1w_reg_rd(uint32_t reg)
{
   return DRV_1w_reg_rd(reg);
}


void OS_1w_pin_set(int state)
{
   DRV_1w_pin_set(state);
}
