/*
 * i2c.c
 *
 *  Created on: Jul 10, 2020
 *      Author: eddie
 */

/***************************** Include Files **********************************/
#include <i2c.h>

#include "disp_txt_library.h"
#include "SSD1306.h"

/************************** Constant Definitions ******************************/
#define IRQA_BUF_RX_SZ 96
#define IRQA_I2C_ADDR 0x5a
/**************************** Type Definitions ********************************/
/************************** Function Prototypes *******************************/
/************************** Variable Definitions ******************************/
uint8_t irqA_buf_rx[IRQA_BUF_RX_SZ];

XIicPs i2c; /**< Instance of the IIC Device */
int dst_addr;

#ifndef CHKR
#define CHKR(x)      if (x) return XST_FAILURE
#endif

int i2c_master_init(void)
{
   XIicPs_Config *Config;

   /* configure the address for the slave */
   i2c_set_slave_addr(OLED1_I2C_ADR);

   /* Look up the pre-defined configuration for our i2c module */
   CHKR(NULL == (Config = XIicPs_LookupConfig(IIC_DEVICE_ID)));

   /* initialize i2c transceiver */
   CHKR(XIicPs_CfgInitialize(&i2c, Config, Config->BaseAddress));

   /* Set the IIC serial clock rate */
   XIicPs_SetSClk(&i2c, I2C_FREQ);

   /* Wait until bus is idle to start another transfer. */
//   while (XIicPs_BusIsBusy(&i2c)) {/* NOP */}
   /* receive data */
//   CHKR((Status = XIicPs_MasterRecvPolled(&i2c, RecvBuffer,
//           I2C_BUFSZ, IIC_SLAVE_ADDR)));
   display_Init();

   return XST_SUCCESS;
}

int i2c_master_wr(uint8_t *data, int32_t size)
{
   int status;

   /* Send the buffer */
   CHKR((status = XIicPs_MasterSendPolled(&i2c, data, size, dst_addr)));

   /* Wait until bus is idle to start another transfer. */
   while (XIicPs_BusIsBusy(&i2c))
   {/* NOP */
   }

   return status;
}

int i2c_master_rd(uint8_t *data, int32_t size)
{
   int status;

   /* read into buffer */
   CHKR((status = XIicPs_MasterRecvPolled(&i2c, data, size, dst_addr)));

   /* Wait until bus is idle to start another transfer. */
   while (XIicPs_BusIsBusy(&i2c))
   {/* NOP */
   }

   return status;
}

void i2c_set_slave_addr(uint8_t addr)
{
   dst_addr = (addr >> 1);
   xil_printf("i2c address: 0x%x\n", (dst_addr << 1));
}

void i2c_scan(void)
{
   int status;
   int command = 0;

   for (int i = 0; i <= 0x7f; i++)
   {
      status = XIicPs_MasterSendPolled(&i2c, (uint8_t*) &command, 0, i);
      if (status == XST_SUCCESS)
      {
         xil_printf("Ping Ok from 0x%02x\n", i << 1);
      }
   }
}

/////////////////////////////////////////////////////////
static void DumpBytes(uint8_t * data, int len)
{
   int i = 0;
   while (len--)
   {
      PRNF("%02x", *data++);

      if ((15 == (i++ % 16)) || (0 == len))
      {
         PRNF(CRLF);
      }
      else
      {
         PRNF(" ");
      }
   }
}

void irqA_irqHandler(void *data)
{
   int dst_addr_ = dst_addr;
   i2c_set_slave_addr(IRQA_I2C_ADDR);
   i2c_master_rd(irqA_buf_rx, IRQA_BUF_RX_SZ);
   dst_addr = dst_addr_;

   PRNF("[iqA]" CRLF);
   DumpBytes(irqA_buf_rx, IRQA_BUF_RX_SZ);
   PRNF(">");
}
////////////////////////////////////////////////////////////
