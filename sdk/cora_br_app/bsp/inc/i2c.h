/*
 * i2c.h
 *
 *  Created on: Jul 10, 2016
 *      Author: eddie
 */

#ifndef INC_I2C_H_
#define INC_I2C_H_

/* required includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "regs.h"

/* BSP & platform specific includes */
#include "xparameters.h"
#include "xiicps.h"
#include "xil_printf.h"

#define IIC_DEVICE_ID     XPAR_PS7_I2C_0_DEVICE_ID

/*
 * The slave address of some peripherals.
 */
#define OLED_MELIFE       (0x78)
#define OLED_BLUE         (0x7a)

#define OLED1_I2C_ADR      OLED_MELIFE
#define I2C_FREQ           (100000)    /* i2c SCL frequency */
#define I2C_BUFSZ          (128)       /* i2c buffer size */

extern int dst_addr;

int i2c_master_init(void);
int i2c_master_wr(uint8_t *data, int32_t size);
int i2c_master_rd(uint8_t *data, int32_t size);
void i2c_set_slave_addr(uint8_t addr);
void i2c_scan(void);

#endif /* INC_I2C_H_ */
