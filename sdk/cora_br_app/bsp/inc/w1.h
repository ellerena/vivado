/*
 * w1.h
 *
 *  Created on: Jul 16, 2020
 *      Author: eddie
 */

#ifndef INC_DRV_W1_H_
#define INC_DRV_W1_H_

//#include "stdint.h"

#include "w1_regs.h"

/*================================= bit definitions ==================================*/
#define W1_B_START                  (1 << 0)       /* start bit */
#define W1_B_DAT_O                  (1 << 1)       /* w1 data output bit */
#define W1_B_DAT_I                  (1 << 0)       /* w1 data input bit state*/
#define W1_F_DR                     (1 << 1)       /* w1 data ready flag */
#define W1_F_ERROR                  (1 << 2)       /* w1 read error/sensor bad flag */
#define W1_F_AWAKE                  (1 << 3)       /* w1 wake up response ok flag */

#define W1_DATA_OUT_HIGH            (W1_B_DAT_O)   /* command to set w1 pin high */
#define W1_DATA_OUT_LOW             (0)            /* command to set w1 pin low */

/*====================================== macros ======================================*/
#define W1_OUTPUT_HIGH              BR_OUT32(REG_W1_CTRL, W1_DATA_OUT_HIGH)
#define W1_OUTPUT_LOW               BR_OUT32(REG_W1_CTRL, W1_DATA_OUT_LOW)
#define W1_START                    BR_OUT32(REG_W1_CTRL, W1_B_START)
#define W1_RX                       BR_OUT32(REG_W1_CTRL, W1_B_START | W1_DATA_OUT_HIGH)
#define W1_RX_RESET                 BR_OUT32(REG_W1_CTRL, W1_DATA_OUT_HIGH)
#define W1_SET_TRESH(t)             BR_OUT32(REG_W1_TRESH, t)
#define W1_STATUS                   BR_IN32(REG_W1_STA)
#define W1_READ_STATE               (W1_STATUS & W1_B_DAT_I)
#define W1_READ_COMPLETE            (W1_STATUS & W1_F_DR)
#define W1_READ_ERR                 (W1_STATUS & W1_F_ERROR)
#define W1_AWAKE                    (W1_STATUS & W1_F_AWAKE)

extern void w1_reg_wr(uint32_t reg, uint32_t val);
extern uint32_t w1_reg_rd(uint32_t reg);
extern void w1_set_output_pin(int state);
extern int w1_get_data (void * buf);

#endif /* INC_DRV_W1_H_ */
