/*
 * w1_regs.h
 *
 *  Created on: Sep 11, 2021
 *      Author: ellerena
 */

#ifndef BSP_INC_W1_REGS_H_
#define BSP_INC_W1_REGS_H_

#include "xparameters.h"
#include "wire1_rx.h"

/*==========================================================================================
 ================================== REGISTER DEFINITIONS ==================================
 ==========================================================================================*/

#define W1_BASE_ADDR              (XPAR_WIRE1_RX_0_S00_AXI_BASEADDR)

/*==========================================================================================
  =============================== REGISTER OFFSET DEFINITIONS ==============================
  ==========================================================================================*/

#define W1_CTRL_OFFSET            (WIRE1_RX_S00_AXI_SLV_REG0_OFFSET)
#define W1_TRESH_OFFSET           (WIRE1_RX_S00_AXI_SLV_REG3_OFFSET)
#define W1_STA_OFFSET             (WIRE1_RX_S00_AXI_SLV_REG0_OFFSET)
#define W1_DAT_L_OFFSET           (WIRE1_RX_S00_AXI_SLV_REG1_OFFSET)
#define W1_DAT_H_OFFSET           (WIRE1_RX_S00_AXI_SLV_REG2_OFFSET)

/*==========================================================================================
  ================================== REGISTER DEFINITIONS ==================================
  ==========================================================================================*/

#define REG_W1_CTRL               (W1_BASE_ADDR + W1_CTRL_OFFSET)
#define REG_W1_TRESH              (W1_BASE_ADDR + W1_TRESH_OFFSET)
#define REG_W1_STA                (W1_BASE_ADDR + W1_STA_OFFSET)
#define REG_W1_DATL               (W1_BASE_ADDR + W1_DAT_L_OFFSET)
#define REG_W1_DATH               (W1_BASE_ADDR + W1_DAT_H_OFFSET)

#endif /* BSP_INC_W1_REGS_H_ */
