/*
 * drv_w1.c
 *
 *  Created on: Jul 16, 2020
 *      Author: eddie
 */

#include <w1.h>
#include <stdio.h>
#include "sleep.h"

#include "regs.h"
#include "wire1_rx.h"

/* local functions */

static int w1_capture_run(void)
{
   W1_OUTPUT_HIGH;           /* start freash */
   W1_OUTPUT_LOW;            /* pull W1 output low */
   usleep_A9(17900);         /* wait 17.9ms in this state */
   W1_START;                 /* rise the start signal */
   usleep_A9(100);           /* wait 100us to complete 18ms */
   W1_RX;                    /* release the i/o pin */

   while (!W1_READ_COMPLETE) /* wait for read process to complete */
   {
      if (W1_READ_ERR)       /* if an error occurred, exit */
      {
         return -1;
      }
   }

   return 0;
}

/* external API */

void w1_reg_wr(uint32_t reg, uint32_t val)
{
   BR_OUT32(reg, val);
}

uint32_t w1_reg_rd(uint32_t reg)
{
   return BR_IN32(reg);
}

void w1_set_output_pin(int state)
{
   BR_OUT32(REG_W1_CTRL,
         state ? W1_DATA_OUT_HIGH : W1_DATA_OUT_LOW);
}

int w1_get_data (void * buf)
{
   int result = w1_capture_run(); /* run dht11 capture and transfer */

   if (0 == result) { /* if w1 data read completed ok */

      /* AXI bus can only read 32 bit words, so we'll read
       * and copy bytes over to target */

      uint32_t val = BR_IN32(REG_W1_DATL); /* first 32 bits */
      *(uint32_t*)buf = val;               /* store at destination */
      val = BR_IN32(REG_W1_DATH);          /* next 32 bits (only use 8 lsb) */
      *((uint32_t*)buf + 1) = val & 0xff;  /* append to destination */
   }

   return result;
}
