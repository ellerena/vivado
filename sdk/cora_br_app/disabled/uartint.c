/***************************** Include Files *******************************/
#include "xparameters.h"
#include "xplatform_info.h"
#include "xuartps.h"
#include "xil_exception.h"
#include "xil_printf.h"
#include "xscugic.h"
#include "regs.h"

/************************** Constant Definitions **************************/
#define INTC                XScuGic
#define UART_DEVICE_ID      XPAR_XUARTPS_0_DEVICE_ID
#define INTC_DEVICE_ID      XPAR_SCUGIC_SINGLE_DEVICE_ID
#define UART_INT_IRQ_ID     XPAR_XUARTPS_0_INTR

#define TEST_BUFFER_SIZE    5

/************************** Variable Definitions ***************************/
XUartPs UartPs  ;           /* Instance of the UART Device */
INTC InterruptController;   /* Instance of the Interrupt Controller */

static u8 SendBuffer[TEST_BUFFER_SIZE]; /* Buffer for Transmitting Data */
static u8 RecvBuffer[TEST_BUFFER_SIZE]; /* Buffer for Receiving Data */

volatile int TotalReceivedCount;
volatile int TotalSentCount;
int TotalErrorCount;

void Handler(void *CallBackRef, u32 Event, unsigned int EventData)
{
    /* All of the data has been sent */
    if (Event == XUARTPS_EVENT_SENT_DATA) {
        TotalSentCount = EventData;
    }

    /* All of the data has been received */
    if (Event == XUARTPS_EVENT_RECV_DATA) {
        TotalReceivedCount = EventData;
    }

    /*
     * Data was received, but not the expected number of bytes, a
     * timeout just indicates the data stopped for 8 character times
     */
    if (Event == XUARTPS_EVENT_RECV_TOUT) {
        TotalReceivedCount = EventData;
    }

    /*
     * Data was received with an error, keep the data but determine
     * what kind of errors occurred
     */
    if (Event == XUARTPS_EVENT_RECV_ERROR) {
        TotalReceivedCount = EventData;
        TotalErrorCount++;
    }

    /*
     * Data was received with an parity or frame or break error, keep the data
     * but determine what kind of errors occurred. Specific to Zynq Ultrascale+
     * MP.
     */
    if (Event == XUARTPS_EVENT_PARE_FRAME_BRKE) {
        TotalReceivedCount = EventData;
        TotalErrorCount++;
    }

    /*
     * Data was received with an overrun error, keep the data but determine
     * what kind of errors occurred. Specific to Zynq Ultrascale+ MP.
     */
    if (Event == XUARTPS_EVENT_RECV_ORERR) {
        TotalReceivedCount = EventData;
        TotalErrorCount++;
    }
}

static int SetupInterruptSystem(INTC *pGic,
                XUartPs *pUart, u16 UartIrq)
{
    int Status;

    XScuGic_Config *iCnf;

    iCnf = XScuGic_LookupConfig(INTC_DEVICE_ID);
    TSTRLN(NULL == iCnf);
    Status = XScuGic_CfgInitialize(pGic, iCnf, iCnf->CpuBaseAddress);
    TSTRLN(Status != XST_SUCCESS);
    Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
                (Xil_ExceptionHandler) XScuGic_InterruptHandler, pGic);

    Status = XScuGic_Connect(pGic, UartIrq,
                  (Xil_ExceptionHandler) XUartPs_InterruptHandler, (void *) pUart);
    TSTRLN(Status != XST_SUCCESS);

    XScuGic_Enable(pGic, UartIrq);
    Xil_ExceptionEnable();

    return XST_SUCCESS;
}

int UartPsIntrExample(INTC *IntcInstPtr, XUartPs *UartInstPtr,
            u16 DeviceId, u16 UartIntrId)
{
    int Status, Index, BadByteCount = 0;
    XUartPs_Config *Config;
    u32 IntrMask;

    Config = XUartPs_LookupConfig(DeviceId);
    TSTRLN (NULL == Config);

    Status = XUartPs_CfgInitialize(UartInstPtr, Config, Config->BaseAddress);
    TSTRLN (Status != XST_SUCCESS);

    Status = XUartPs_SelfTest(UartInstPtr);
    TSTRLN (Status != XST_SUCCESS);

    Status = SetupInterruptSystem(IntcInstPtr, UartInstPtr, UartIntrId);
    TSTRLN (Status != XST_SUCCESS);

    XUartPs_SetHandler(UartInstPtr, (XUartPs_Handler)Handler, UartInstPtr);

    IntrMask = XUARTPS_IXR_RXFULL \
       | XUARTPS_IXR_TOUT | XUARTPS_IXR_PARITY | XUARTPS_IXR_FRAMING |
        XUARTPS_IXR_OVER | XUARTPS_IXR_TXEMPTY | XUARTPS_IXR_RXOVR;

    XUartPs_SetInterruptMask(UartInstPtr, IntrMask);
    XUartPs_SetOperMode(UartInstPtr, XUARTPS_OPER_MODE_LOCAL_LOOP);
    XUartPs_SetRecvTimeout(UartInstPtr, 8);

    for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
        SendBuffer[Index] = Index + 'A';
        RecvBuffer[Index] = 0;
    }

    XUartPs_Recv(UartInstPtr, RecvBuffer, TEST_BUFFER_SIZE);
    XUartPs_Send(UartInstPtr, SendBuffer, TEST_BUFFER_SIZE);

    while (1) {
        if ((TotalSentCount == TEST_BUFFER_SIZE) &&
            (TotalReceivedCount == TEST_BUFFER_SIZE)) {
            break;
        }
    }

    for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
        if (RecvBuffer[Index] != SendBuffer[Index]) {
            BadByteCount++;
        }
    }

    XUartPs_SetOperMode(UartInstPtr, XUARTPS_OPER_MODE_NORMAL);
    if (BadByteCount != 0) {
        return XST_FAILURE;
    }

    return XST_SUCCESS;
}

int main(void)
{
    int Status;

    Status = UartPsIntrExample(&InterruptController, &UartPs,
                UART_DEVICE_ID, UART_INT_IRQ_ID);
    if (Status != XST_SUCCESS) {
        xil_printf("UART Interrupt Example Test Failed\r\n");
        return XST_FAILURE;
    }

    xil_printf("Successfully ran UART Interrupt Example Test\r\n");
    return XST_SUCCESS;
}
