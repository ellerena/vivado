/*
 * main.c
 *
 *  Created on: Oct 31, 2018
 *      Author: Eddie Llerena
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define INSRC           stdin
#define BUFFER_LENGTH   (256)      ///< The buffer length (crude but fine)
static char buf[BUFFER_LENGTH] = {0};     ///< The receive buffer from the LKM

int main()
{
    int dst = 0;
    ssize_t len;

    printf("destination file:");
    fgets(buf, BUFFER_LENGTH, stdin);
    buf[strlen(buf)-1] = 0;
    if((dst = open(buf, O_WRONLY))<0)
    {
        printf("Failed %d\n", __LINE__);
        goto endp;
    }

    while(1)
    {
        if(fread (&len, 4, 1, INSRC) < 0)   /* read packet size */
        {
            printf("Failed %d\n", __LINE__);
            break;
        }
        if(!len) break;
        if(fread(buf, len, 1, INSRC) < 0)   /* read data packet */
        {
            printf("Failed %d\n", __LINE__);
            break;
        }
        if(write(dst, buf, len) < 0)     /* write data packet */
        {
            printf("Failed %d\n", __LINE__);
            break;
        }
        printf(".");
        if(len < BUFFER_LENGTH) break;
    }

endp:
    if(dst > 0) close (dst);

    printf("rx end\n");
    return 0;
}

