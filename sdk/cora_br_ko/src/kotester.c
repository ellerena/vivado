/*
 * main.c
 *
 *  Created on: Oct 31, 2018
 *      Author: Eddie Llerena
 */

/**
 * kotester is a simple tool to test kernel module.
 * after loading a kernel module, use kotester to
 * run a simple write/read test for any '/dev' or
 * '/sys' device.
 *
 * this module is used in brextern0 repo to generate
 * kotester_nosvc which is included as a 'common'
 * tool ik all our linux builds
 * */

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#define COM_RST         (0)
#define COM_K           (1)
#define COM_CAP         (2)
#define COM_DMP         (3)

#define BUFFER_LENGTH   (100)       ///< The buffer length (crude but fine)
static char buf[BUFFER_LENGTH] = {0};     ///< The receive buffer from the LKM

int main()
{
   unsigned sel, i, cnt;
   int dat = 0;

   printf("Kernel Module Dev Tester %s\n", __DATE__ " " __TIME__);

   /* read from user the device name to use */
   printf("Device name:");
   fgets(buf, 50, stdin);     /* read device name (includes EOL) */
   buf[strlen(buf)-1] = 0;    /* null end the string */
   printf("Opening %s [%zu]\n", buf, strlen(buf));

   /* open the device as a file */
   if ((dat = open(buf, O_RDWR)) < 0)
      goto fin;

   while(1) {

      /* present and read our selection menu */
      printf("\n0:pr 1:pk 2:pc 3:pd #");
      scanf("%u", &sel);
      if(sel > COM_DMP) break;   /* any entry above 3 exits the break loop */

      write(dat, &sel, 1);                       /* writes task to device */
      cnt = pread(dat, buf, BUFFER_LENGTH, 0);   /* read response */

      printf("\tReceived: %u\n\t", cnt);
      if ((sel ==  COM_K) || (sel == COM_CAP))   /* read as uint32_t */
         for (i = 0; i < cnt; i+=4)
            printf("%08x, ", *(unsigned*)&buf[i]);
      else
         printf("%s", buf);
   }

   if(dat > 0) close (dat);

fin:
   printf("End of the program\n");
   return 0;
}


