/*
 * spi.c
 *
 *  Created on: Mar 16, 2020
 *      Author: eddie
 */

/***************************** Include Files **********************************/
#include "xparameters.h"
#include "xspi.h"

/************************** Constant Definitions ******************************/
#define SPI_DEVICE_ID       XPAR_SPI_0_DEVICE_ID
#define BUFSPI				12

extern XSpi_Config XSpi_ConfigTable[];

int SpiSelfTestExample(u16 DeviceId);

/************************** Variable Definitions ******************************/
XSpi Spi;									/* The instance of the SPI device */
u8 ReadBuffer[BUFSPI];
u8 WriteBuffer[BUFSPI];

/*****************************************************************************/
int Spi_Initialize(void)
{
	int Status;
	XSpi_Config *ConfigPtr = &XSpi_ConfigTable[0];

	Status = XSpi_CfgInitialize(&Spi, ConfigPtr, ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		xil_printf("Spi Initialize failed!\n\r");
	}

	Status = XSpi_SetOptions(&Spi, XSP_MASTER_OPTION);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XSpi_SetSlaveSelect(&Spi, 0);
	XSpi_Start(&Spi);

	return XST_SUCCESS;
}

void Spi_TxRx(u8 * data, u32 len)
{
	XSpi_IntrGlobalDisable(&Spi);
	XSpi_SetSlaveSelect(&Spi, 1);
	XSpi_Transfer(&Spi, data, data, len);
//	XSpi_SetSlaveSelect(&Spi, 0);
}

void Spi_TxRxByte(u32 data)
{
	XSpi_IntrGlobalDisable(&Spi);
	XSpi_SetSlaveSelect(&Spi, 1);
	XSpi_Transfer(&Spi, (u8*)&data, (u8*)&data, 1);
//	XSpi_SetSlaveSelect(&Spi, 0);
}

