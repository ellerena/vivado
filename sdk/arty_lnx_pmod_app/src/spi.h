/*
 * spi.h
 *
 *  Created on: Mar 16, 2020
 *      Author: eddie
 */

#ifndef SRC_SPI_H_
#define SRC_SPI_H_

int Spi_Initialize(void);
void Spi_TxRxByte(u32 data);
void Spi_TxRx(u8 * data, u32 len);

#endif /* SRC_SPI_H_ */
