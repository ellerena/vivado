/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "timers.h"

#include "InfoModule.h"
#include "time_interval.h"
#include "spisnif.h"
#include "spi.h"
#include "uart.h"
#include "gpio.h"
#include "drv_i2c.h"
#include "command_handlers.h"

#include "xil_printf.h"

#define PRN                   xil_printf
#define TIMER_ID              1
#define CRLF                  "\n"
#define TIK_PER_MS(x)         (x / portTICK_PERIOD_MS)

extern void timer0 (TimerHandle_t);
extern void thread0 (void *);
extern void thread1 (void *);
extern void th_irq_gpio(void*);
extern void th_mainproc(void*);

TaskHandle_t htask0 = NULL;
TaskHandle_t htask1 = NULL;
TaskHandle_t htask_mainproc = NULL;
static QueueHandle_t hqueue0 = NULL;
static TimerHandle_t htimer0 = NULL;
static SemaphoreHandle_t hmtx0;

volatile uint32_t gcount = 0;
volatile unsigned int gFlags = 0;

int main( void )
{
   PRN( "Zynq - FreeRtos - " __DATE__ " " __TIME__  CRLF);

   hmtx0 = xSemaphoreCreateMutex();
   hqueue0 = xQueueCreate(30, sizeof(uint32_t));

   htimer0 = xTimerCreate("tim0", TIK_PER_MS(10000), pdFALSE,
                         (void*)1, timer0);

   xTaskCreate(thread0, "thread0", configMINIMAL_STACK_SIZE,
               NULL, tskIDLE_PRIORITY,	&htask0 );

   xTaskCreate(thread1, "thread1", configMINIMAL_STACK_SIZE,
               NULL, tskIDLE_PRIORITY,	&htask1 );

   xTaskCreate(th_mainproc, "th_mainproc", configMINIMAL_STACK_SIZE,
                     NULL, tskIDLE_PRIORITY,  &htask_mainproc );

   xTimerStart( htimer0, 0 );
   vTaskStartScheduler();

   for( ;; )
       PRN("main\n");                       	/* If insufficient FreeRTOS heap memory available */
}

void thread0( void *parg )
{
   for(;;)
   {
      xSemaphoreTake(hmtx0, portMAX_DELAY);
      xQueueSend( hqueue0, (void*)&gcount, 0UL );
      PRN("thread0\t0x%08x\n" , gcount++);
      xSemaphoreGive(hmtx0);
      vTaskDelay(TIK_PER_MS(500));
   }
}

void thread1( void *parg )
{
   uint32_t qdata;
   for(;;)
   {
      vTaskDelay(TIK_PER_MS(2000));
      //if (PB_ANY_ON)
      {
         xSemaphoreTake(hmtx0, portMAX_DELAY);
         xQueueReceive(hqueue0, &qdata, portMAX_DELAY);
         PRN("thread1\t0x%08x\t<===\n" , qdata);
         xSemaphoreGive(hmtx0);
      }
   }
}

void th_mainproc(void *parg)
{
    u32 i;
    char s[] = "\nCora Z7 build " __DATE__ " " __TIME__ CRLF;

//    init_platform();
    gpio_init();
    TSTPRNS(uart_Initialize(), "UArt");
//    TSTPRN(SetupInterruptSystem(), "IntC");
    TSTPRNS(uart_IrqInit(), "UartInt");
//    usleep(500);
    TSTPRNS(spi_Initialize(), "Spi");

    i2c_master_init();

    PRNF("%s", s);
    PRNF("HW build %08x: %08x %08x" CRLF ">", BR_IN32(REG_INFO_HASH), BR_IN32(REG_INFO_DATE), BR_IN32(REG_INFO_TIME));

    BR_OUT32(REG_1_TIM, 0x80);
    while (1)
    {
        vTaskSuspend(NULL);
        if (gFlags) {
            if (GFL(F_UART)) {
               CommandProcessor((char*)acCommand);
               GFLC(F_UART);
               PRNF("%s\n>", acCommand);
         }
            if (GFL(F_PB)) {
                if(PB_0_ON) {
                    gpio_ledrotate(LED0);
//                    BR_OUT32(REG_1_TIM, i&0xff);
//                    BR_OUT32(REG_0_TIM, 0x1);       /* reset TIM */
//                    BR_OUT32(REG_0_TIM, 0x0);       /* release reset */
//                    BR_OUT32(REG_0_TIM, 0x2);
//                    BR_OUT32(REG_0_TIM, 0x0);
//                    i = cod2bin(BR_IN32(REG_0_TIM));
//                    PRNF("tim val:%u\t", i);
//                    i = inbyte();
//                    PRNF("%u" CRLF, i);
                }

                if(PB_1_ON) {
                    gpio_ledrotate(LED1);
                }

                debounce(i, PB_ANY_ON);
                GFLC(F_PB);
            }
        }
    }

}

void timer0 ( TimerHandle_t htimer )
{
	long lTimerId;
	configASSERT( htimer );

	lTimerId = ( long ) pvTimerGetTimerID( htimer );

	if (lTimerId != 1) {
		PRN("FreeRTOS Hello World Example FAILED");
	}

	/* If the RxtaskCntr is updated every time the Rx task is called. The
	 Rx task is called every time the Tx task sends a message. The Tx task
	 sends a message every 1 second.
	 The timer expires after 10 seconds. We expect the RxtaskCntr to at least
	 have a value of 9 (TIMER_CHECK_THRESHOLD) when the timer expires. */
//	if (RxtaskCntr >= TIMER_CHECK_THRESHOLD) {
		PRN("FreeRTOS Hello World Example PASSED\n");
//	} else {
//		PRN("FreeRTOS Hello World Example FAILED\n");
//	}

	vTaskDelete( htask0 );
	vTaskDelete( htask1 );
}

