/*
 * uart.c
 *
 *  Created on: Mar 12, 2020
 *      Author: eddie
 */

#include "regs.h"
#include "uart.h"

extern int gFlags;

unsigned char gComIn[RXBUFSZ] = {'0'};

static void COM_init_port (void)
{

}

void COM_init(void)
{

    COM_init_port();

}

//void __attribute__ ((interrupt)) INT_Excep_SCI4_RXI4(void)
//{
//    uint8_t t;
//    static uint8_t *pacCommand = gComIn;                    /* pointer used to write characters recvd by UART1 */
//
//    t = SCI4.RDR;
//    *pacCommand++ = t;
//    if (t < 32)                                     /* <return> ends the uart command entry */
//    {
//        pacCommand = gComIn;
//        gFlags |= FCOMRX;                           /* rise a flag to alert main() */
//    }
//}

//static void IntrHandler(void *CallBackRef, u32 Event, u32 EventData)
//{
//    BR_OUT32(REG_LEDS, 0xff);
//    flags = 1;
//    if (Event == XUARTPS_EVENT_RECV_DATA)
//    {
//        PRNF("%c" CRLF, XUartPs_RecvByte(STDIN_BASEADDRESS));
//    }
//}

__attribute__ ((fast_interrupt)) void UART_lite_handler(void)
{
    uint8_t t;
    static uint8_t *pacCommand = gComIn;   /* pointer used to write characters recvd by UART1 */

    if(BR_IN32(REG_ULI_STATUS) & XUL_SR_RX_FIFO_VALID_DATA)
    {
        t = (unsigned char)BR_IN32(REG_ULI_RX);
        *pacCommand++ = t;
        outbyte(t);
        if (t < 32)                            /* <return> ends the uart command entry */
        {
            *pacCommand = 0;
            pacCommand = gComIn;
            gFlags |= FCOMRX;                  /* rise a flag to alert main() */
            outbyte(0xA);
        }
    }
}
