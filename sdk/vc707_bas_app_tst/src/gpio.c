/**
	@file		BR_gpioc
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "BR_gpio.h"

extern int gFlags;

/**
 *	@brief	Routine to initialize the devices that are controlled via GPIO: LCD, LED, Push buttons, DIP switches.
 *			Since on the FPGA side these devices are all handled from the memory space, we
 *			perform a single initialization for them all together.
 */
void BR_GPIO_init(void)
{
	PUSH_SET_DIR(0xff);					/* set PUSH button ports as input */
	DIPS_SET_DIR(0xff);					/* set DIP SW ports as input */
	LCD_SET_DIR(0x80);					/* set LCD pins as output */
	LED_SET_DIR(0x00);					/* set LED pins as output */
	LCD_WR_DATA32(0x00);				/* write to LCD pins */
}

__attribute__ ((fast_interrupt)) void GPIO1_Handler(void)
{
    if(BR_IN32(REG_PUSH_DATA))
    {
        BITC(PB_SW_BASE_ADDR + XGPIO_IER_OFFSET, XGPIO_IR_CH2_MASK);
        GFLS(FPB);
    }
    BR_OUT32(PB_SW_BASE_ADDR + XGPIO_ISR_OFFSET, XGPIO_IR_CH2_MASK);
}

