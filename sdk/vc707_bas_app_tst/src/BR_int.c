/**
	@file		BR_int.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		Interrupt controller

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "xintc.h"
#include "uart.h"
#include "BR_int.h"
#include "BR_gpio.h"

extern __attribute__ ((fast_interrupt)) void UART_lite_handler(void);
extern __attribute__ ((fast_interrupt)) void GPIO1_Handler(void);

#define	IC_IRQ_MASK (\
        INT_M_TIM0_IRQ        /* systick timer (timer 0) */\
		| INT_M_PUSH_IRQ		/* push buttons mask */\
		| INT_M_ULI_IRQ			/* UART console mask */)
//		| INT_M_TIM1_IRQ        /* timer 1 */
//		| INT_M_DMA_IRQ         /* DMA */

#define	IC_MER_INIT	(\
        XIN_INT_HARDWARE_ENABLE_MASK \
		| XIN_INT_MASTER_ENABLE_MASK \
        )

#if 0
__attribute__ ((fast_interrupt)) void FTDI_Handler(void)
{
	u32 temp;

	temp = FT_RD_REG32(REG_FT_STATUS);
	xil_printf("FT_Status: 0x%08x\n", temp);

	temp &= FT_STATUS_RX_COUNT_M;
	if(temp)
	{
		u32 i;
		u8 * pDstBuffer;

		pDstBuffer = gFT_RX_buffer;
		for(i = 0; i < temp; i++)
		{
			* pDstBuffer++ = FT_RD_REG8(REG_FT_READ_RX);
		}

		* pDstBuffer = 0;
		xil_printf("<%s\n", gFT_RX_buffer);
	}
}

__attribute__ ((fast_interrupt)) void Systick_Handler(void)
{
    SYSTICK++;
    if (TIM1_COUNT_VALUE) TIM1_COUNT_VALUE--;
    if (STMREFRESH) STMREFRESH--;
//    if (FINGERTIMER) FINGERTIMER--;
    BR_TIMER_Start();
}
#endif

void BR_Intc_Initialize(void)
{
    u32 *ptemp;

    ptemp = (u32*)INT_BASE_ADDR;

	WR_OFF32(INT_MER_OFFSET, 0);							/* Stop the controller */
	WR_OFF32(INT_IER_OFFSET, 0);							/* Disable all IRQs */
	WR_OFF32(INT_IAR_OFFSET, ~0);	        		    	/* Clear all pending IRQs */

	WR_OFF32(INT_IMR_OFFSET, 0x1f);						    /* All IRQs in Fast mode */
	WR_OFF32(INT_ILR_OFFSET, 0x1f);

	/* set irq handler vector table */
	WR_OFF32(INT_PUSH_VEC_OFFSET, (u32)(GPIO1_Handler));
	WR_OFF32(INT_ULI_VEC_OFFSET, (u32)(UART_lite_handler));
//	WR_OFF32(INT_TIM0_VEC_OFFSET, (u32)(Systick_Handler));

	WR_OFF32(INT_MER_OFFSET, IC_MER_INIT);
	WR_OFF32(INT_IER_OFFSET, IC_IRQ_MASK);

	/* enable each device's irq */
	BR_OUT32(PB_SW_BASE_ADDR + XGPIO_IER_OFFSET, XGPIO_IR_CH2_MASK);
	BR_OUT32(PB_SW_BASE_ADDR + XGPIO_GIE_OFFSET, XGPIO_GIE_GINTR_ENABLE_MASK);
	BR_OUT32(REG_ULI_CTRL, XUL_CR_ENABLE_INTR);

	microblaze_enable_interrupts();
}


