/**
 * Main.c
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xgpio.h"

#include "regs.h"
#include "InfoModule.h"
#include "BR_timer.h"
#include "BR_gpio.h"
#include "BR_lcd.h"
#include "BR_int.h"
#include "uart.h"

#define CRLF                "\n"
#define SW_VER              "0.0.0 "

int gFlags = 0;

int main()
{
    init_platform();
    BR_TIMER_init();
    BR_GPIO_init();
    BR_LCD_init();
    BR_Intc_Initialize();

    LED_WR_DATA32(0x81);
    PRNF("\nVC707_Bas v. "SW_VER __DATE__ " " __TIME__ CRLF "HW build %08x: %08x %08x" CRLF, \
         BR_IN32(REG_INFO_HASH), BR_IN32(REG_INFO_DATE), BR_IN32(REG_INFO_TIME));
    BR_TIMER_delay_ms(500);
    BR_LCD_clear();
    BR_LCD_puts(0, "VC707 ver." SW_VER);
    BR_LCD_puts(1, __DATE__);
    while(1)
    {
        if (GFL(FCOMRX))
        {
            LED_WR_DATA32(1^BR_IN32(REG_LED_DATA));
            GFLC(FCOMRX);
        }

        if (GFL(FPB))
        {
            PRNF("Pb %08X\n", BR_IN32(REG_PUSH_DATA));
            for(int i = 0; i < 500; i++)
            {
                if (BR_IN32(REG_PUSH_DATA))
                    i = 0;
            }
            GFLC(FPB);
            BITS(PB_SW_BASE_ADDR + XGPIO_IER_OFFSET, XGPIO_IR_CH2_MASK);
        }
    }

    cleanup_platform();
    return 0;
}
