/**
	@file		BR_timer.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "regs.h"
#include "BR_timer.h"

#define TMR0_CONFIG_MSK (XTC_CSR_DOWN_COUNT_MASK | XTC_CSR_ENABLE_INT_MASK | XTC_CSR_AUTO_RELOAD_MASK)
#define TMR0_PERIOD     TIM_MS_TICKS(TMR0_PERIOD_MS)  //100 ticks/second
#define TMR0_PERIOD_MS  (10)

#define TIM_US_TICKS(x)      (x * (XPAR_CPU_CORE_CLOCK_FREQ_HZ/1000000))
#define TIM_MS_TICKS(x)      (x * (XPAR_CPU_CORE_CLOCK_FREQ_HZ/1000))

/*==================================== External API ====================================*/
/**
 * Initialize timer.
 *
 * @return The XST_* status, will be XST_SUCCESS on success.
 */
extern u32 BR_TIMER_init(void)
{
	volatile u32 *ptemp;

	ptemp = (u32*)TIM_BASE_ADDR;
	WR_OFF32(TIM_TLR0_OFFSET, 0);
	WR_OFF32(TIM_TCSR0_OFFSET, XTC_CSR_INT_OCCURED_MASK | XTC_CSR_LOAD_MASK);
	WR_OFF32(TIM_TCSR0_OFFSET, 0);

	WR_OFF32(TIM_TLR1_OFFSET, 0);
	WR_OFF32(TIM_TCSR1_OFFSET, XTC_CSR_INT_OCCURED_MASK | XTC_CSR_LOAD_MASK);
	WR_OFF32(TIM_TCSR1_OFFSET, 0);

	WR_OFF32(TIM_TLR0_OFFSET, TMR0_PERIOD);      // Set timer 0 for periodic tick
	WR_OFF32(TIM_TCSR1_OFFSET, TMR0_CONFIG_MSK); // timer 0 MUST only be started after irq handler is installed

	WR_OFF32(TIM_TCSR1_OFFSET, XTC_CSR_DOWN_COUNT_MASK);    // Set timer 1 for time delay operations
	return 0;
}

/**
 * Delay in microseconds. Will block waiting for timer expiration.
 *
 * @param[in] time The number of microseconds to delay.
 */
extern void BR_TIMER_delay_us(u32 time)
{
	u32 csr;
	volatile u32 *ptemp;

	ptemp = (u32*)TIM_BASE_ADDR;
	WR_OFF32(TIM_TLR1_OFFSET, TIM_US_TICKS(time));	        	/* set the reset value */
	csr = RD_OFF32(TIM_TCSR1_OFFSET) | XTC_CSR_ENABLE_TMR_MASK;
	WR_OFF32(TIM_TCSR1_OFFSET, XTC_CSR_LOAD_MASK);
	WR_OFF32(TIM_TCSR1_OFFSET, csr);

	do
	{
		csr = RD_OFF32(TIM_TCSR1_OFFSET);
	} while(!(csr & XTC_CSR_INT_OCCURED_MASK));

	csr &= ~(XTC_CSR_ENABLE_TMR_MASK);
	WR_OFF32(TIM_TCSR1_OFFSET, csr);
}

/**
 * Delay in milliseconds. Will block waiting for timer expiration.
 *
 * @param[in] time The number of milliseconds to delay.
 */
extern void BR_TIMER_delay_ms(u32 time)
{
	BR_TIMER_delay_us(1000*time);
}

/**
 * Start timeout Delay in milliseconds. Use BR_TIMER_expired() for
 * checking on expiration.
 *
 * @param[in] time The number of milliseconds
 */
void BR_TIMER_start_ms(u32 time)
{
    volatile u32 *ptemp;
    u32 csr;

    ptemp = (u32*)TIM_BASE_ADDR;
    WR_OFF32(TIM_TLR1_OFFSET, TIM_MS_TICKS(time));             /* set the reset value */
    csr = RD_OFF32(TIM_TCSR1_OFFSET) | XTC_CSR_ENABLE_TMR_MASK;
    WR_OFF32(TIM_TCSR1_OFFSET, XTC_CSR_LOAD_MASK);
    WR_OFF32(TIM_TCSR1_OFFSET, csr);
}

/**
 * Check if time has expired. If TRUE, will Stop the timer.
 * Assumes previous call to BR_TIMER_start_ms
 *
 * @return  TRUE if the timer has expired, and FALSE otherwise.
 */
u32  BR_TIMER_expired()
{
    u32 status, csr;
    volatile u32 *ptemp;

    ptemp = (u32*)TIM_BASE_ADDR;
    status = RD_OFF32(TIM_TCSR1_OFFSET) & XTC_CSR_INT_OCCURED_MASK;
    if (status)
    {
        csr = RD_OFF32(TIM_TCSR1_OFFSET);
        csr &= ~(XTC_CSR_ENABLE_TMR_MASK);
        WR_OFF32(TIM_TCSR1_OFFSET, csr);
        status = TRUE;
    }

    return status;
}

/**
 * Start Timer
 *
 * @return  None.
 *
 * @note     None.
 */
void BR_TIMER_Start(void)
{
    volatile u32 *ptemp;
    u32 csr;

    ptemp = (u32*)TIM_BASE_ADDR;
    csr = RD_OFF32(TIM_TCSR1_OFFSET);
    csr |= XTC_CSR_ENABLE_TMR_MASK;
    WR_OFF32(TIM_TCSR1_OFFSET, XTC_CSR_LOAD_MASK);
    WR_OFF32(TIM_TCSR1_OFFSET, csr);
}

