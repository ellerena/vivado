/**
	@file		BR_lcd.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "BR_timer.h"
#include "BR_gpio.h"
#include "BR_lcd.h"

// Due to odd GPIO mapping, a bit reversal is required for data bits
static u32 bit_reverse(u32 v)
{
	u32 r = v; // r will be reversed bits of v; first get LSB of v
	u32 s = sizeof(v) * 32 - 1; // extra shift needed at end

	for (v >>= 1; v; v >>= 1)
	{
	  r <<= 1;
	  r |= v & 1;
	  s--;
	}
	r <<= s; // shift when v's highest bits are zero

	return r;
}

// Clock the LCD (toggles E)
// Takes in current data to be written
static void BR_LCD_clk(u32 data)
{
	LCD_WR_DATA32(data | LCD_BIT_E);	// Assert clock signal
	BR_TIMER_delay_us(50);

	LCD_WR_DATA32(data & ~LCD_BIT_E);	// De-assert the clock signal
	BR_TIMER_delay_us(50);
}

// Write a byte to LCD (4 bit mode)
static void BR_LCD_write(u32 c)
{
	u32 temp;

	temp = ((bit_reverse(c) >> 21) & 0x78);	// Reverse bits, set the high nibble

	LCD_WR_DATA8(temp);
	BR_LCD_clk(temp);

	temp &= 0x87;								// Reset data bits
	temp |= ((bit_reverse(c) >> 25) & 0x78);	// Reverse bits, set the low nibble

	LCD_WR_DATA8(temp);
	BR_LCD_clk(temp);

}

static void BR_LCD_data_write(u32 c)
{
	u32 temp;

	temp = (((bit_reverse(c) >> 21) & 0x78) | LCD_BIT_RS);		// Reverse bits, set the high nibble

	LCD_WR_DATA8(temp);
	BR_LCD_clk(temp);

	temp &= 0x87;												// Reset data bits
	temp |= (((bit_reverse(c) >> 25) & 0x78) | LCD_BIT_RS);		// Reverse bits, set the low nibble

	LCD_WR_DATA8(temp);
	BR_LCD_clk(temp);
}

/*==================================== External API ====================================*/

// Clear LCD
void BR_LCD_clear(void)
{
	BR_TIMER_delay_us(100);
	BR_LCD_write(0x01);	// Clear LCD
	BR_TIMER_delay_ms(5);		// Delay for "Clear display 1.53ms"
}

// Write a string to the LCD
void BR_LCD_puts(u32 line, const char * s)
{
  BR_LCD_goto(line, 0);
  while(*s)
  {
	  BR_LCD_data_write(*s++);
  }
}

// Write character to the LCD
void BR_LCD_putch(u32 c)
{
  BR_LCD_data_write(c);
}

// Change cursor position
// (line = 0 or 1, pos = 0 to 15)
void BR_LCD_goto(u32 line, u32 pos)
{
	pos &= 0x3F;
	pos |= (line ? 0xC0 : 0x80);
	BR_LCD_write(0x80 | pos);
}

// Initialize the LCD
void BR_LCD_init(void)
{
	u32 temp;

	LCD_WR_DATA32(0x00);
	BR_TIMER_delay_ms(30);

	temp = (LCD_BIT_DB5 | LCD_BIT_DB4);	// Initial sequence to init 4 bit mode according to spec
	LCD_WR_DATA8(temp);					// Start in 8 bit mode
	BR_LCD_clk(temp);
	BR_TIMER_delay_ms(5);
	BR_LCD_clk(temp);
	BR_TIMER_delay_ms(1);
	BR_LCD_clk(temp);

	temp = LCD_BIT_DB5;					// Change to 4 bit mode
	LCD_WR_DATA32(temp);
	BR_TIMER_delay_ms(1);
	BR_LCD_clk(temp);

	BR_TIMER_delay_ms(5);						// Delay 5ms
	BR_LCD_write(0x28);					// Function Set: 4 bit mode, 1/16 duty, 5x8 font, 2 lines
	BR_LCD_write(0x08);					//Turn Display: OFF
	BR_LCD_clear();						//Clear LCD
	BR_LCD_write(0x06);					// Entry Mode Set: Increment (cursor moves forward)
	BR_LCD_write(0x0C);					// Display ON/OFF Control: ON
	BR_LCD_clear();						// Clear the display

	BR_LCD_puts(1, "b:" __TIME__);
	BR_TIMER_delay_ms(1000);
}




