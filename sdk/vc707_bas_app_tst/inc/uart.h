/*
 * uart.h
 *
 *  Created on: Mar 12, 2020
 *      Author: eddie
 */

#ifndef INC_UART_H_
#define INC_UART_H_

#include "xuartlite_l.h"

#define RXBUFSZ         (60)

#define FCOMRX          (1 << 0)

extern unsigned char gComIn[RXBUFSZ];

#endif /* INC_UART_H_ */
