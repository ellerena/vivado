/*
 * regs.h
 *
 *  Created on: Oct 3, 2016
 *      Author: eddie.llerena
 */

#ifndef __BR_REGS_H__
#define __BR_REGS_H__

#ifdef __GNUC__
#include "xparameters.h"
#include "xil_types.h"
#endif

/*********************************** ALIASES FOR FW ***********************************/
#ifdef __LOGGING_H__
#undef ERRORPRINTFV
#undef ERRORPRINTF
#undef PRINTF
#undef PRINTFV
#else
#define __LOGGING_H__
#endif
#define ERRORPRINTFV   		            xil_printf
#define ERRORPRINTF         	        xil_printf
#define	PRNF							xil_printf
#define PRNL                            /* print raw LCD */
#define PRNS                            print    /* print raw bites serial */
#define PRNT(x, y)                      if(x) PRNF(y CRLF);
#define PRINTF(format)
#define PRINTFV(format, ...)

/*==========================================================================================*/
/*================================== REGISTER DEFINITIONS ==================================*/
/*==========================================================================================*/
#define DDR_BASE_ADDR					(XPAR_MIG_7SERIES_0_BASEADDR)		/* external DDR3L 256MB */
#define	DMA_BASE_ADDR					(XPAR_AXI_CDMA_0_BASEADDR)
#define	LCD_LED_BASE_ADDR				(XPAR_GPIO_0_BASEADDR)
#define	PB_SW_BASE_ADDR					(XPAR_GPIO_1_BASEADDR)
#define	TIM_BASE_ADDR					(XPAR_AXI_TIMER_0_BASEADDR)
#define TIM2_BASE_ADDR                  (XPAR_AXI_TIMER_1_BASEADDR)
#define ULI_BASE_ADDR                   (XPAR_AXI_UARTLITE_0_BASEADDR)
#define	INT_BASE_ADDR					(XPAR_MICROBLAZE_0_AXI_INTC_BASEADDR)	/* interrupt controller base address */
#define IIC_BASE_ADDR                   (XPAR_AXI_IIC_0_BASEADDR)           /* i2c controller */
#define INFO_BASE_ADDR                  (XPAR_INFOMODULE_0_S00_AXI_BASEADDR)

/*==========================================================================================*/
/*=============================== REGISTER OFFSET DEFINITIONS ==============================*/
/*==========================================================================================*/
#define DDR_DBG_OFFSET					(0x00)

#define RD_OFFSET						(0x00)
#define WR_OFFSET						(0x04)
#define STATUS_OFFSET					(0x08)
#define CTRL_OFFSET						(0x0c)
#define VER_OFFSET						(0x10)

#define ULI_RX_OFFSET                   (0x00)
#define ULI_TX_OFFSET                   (0x04)
#define ULI_STATUS_OFFSET               (0x08)
#define ULI_CTRL_OFFSET                 (0x0c)

#define	DMA_CR_OFFSET					(XAXICDMA_CR_OFFSET)
#define	DMA_SR_OFFSET					(XAXICDMA_SR_OFFSET)
#define	DMA_CDESC_OFFSET				(XAXICDMA_CDESC_OFFSET)
#define	DMA_TDESC_OFFSET				(XAXICDMA_TDESC_OFFSET)
#define	DMA_SRCADDR_OFFSET				(XAXICDMA_SRCADDR_OFFSET)
#define	DMA_DSTADDR_OFFSET				(XAXICDMA_DSTADDR_OFFSET)
#define	DMA_BTT_OFFSET					(XAXICDMA_BTT_OFFSET)

#define IIC_DGIER_OFFSET                (XIIC_DGIER_OFFSET)
#define IIC_IISR_OFFSET                 (XIIC_IISR_OFFSET)
#define IIC_IIER_OFFSET                 (XIIC_IIER_OFFSET)
#define IIC_RESETR_OFFSET               (XIIC_RESETR_OFFSET)
#define IIC_CR_OFFSET                   (XIIC_CR_REG_OFFSET)
#define IIC_SR_OFFSET                   (XIIC_SR_REG_OFFSET)
#define IIC_DTR_OFFSET                  (XIIC_DTR_REG_OFFSET)
#define IIC_DRR_OFFSET                  (XIIC_DRR_REG_OFFSET)
#define IIC_ADR_OFFSET                  (XIIC_ADR_REG_OFFSET)
#define IIC_TFO_OFFSET                  (XIIC_TFO_REG_OFFSET)
#define IIC_RFO_OFFSET                  (XIIC_RFO_REG_OFFSET)
#define IIC_TBA_OFFSET                  (XIIC_TBA_REG_OFFSET)
#define IIC_RFD_OFFSET                  (XIIC_RFD_REG_OFFSET)
#define IIC_GPO_OFFSET                  (XIIC_GPO_REG_OFFSET)

#define	LED_DATA_OFFSET					(XGPIO_DATA_OFFSET)
#define	LED_DIR_OFFSET					(XGPIO_TRI_OFFSET)
#define	LCD_DATA_OFFSET					(XGPIO_DATA2_OFFSET)
#define	LCD_DIR_OFFSET					(XGPIO_TRI2_OFFSET)

#define	DIPS_DATA_OFFSET				(XGPIO_DATA_OFFSET)
#define	DIPS_DIR_OFFSET					(XGPIO_TRI_OFFSET)
#define	PUSH_DATA_OFFSET				(XGPIO_DATA2_OFFSET)
#define	PUSH_DIR_OFFSET					(XGPIO_TRI2_OFFSET)

#define TIM_TCSR0_OFFSET                (XTC_TCSR_OFFSET)
#define TIM_TLR0_OFFSET                 (XTC_TLR_OFFSET)
#define TIM_TCR0_OFFSET                 (XTC_TCR_OFFSET)
#define TIM_TCSR1_OFFSET                (TIM_TCSR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM_TLR1_OFFSET                 (TIM_TLR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM_TCR1_OFFSET                 (TIM_TCR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)

#define TIM2_TCSR0_OFFSET               (XTC_TCSR_OFFSET)
#define TIM2_TLR0_OFFSET                (XTC_TLR_OFFSET)
#define TIM2_TCR0_OFFSET                (XTC_TCR_OFFSET)
#define TIM2_TCSR1_OFFSET               (TIM2_TCSR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM2_TLR1_OFFSET                (TIM2_TLR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)
#define TIM2_TCR1_OFFSET                (TIM2_TCR0_OFFSET + XTC_TIMER_COUNTER_OFFSET)

#define INT_ISR_OFFSET                  XIN_ISR_OFFSET      /* Interrupt Status Register */
#define INT_IPR_OFFSET                  XIN_IPR_OFFSET      /* Interrupt Pending Register */
#define INT_IER_OFFSET                  XIN_IER_OFFSET      /* Interrupt Enable Register */
#define INT_IAR_OFFSET                  XIN_IAR_OFFSET      /* Interrupt Acknowledge Register */
#define INT_SIE_OFFSET                  XIN_SIE_OFFSET      /* Set Interrupt Enable Register */
#define INT_CIE_OFFSET                  XIN_CIE_OFFSET      /* Clear Interrupt Enable Register */
#define INT_IVR_OFFSET                  XIN_IVR_OFFSET      /* Interrupt Vector Register */
#define INT_MER_OFFSET                  XIN_MER_OFFSET      /* Master Enable Register */
#define INT_IMR_OFFSET                  XIN_IMR_OFFSET      /* Interrupt Mode Register, only for Fast Interrupt */
#define INT_ILR_OFFSET                  XIN_ILR_OFFSET      /* Interrupt level register */
#define INT_IVAR_OFFSET                 XIN_IVAR_OFFSET     /* Interrupt Vector Address Register */

#define INFO_DATE_OFFSET                (INFOMODULE_S00_AXI_SLV_REG0_OFFSET)
#define INFO_TIME_OFFSET                (INFOMODULE_S00_AXI_SLV_REG1_OFFSET)
#define INFO_VERSION_OFFSET             (INFOMODULE_S00_AXI_SLV_REG2_OFFSET)
#define INFO_PRODUCT_OFFSET             (INFOMODULE_S00_AXI_SLV_REG3_OFFSET)

#define	UBLAZE_CONTROL_OFFSET			(0)

/*==========================================================================================*/
/*================================== REGISTER DEFINITIONS ==================================*/
/*==========================================================================================*/
#define REG_DDR_DBG						(DDR_BASE_ADDR + DDR_DBG_OFFSET)

#define REG_ULI_RX                      (ULI_BASE_ADDR + ULI_RX_OFFSET)
#define REG_ULI_TX                      (ULI_BASE_ADDR + ULI_TX_OFFSET)
#define REG_ULI_STATUS                  (ULI_BASE_ADDR + ULI_STATUS_OFFSET)
#define REG_ULI_CTRL                    (ULI_BASE_ADDR + ULI_CTRL_OFFSET)

#define	REG_DMA_CR						(DMA_BASE_ADDR + DMA_CR_OFFSET)
#define	REG_DMA_SR						(DMA_BASE_ADDR + DMA_SR_OFFSET)
#define	REG_DMA_CDESC					(DMA_BASE_ADDR + DMA_CDESC_OFFSET)
#define	REG_DMA_TDESC					(DMA_BASE_ADDR + DMA_TDESC_OFFSET)
#define	REG_DMA_SRCADDR					(DMA_BASE_ADDR + DMA_SRCADDR_OFFSET)
#define	REG_DMA_DSTADDR					(DMA_BASE_ADDR + DMA_DSTADDR_OFFSET)
#define	REG_DMA_BTT						(DMA_BASE_ADDR + DMA_BTT_OFFSET)

#define REG_IIC_DGIER                   (IIC_BASE_ADDR + IIC_DGIER_OFFSET)
#define REG_IIC_IISR                    (IIC_BASE_ADDR + IIC_IISR_OFFSET)
#define REG_IIC_IIER                    (IIC_BASE_ADDR + IIC_IIER_OFFSET)
#define REG_IIC_RESETR                  (IIC_BASE_ADDR + IIC_RESETR_OFFSET)
#define REG_IIC_CR_REG                  (IIC_BASE_ADDR + IIC_CR_OFFSET)
#define REG_IIC_SR_REG                  (IIC_BASE_ADDR + IIC_SR_OFFSET)
#define REG_IIC_DTR_REG                 (IIC_BASE_ADDR + IIC_DTR_OFFSET)
#define REG_IIC_DRR_REG                 (IIC_BASE_ADDR + IIC_DRR_OFFSET)
#define REG_IIC_ADR_REG                 (IIC_BASE_ADDR + IIC_ADR_OFFSET)
#define REG_IIC_TFO_REG                 (IIC_BASE_ADDR + IIC_TFO_OFFSET)
#define REG_IIC_RFO_REG                 (IIC_BASE_ADDR + IIC_RFO_OFFSET)
#define REG_IIC_TBA_REG                 (IIC_BASE_ADDR + IIC_TBA_OFFSET)
#define REG_IIC_RFD_REG                 (IIC_BASE_ADDR + IIC_RFD_OFFSET)
#define REG_IIC_GPO_REG                 (IIC_BASE_ADDR + IIC_GPO_OFFSET)

#define	REG_LED_DATA					(LCD_LED_BASE_ADDR + LED_DATA_OFFSET)
#define	REG_LED_DIR						(LCD_LED_BASE_ADDR + LED_DIR_OFFSET)
#define	REG_LCD_DATA					(LCD_LED_BASE_ADDR + LCD_DATA_OFFSET)
#define	REG_LCD_DIR						(LCD_LED_BASE_ADDR + LCD_DIR_OFFSET)

#define	REG_DIPS_DATA					(PB_SW_BASE_ADDR + DIPS_DATA_OFFSET)
#define	REG_DIPS_DIR					(PB_SW_BASE_ADDR + DIPS_DIR_OFFSET)
#define	REG_PUSH_DATA					(PB_SW_BASE_ADDR + PUSH_DATA_OFFSET)
#define	REG_PUSH_DIR					(PB_SW_BASE_ADDR + PUSH_DIR_OFFSET)

#define REG_TIM_TCSR0                   (TIM_BASE_ADDR + TIM_TCSR0_OFFSET)
#define REG_TIM_TLR0                    (TIM_BASE_ADDR + TIM_TLR0_OFFSET)
#define REG_TIM_TCR0                    (TIM_BASE_ADDR + TIM_TCR0_OFFSET)
#define REG_TIM_TCSR1                   (TIM_BASE_ADDR + TIM_TCSR1_OFFSET)
#define REG_TIM_TLR1                    (TIM_BASE_ADDR + TIM_TLR1_OFFSET)
#define REG_TIM_TCR1                    (TIM_BASE_ADDR + TIM_TCR1_OFFSET)

#define REG_TIM2_TCSR0                  (TIM2_BASE_ADDR + TIM2_TCSR0_OFFSET)
#define REG_TIM2_TLR0                   (TIM2_BASE_ADDR + TIM2_TLR0_OFFSET)
#define REG_TIM2_TCR0                   (TIM2_BASE_ADDR + TIM2_TCR0_OFFSET)
#define REG_TIM2_TCSR1                  (TIM2_BASE_ADDR + TIM2_TCSR1_OFFSET)
#define REG_TIM2_TLR1                   (TIM2_BASE_ADDR + TIM2_TLR1_OFFSET)
#define REG_TIM2_TCR1                   (TIM2_BASE_ADDR + TIM2_TCR1_OFFSET)

#define REG_INT_ISR_OFFSET              (INT_BASE_ADDR + INT_ISR_OFFSET)        /* Interrupt Status Register */
#define REG_INT_IPR_OFFSET              (INT_BASE_ADDR + INT_IPR_OFFSET)        /* Interrupt Pending Register */
#define REG_INT_IER_OFFSET              (INT_BASE_ADDR + INT_IER_OFFSET)        /* Interrupt Enable Register */
#define REG_INT_IAR_OFFSET              (INT_BASE_ADDR + INT_IAR_OFFSET)        /* Interrupt Acknowledge Register */
#define REG_INT_SIE_OFFSET              (INT_BASE_ADDR + INT_SIE_OFFSET)        /* Set Interrupt Enable Register */
#define REG_INT_CIE_OFFSET              (INT_BASE_ADDR + INT_CIE_OFFSET)        /* Clear Interrupt Enable Register */
#define REG_INT_IVR_OFFSET              (INT_BASE_ADDR + INT_IVR_OFFSET)        /* Interrupt Vector Register */
#define REG_INT_MER_OFFSET              (INT_BASE_ADDR + INT_MER_OFFSET)        /* Master Enable Register */
#define REG_INT_IMR_OFFSET              (INT_BASE_ADDR + INT_IMR_OFFSET)        /* Interrupt Mode Register, only for Fast Interrupt */
#define REG_INT_ILR_OFFSET              (INT_BASE_ADDR + INT_ILR_OFFSET)        /* Interrupt level register */
#define REG_NT_IVAR_OFFSET              (INT_BASE_ADDR + INT_IVAR_OFFSET)       /* Interrupt Vector Address Register */

#define REG_INFO_DATE                   (INFO_BASE_ADDR + INFO_DATE_OFFSET)
#define REG_INFO_TIME                   (INFO_BASE_ADDR + INFO_TIME_OFFSET)
#define REG_INFO_VERSION                (INFO_BASE_ADDR + INFO_VERSION_OFFSET)
#define REG_INFO_HASH                   (INFO_BASE_ADDR + INFO_PRODUCT_OFFSET)

#define	REG_UBLAZE_CONTROL				(UBL_BASE_ADDR + UBLAZE_CONTROL_OFFSET)

/*==========================================================================================*/
/*=============================== Miscelaneous definitions =================================*/
/*==========================================================================================*/
#ifdef UBLAZE_FW
#define	WR_OFF32(x, y)					(*(ptemp + ((x)/4)) = (y))
#define RD_OFF32(x)						(*(ptemp + ((x)/4)))
#define	WR_OFF8(x, y)					(*((u8*)ptemp + (x))) = (y))
#define RD_OFF8(x)						(*((u8*)ptemp + (x)))
#define	BR_OUT32(x, y)					(*(volatile u32*)(x) = (y))
#define	BR_IN32(x)						(*(volatile u32*)(x))
#define	BR_OUT8(x, y)					(*(volatile u8*)(x) = (y))
#define	BR_IN8(x)						(*(volatile u8*)(x))
#else
#define	WR_OFF32(x, y)					Xil_Out32(((u32)ptemp + (x)),(y))
#define RD_OFF32(x)						Xil_In32((u32)ptemp + (x))
#define	WR_OFF8(x, y)					Xil_Out8(((u32)ptemp + (x)),(y))
#define RD_OFF8(x)						Xil_In8((u32)ptemp + (x))
#define	BR_OUT32(x, y)					Xil_Out32((u32)(x),(y))
#define	BR_IN32(x)						Xil_In32((u32)(x))
#define	BR_OUT8(x, y)					Xil_Out8((u32)(x),(y))
#define	BR_IN8(x)						Xil_In8((u32)(x))
#endif

#ifdef PRINTF_VERBOSE
#define DBGPRINTF(...)			       	xil_printf(__VA_ARGS__)
#else
#define DBGPRINTF(...)
#endif

#define st(x)                           do { x } while (__LINE__ == -1)

#define DBGLED                          *(u32*)REG_LED_DATA = dbgled++;
#define DBGLEDSHOW(x)                   *(u32*)REG_LED_DATA = x;

#define BITS(x,y)                       BR_OUT32(x, BR_IN32(x) | y)
#define BITC(x,y)                       BR_OUT32(x, BR_IN32(x) & ~y)
#define GFLS(x)                         gFlags |= x
#define GFLC(x)                         gFlags &= ~x
#define GFL(x)                           (gFlags & (x))

/************************************ Other definitions ************************************/

#endif /* __BR_REGS_H__ */
