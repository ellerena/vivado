/*
 * Copyright 2016 (C) Ellerena, Inc
 *
 */
#ifndef BR_TIMER_H_
#define BR_TIMER_H_

#include "xtmrctr.h"

#define TIM2_RESET                      {BR_OUT32(REG_TIM2_TCSR0, XTC_CSR_LOAD_MASK);\
                                        BR_OUT32(REG_TIM2_TCSR0, XTC_CSR_ENABLE_TMR_MASK);}
#define TIM2_COUNT                      BR_IN32(REG_TIM2_TCR0)

/*
 * Timer 2 macros used only function profiling. Usage as follows:
 *
 * 1) at code point where you want to start measuring insert:
 *    TIM2_RESET;
 *
 * 2) call function or code point to measure
 *
 * 3) at code points(s) where where the elapsed time is desired, use:
 *    ERRORPRINTFV("Timer Value: %08x\n", TIM2_COUNT);
 *
 *    with current clock of 96000000Hz (96MHz), use the following for usec
 *    ERRORPRINTFV("Elapsed Time: %5.2fus\n", (float)(TIM2_COUNT/96) )
 *
 * Example:
 *   TIM2_RESET;
 *   FooBar();
 *   ERRORPRINTFV("FooBar elapsed time: %5.2fus\n", (float)(TIM2_COUNT/(XPAR_CPU_CORE_CLOCK_FREQ_HZ/1000000)) );
 */

uint32_t BR_TIMER_init();
void BR_TIMER_delay_us(uint32_t time);
void BR_TIMER_delay_ms(uint32_t time);
void BR_TIMER_start_ms(uint32_t time);
void BR_TIMER_Start(void);
uint32_t  BR_TIMER_expired();

#endif /* BR_TIMER_H_ */
