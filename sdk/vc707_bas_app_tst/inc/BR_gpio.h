/**
	@file		BR_gpio.h
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#ifndef BR_GPIO_H_
#define BR_GPIO_H_

#include "regs.h"
#include "xgpio_l.h"

/*================================= bit definitions ==================================*/
// Masks to the pins on the GPIO port
//#define LCD_BIT_TEST	0x80		//bit 7
#define LCD_BIT_DB4		0x40		//bit 6
#define LCD_BIT_DB5		0x20		//bit 5
#define LCD_BIT_DB6		0x10		//bit 4
#define LCD_BIT_DB7		0x08		//bit 3
#define LCD_BIT_RW		0x04		//bit 2
#define LCD_BIT_RS		0x02		//bit 1
#define LCD_BIT_E		0x01		//bit 0

#define	LED_ALL_OFF					(0)
#define	LED_USB_ACTIVE				(1 << 0)

#define FPB                         (1 << 1)

/*====================================== masks  ======================================*/
/* Push buttons masks */
#define	PB_ALL_MASK					(0x1F)
#define	PB_NORTH					(0x10)
#define	PB_EAST						(0x08)
#define	PB_SOUTH					(0x04)
#define	PB_WEST						(0x02)
#define	PB_CENTER					(0x01)

#define	DIPS_SET_DIR(x)				BR_OUT32(REG_DIPS_DIR, (x))
#define	PUSH_SET_DIR(x)				BR_OUT32(REG_PUSH_DIR, (x))
#define	LCD_SET_DIR(x)				BR_OUT32(REG_LCD_DIR, (x))
#define	LED_SET_DIR(x)				BR_OUT32(REG_LED_DIR, (x))
#define	LCD_WR_DATA32(x)			BR_OUT32(REG_LCD_DATA, (x))
#define	LED_WR_DATA32(x)			BR_OUT32(REG_LED_DATA, (x))				/* present data on the LED (8 LSB used only) */
#define	LCD_WR_DATA8(x)				LCD_WR_DATA32((x) * 0xff)				/* write data to the LCD output register */
#define	DIPS_RD_DATA32				BR_IN32(REG_DIPS_DATA)					/* returns content of the DIP switches (only 8 LSB are valid) */
#define	PUSH_RD_DATA32				(PB_ALL_MASK & BR_IN32(REG_PUSH_DATA))	/* returns state of push buttons (bit mask defined above) */


/*==================================== External API ====================================*/
/**
 *	@brief	Routine to initialize the devices that are controlled via GPIO: LCD, LED, Push buttons, DIP switches.
 *			Since on the FPGA side these devices are all handled from the memory space, we
 *			perform a single initialization for them all together.
 */
extern void BR_GPIO_init(void);

#endif /* BR_GPIO_H_ */
