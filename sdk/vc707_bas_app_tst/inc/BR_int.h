/*
 * BR_int.c
 *
 *  Created on: Aug 15, 2016
 *      Author: Eddie.Llerena
 */

#ifndef BR_INT_C_
#define BR_INT_C_

#ifdef UBLAZE_FW
#include "regs.h"
#include "BR_int.h"
#else
#endif

/* IRQ vector IDs */
#define INT_ULI_VEC_OFFSET          (INT_IVAR_OFFSET + (4 * XPAR_INTC_0_UARTLITE_0_VEC_ID))
#define INT_TIM0_VEC_OFFSET         (INT_IVAR_OFFSET + (4 * XPAR_INTC_0_TMRCTR_0_VEC_ID))
#define INT_TIM1_VEC_OFFSET         (INT_IVAR_OFFSET + (4 * XPAR_INTC_0_TMRCTR_1_VEC_ID))
#define INT_PUSH_VEC_OFFSET         (INT_IVAR_OFFSET + (4 * XPAR_INTC_0_GPIO_1_VEC_ID))
#define INT_DMA_VEC_OFFSET          (INT_IVAR_OFFSET + (4 * XPAR_INTC_0_AXICDMA_0_VEC_ID))

#define INT_M_ULI_IRQ               (XPAR_AXI_UARTLITE_0_INTERRUPT_MASK)
#define INT_M_TIM0_IRQ              (XPAR_AXI_TIMER_0_INTERRUPT_MASK)
#define INT_M_TIM1_IRQ              (XPAR_AXI_TIMER_1_INTERRUPT_MASK)
#define INT_M_PUSH_IRQ              (XPAR_AXI_GPIO_1_IP2INTC_IRPT_MASK)
#define INT_M_DMA_IRQ               (XPAR_AXI_CDMA_0_CDMA_INTROUT_MASK)

void BR_Intc_Initialize(void);

#endif /* BR_INT_C_ */
