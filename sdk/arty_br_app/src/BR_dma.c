/**
	@file		BR_dma.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		DMA module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "BR_dma.h"
#include "BR_regs.h"
#include "xil_printf.h"

/**
 * @brief	Initializes the Central DMA module
 * */
void BR_DMA_Initialize(void)
{
	u32 temp;
	volatile u32 *ptemp;

	ptemp = (u32*)DMA_BASE_ADDR;
	WR_OFF32(DMA_CR_OFFSET, XAXICDMA_CR_RESET_MASK);			/* reset CDMA block */

	temp = XAXICDMA_RESET_LOOP_LIMIT;
	while(temp--)												/* wait for reset completion */
	{
		while(RD_OFF32(DMA_CR_OFFSET) & XAXICDMA_CR_RESET_MASK);
	}

	WR_OFF32(DMA_CR_OFFSET, DMA_CR_INIT);						/* configure CDMACR register */
	WR_OFF32(DMA_SRCADDR_OFFSET, DMA_SA_INIT);					/* configure CDMASA register */
	WR_OFF32(DMA_DSTADDR_OFFSET, DMA_DA_INIT);					/* configure CDMADA register */
}

/**
 *	@brief	Starts a CDMA transfer of <numbytes> bytes.
 *	@param	numbytes, is the transfer size in bytes
 */
void BR_DMA_Start_Transfer (u32 numbytes)
{
	volatile u32 *ptemp;
	u32 txsize;

	ptemp = (u32*)DMA_BASE_ADDR;
	while(numbytes)															/* loop till all data has been sent */
	{
		txsize = (numbytes > DMA_TX_PAGE) ? DMA_TX_PAGE : numbytes;			/* limit transfers to one page at a time */
		numbytes -= txsize;													/* reduce transfer counter accordingly */
		WR_OFF32(DMA_SR_OFFSET, XAXICDMA_XR_IRQ_ALL_MASK);					/* clear any pending DMA interrupt flags */
		WR_OFF32(DMA_BTT_OFFSET, txsize);									/* set transfer size and start it */
		while (!(XAXICDMA_XR_IRQ_IOC_MASK & RD_OFF32(DMA_SR_OFFSET)));		/* wait for the CDMA to complete the transfer */
	}
}

void DMA_qt(void)
{
    u32 i, *d;
    u8 *s;

    s = (u8*)(DMA_SA_INIT);
    for (i = 0; i < (16*16); i++) {
        *s++ = i;
    }

    BR_DMA_Start_Transfer(16*16);

    d = (u32*)DMA_DA_INIT;
    for (i = 0; i < 16; i+=4) {
        xil_printf("%08x\t%08x\t%08x\t%08x\r\n", d[0], d[1], d[2], d[3]);
        d+=4;
    }
}

