/**
	@file		BR_ft232.h
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#ifndef BR_FT232_H_
#define BR_FT232_H_

#include <stdio.h>
#include "BR_regs.h"
#include "sona_usb.h"

/*=============================== Bit definitions =================================*/
#define	FT_M_INT_RX_EMPTY			(1 << 12)
#define	FT_M_INT_RX_FULL			(1 << 13)
#define	FT_M_INT_NEW_DATA			(1 << 14)
#define	FT_M_INT_TX_EMPTY			(1 << 28)
#define	FT_M_INT_TX_FULL			(1 << 29)
#define	FT_M_INT_CLK_GOOD			(1 << 30)
#define	FT_M_RX_COUNT				(0xFFF)
#define	FT_M_TX_COUNT				(0xFFF << 16)

#define	FT_B_RX_EMPTY				(1 << 12)
#define	FT_B_RX_FULL				(1 << 13)
#define	FT_B_RX_NEW_DATA			(1 << 14)
#define	FT_B_EN_32BIT_WR			(1 << 15)
#define	FT_B_TX_EMPTY				(1 << 28)
#define	FT_B_TX_FULL				(1 << 29)
#define	FT_B_CLK_GOOD				(1 << 30)
#define	FT_B_RESET					(1 << 31)
#define	FT_B_RX_ALMOST_FULL			(0)
#define	FT_B_TX_ALMOST_EMPTY		(0)

/*=============================== Constant definitions =================================*/
#define	FT_RX_BUFFER_SZ				(2048)
#define	FT_TX_BUFFER_SZ				(2048)
#define	FT_RX_BLOCK_SZ				(512)
#define	FT_TX_BLOCK_SZ				(512)

/*================================ Macro definitions ===================================*/
#ifdef BR_USE_32BIT_WRITE
#define	FT_CONTROL_INIT				(FT_B_RX_NEW_DATA | FT_B_EN_32BIT_WR)
#define	FT_WR_TYPE					u32
#define	FT_WR_SZ					4
#define	FT_WR_FN					WR_OFF32
#else
#define	FT_CONTROL_INIT				(FT_B_RX_NEW_DATA)
#define	FT_WR_TYPE					u8
#define	FT_WR_SZ					1
#define	FT_WR_FN					WR_OFF8
#endif

#define	FT_RX_DATA_AVAILABLE	(FT_M_RX_COUNT & BR_IN32(REG_FT_STATUS))
#define	FT_TX_IS_FULL			(FT_B_TX_FULL & BR_IN32(REG_FT_STATUS))
#define	FT_RX_IS_EMPTY			(FT_B_RX_EMPTY & BR_IN32(REG_FT_STATUS))
#define	FT_RX_GOT_DATA			(FT_B_RX_NEW_DATA & BR_IN32(REG_FT_STATUS))

/*==================================== Variable definitions =========================== */
typedef struct sTagComm
{
	TAG_CAT_TYPE category;
	TAG_NUM_TYPE num;
} tTagComm;

typedef union uTagComm
{
	tTagComm ucomm;
	TAG_COM_TYPE comm;
} tuTagComm;

typedef struct sTagHeader
	{
		tuTagComm command;
		TAG_FLG_TYPE flags;
		TAG_LEN_TYPE length;
	} tTagHeader;


/*==================================== External API ====================================*/
/**
 * @brief	Initializes the FTDI 232H module and FIFO
 * */
extern void BR_FT_Initialize(void);

/**
 * @brief	When called, picks up data present in the FT232 FIFO, copies it
 * 			into out local buffer FT_RX_buffer[] and returns the number of bytes
 * 			read.
 *
 * @return	Number of bytes read from the FTDI Fifo.
 */
extern u32 BR_FT_PickUp_Data(void);

/* @brief	When called, picks up the request amount of bytes in the FT232 FIFO, copies it
 * 			into out local buffer FT_RX_buffer[] and returns the number of bytes
 * 			read.
 * @param	len is the number of bytes requested to be read, must be less/equal to FT_RX_BLOCK_SZ
 * @return	0: success, otherwise it means error.
 */
extern int BR_FT_Fetch_Bytes(u32 len);

/**
 * @brief	Transmits data via USB to the external application.
 * 			Data to be transmitted must be pre-allocated in the
 * 			FT_TX_buffer.
 * 			This function can transmit in either 32bit or 8bit format
 * 			depending on the build option BR_USE_32BIT_WRITE.
 */
void BR_FT_Send_Data(FT_WR_TYPE* data, u32 len);

/**
 * 	@brief	Transmits a TAG formatted packet to the client
 * 	@param	com: command code (union of category and number)
 * 	@param	flags: processing/description flags
 * 	@param	data_len: size of the data to follow later.
 * */
extern void BR_FT_Send_Tag(u32 com, u32 flags, u32 data_len);

/**
 * @brief	Transmits a block of data from the OFO, straight to the
 * 			FTDI fifo.
 * @param	len, number of BYTES to transmit. Must be multiple of 4.
 */
void BR_FT_Send_from_OFO(s32 len);


/* Below variables are for sharing data among different modules */
extern u8 FT_RX_buffer[];			/* Ram location for receiving USB data */
extern u8 FT_TX_buffer[];			/* Ram location for transmitting data */

#endif /* BR_FT232_H_ */
