/**
	@file		BR_dma.h
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		DMA controller

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#ifndef BR_DMA_H_
#define BR_DMA_H_

#include "xparameters.h"
#include "xaxicdma.h"
#include "xaxicdma_i.h"


#define	DDR_READ_RX_OFFSET				(0u)
#define	DDR_WRITE_TX_OFFSET				(16*1024u)

#define	DMA_CR_INIT					(XAXICDMA_XR_IRQ_ALL_MASK) // + XAXICDMA_CR_KHOLE_WR_MASK + XAXICDMA_CR_KHOLE_RD_MASK)
#define	DMA_SA_INIT					(DDR_BASE_ADDR + DDR_READ_RX_OFFSET)			//((u32)FT_TX_buffer)							/* set default data source to the FT232 RAM buffer */
#define	DMA_DA_INIT					(DDR_BASE_ADDR + DDR_WRITE_TX_OFFSET)			/* set default destination to the FT232 FIFO */

#define	DMA_TX_PAGE			(2048)											/* maximum data we can transfer @ once */
#define	DMA_TX_LOW_TRES		(0x20000)										/* Status register value when TX is empty */


/*==================================== External API ====================================*/

/**
 * @brief	Initializes the Central DMA module
 * */
void BR_DMA_Initialize(void);

/**
 *	@brief	Starts a CDMA transfer of <numbytes> bytes.
 *	@param	numbytes, is the transfer size in bytes
 */
void BR_DMA_Start_Transfer (u32 numbytes);

/**
 * @brief: DMA quick test
 * */
void DMA_qt(void);

#define	DMA_IS_IDLE					(XAXICDMA_SR_IDLE_MASK & BR_IN32(REG_DMA_SR))	/* returns non-zero if CDMA is idle */
#endif /* BR_DMA_H_ */
