/**
	@file		BR_gpioc
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#include "BR_gpio.h"

/**
 *	@brief	Routine to initialize the devices that are controlled via GPIO: LCD, LED, Push buttons, DIP switches.
 *			Since on the FPGA side these devices are all handled from the memory space, we
 *			perform a single initialization for them all together.
 */
void BR_GPIO_initialize(void)
{
//	BR_OUT32(REG_LED_DIR, 0);			/* set LED pins as output */
//	BR_OUT32(REG_RGBL_DIR, 0);			/* set RGB LED pins as output */
//	BR_OUT32(REG_BTN_DIR, BTN_ALL);		/* set PUSH button pins as input */
//	BR_OUT32(REG_SW_DIR, SW_ALL);		/* set DIP SW pins as input */
}


