/*
 * BR_regs.h
 *
 *  Created on: Oct 3, 2016
 *      Author: eddie.llerena
 */

#ifndef __BR_REGS_H__
#define __BR_REGS_H__

#ifdef __MICROBLAZE__
#include "xparameters.h"
#include "xil_types.h"
#endif

/*==========================================================================================*/
/*================================== REGISTER DEFINITIONS ==================================*/
/*==========================================================================================*/
#define	DDR_BASE_ADDR					(XPAR_MIG_7SERIES_0_BASEADDR)		/* external DDR3L 256MB */
#define	DMA_BASE_ADDR					(XPAR_AXI_CDMA_0_BASEADDR)
#define	INT_BASE_ADDR					(XPAR_INTC_0_BASEADDR)				/* interrupt controller base address */
#define	FT_BASE_ADDR					(XPAR_FTDI_245_SYNC_FIFO_IF_0_S00_AXI_BASEADDR)
#define	ULI_BASE_ADDR					(XPAR_UARTLITE_0_BASEADDR)
#define	PCAP_BASE_ADDR					(XPAR_PMODCAPTURER_0_S00_AXI_BASEADDR)
#define INFO_BASE_ADDR					(XPAR_INFOMODULE_0_S00_AXI_BASEADDR)
//#define	TST_BASE_ADDR					(XPAR_AXI_TEST_0_0)
//#define	UI_BASE_ADDR					(XPAR_ARTY_USER_IF_0_0)
#define	LED_BTN_BASE_ADDR				(XPAR_GPIO_0_BASEADDR)
#define	RGBLED_SW_BASE_ADDR				(XPAR_GPIO_1_BASEADDR)
//#define	JA_BASE_ADDR					(XPAR_PMOD_IF_0_0)


/*==========================================================================================*/
/*=============================== REGISTER OFFSET DEFINITIONS ==============================*/
/*==========================================================================================*/
#define	DDR_DBG_OFFSET					(0x00)

#define RD_OFFSET						(0x00)
#define WR_OFFSET						(0x04)
#define STATUS_OFFSET					(0x08)
#define CTRL_OFFSET						(0x0c)
#define VER_OFFSET						(0x10)

#define	TST_REG0_OFFSET					(0x00)
#define	TST_REG1_OFFSET					(0x04)
#define	TST_REG2_OFFSET					(0x08)
#define	TST_REG3_OFFSET					(0x0c)

#define	JA_CTRL_OFFSET					(0x00)
#define	JA_RD_OFFSET					(0x04)
#define	JA_WR_OFFSET					(0x08)

#define	DMA_CR_OFFSET					(XAXICDMA_CR_OFFSET)
#define	DMA_SR_OFFSET					(XAXICDMA_SR_OFFSET)
#define	DMA_CDESC_OFFSET				(XAXICDMA_CDESC_OFFSET)
#define	DMA_TDESC_OFFSET				(XAXICDMA_TDESC_OFFSET)
#define	DMA_SRCADDR_OFFSET				(XAXICDMA_SRCADDR_OFFSET)
#define	DMA_DSTADDR_OFFSET				(XAXICDMA_DSTADDR_OFFSET)
#define	DMA_BTT_OFFSET					(XAXICDMA_BTT_OFFSET)

#define	FT_STATUS_OFSET					(FTDI_245_SYNC_FIFO_IF_S00_AXI_SLV_REG0_OFFSET)
#define	FT_RD_OFFSET					(FTDI_245_SYNC_FIFO_IF_S00_AXI_SLV_REG1_OFFSET)
#define	FT_WR_OFFSET					(FTDI_245_SYNC_FIFO_IF_S00_AXI_SLV_REG2_OFFSET)
#define	FT_CTRL_OFFSET					(FTDI_245_SYNC_FIFO_IF_S00_AXI_SLV_REG3_OFFSET)

#define	ULI_RXFIFO_OFFSET				(XUL_RX_FIFO_OFFSET)
#define	ULI_TXFIFO_OFFSET				(XUL_TX_FIFO_OFFSET)
#define	ULI_STATUS_OFFSET				(XUL_STATUS_REG_OFFSET)
#define	ULI_CTRL_OFFSET					(XUL_CONTROL_REG_OFFSET)

#define	UI_BTN_OFFSET					(0x04)
#define	UI_LEDS_OFFSET					(0x08)

#define	PCAP_CTRL_OFFSET				(PMODCAPTURER_S00_AXI_SLV_REG0_OFFSET)
#define PCAP_TIM0_OFFSET				(PMODCAPTURER_S00_AXI_SLV_REG1_OFFSET)
#define PCAP_VAL0_OFFSET				(PMODCAPTURER_S00_AXI_SLV_REG2_OFFSET)
#define PCAP_VAL1_OFFSET				(PMODCAPTURER_S00_AXI_SLV_REG3_OFFSET)

#define	INFO_DATE_OFFSET				(INFOMODULE_S00_AXI_SLV_REG0_OFFSET)
#define	INFO_TIME_OFFSET				(INFOMODULE_S00_AXI_SLV_REG1_OFFSET)
#define INFO_VERSION_OFFSET				(INFOMODULE_S00_AXI_SLV_REG2_OFFSET)
#define INFO_PRODUCT_OFFSET				(INFOMODULE_S00_AXI_SLV_REG3_OFFSET)

#define	LED_DATA_OFFSET					(XGPIO_DATA_OFFSET)
#define	LED_DIR_OFFSET					(XGPIO_TRI_OFFSET)
#define	BTN_DATA_OFFSET					(XGPIO_DATA2_OFFSET)
#define	BTN_DIR_OFFSET					(XGPIO_TRI2_OFFSET)

#define	RGBLED_DATA_OFFSET				(XGPIO_DATA_OFFSET)
#define	RGBLED_DIR_OFFSET				(XGPIO_TRI_OFFSET)
#define	SW_DATA_OFFSET					(XGPIO_DATA2_OFFSET)
#define	SW_DIR_OFFSET					(XGPIO_TRI2_OFFSET)

/*==========================================================================================*/
/*================================== REGISTER DEFINITIONS ==================================*/
/*==========================================================================================*/
//#define	REG_TST_REG0					(TST_BASE_ADDR + TST_REG0_OFFSET)
//#define	REG_TST_REG1					(TST_BASE_ADDR + TST_REG1_OFFSET)
//#define	REG_TST_REG2					(TST_BASE_ADDR + TST_REG2_OFFSET)
//#define	REG_TST_REG3					(TST_BASE_ADDR + TST_REG3_OFFSET)
#define	REG_DDR_DBG						(DDR_BASE_ADDR + DDR_DBG_OFFSET)

//#define	REG_JA_CTRL						(JA_BASE_ADDR + JA_CTRL_OFFSET)
//#define	REG_JA_RD						(JA_BASE_ADDR + JA_RD_OFFSET)
//#define	REG_JA_WR						(JA_BASE_ADDR + JA_WR_OFFSET)

#define	REG_DMA_CR						(DMA_BASE_ADDR + DMA_CR_OFFSET)
#define	REG_DMA_SR						(DMA_BASE_ADDR + DMA_SR_OFFSET)
#define	REG_DMA_CDESC					(DMA_BASE_ADDR + DMA_CDESC_OFFSET)
#define	REG_DMA_TDESC					(DMA_BASE_ADDR + DMA_TDESC_OFFSET)
#define	REG_DMA_SRCADDR					(DMA_BASE_ADDR + DMA_SRCADDR_OFFSET)
#define	REG_DMA_DSTADDR					(DMA_BASE_ADDR + DMA_DSTADDR_OFFSET)
#define	REG_DMA_BTT						(DMA_BASE_ADDR + DMA_BTT_OFFSET)

#define	REG_FT_STATUS					(FT_BASE_ADDR + FT_STATUS_OFSET)
#define	REG_FT_RD						(FT_BASE_ADDR + FT_RD_OFFSET)
#define	REG_FT_WR						(FT_BASE_ADDR + FT_WR_OFFSET)
#define	REG_FT_CTRL						(FT_BASE_ADDR + FT_CTRL_OFFSET)

#define	REG_ULI_RXFIFO					(ULI_BASE_ADDR + ULI_RXFIFO_OFFSET)
#define	REG_ULI_TXFIFO					(ULI_BASE_ADDR + ULI_TXFIFO_OFFSET)
#define	REG_ULI_STATUS					(ULI_BASE_ADDR + ULI_STATUS_OFFSET)
#define	REG_ULI_CTRL					(ULI_BASE_ADDR + ULI_CTRL_OFFSET)

#define REG_PCAP_CTRL					(PCAP_BASE_ADDR + PCAP_CTRL_OFFSET)
#define REG_PCAP_TIM0					(PCAP_BASE_ADDR + PCAP_TIM0_OFFSET)
#define REG_PCAP_VAL0					(PCAP_BASE_ADDR + PCAP_VAL0_OFFSET)
#define REG_PCAP_VAL1					(PCAP_BASE_ADDR + PCAP_VAL1_OFFSET)

#define REG_INFO_DATE					(INFO_BASE_ADDR + INFO_DATE_OFFSET)
#define REG_INFO_TIME					(INFO_BASE_ADDR + INFO_TIME_OFFSET)
#define REG_INFO_VERSION				(INFO_BASE_ADDR + INFO_VERSION_OFFSET)
#define REG_INFO_HASH					(INFO_BASE_ADDR + INFO_PRODUCT_OFFSET)

//#define	REG_UI_BUTTONS					(UI_BASE_ADDR + UI_BTN_OFFSET)
//#define	REG_UI_LEDS						(UI_BASE_ADDR + UI_LEDS_OFFSET)

#define	REG_LED_DATA					(LED_BTN_BASE_ADDR + LED_DATA_OFFSET)
#define	REG_LED_DIR						(LED_BTN_BASE_ADDR + LED_DIR_OFFSET)
#define	REG_BTN_DATA					(LED_BTN_BASE_ADDR + BTN_DATA_OFFSET)
#define	REG_BTN_DIR						(LED_BTN_BASE_ADDR + BTN_DIR_OFFSET)
#define	REG_RGBL_DATA					(RGBLED_SW_BASE_ADDR + RGBLED_DATA_OFFSET)
#define	REG_RGBL_DIR					(RGBLED_SW_BASE_ADDR + RGBLED_DIR_OFFSET)
#define	REG_SW_DATA						(RGBLED_SW_BASE_ADDR + SW_DATA_OFFSET)
#define	REG_SW_DIR						(RGBLED_SW_BASE_ADDR + SW_DIR_OFFSET)

/*==========================================================================================*/
/*=============================== Miscelaneous definitions =================================*/
/*==========================================================================================*/
#ifdef UBLAZE_FW
#define	WR_OFF32(x, y)					(*(ptemp + ((x)/4)) = (y))
#define RD_OFF32(x)						(*(ptemp + ((x)/4)))
#define	WR_OFF8(x, y)					(*((u8*)ptemp + (x))) = (y))
#define RD_OFF8(x)						(*((u8*)ptemp + (x)))
#define	BR_OUT32(x, y)					(*(volatile u32*)(x) = (y))
#define	BR_IN32(x)						(*(volatile u32*)(x))
#define	BR_OUT8(x, y)					(*(volatile u8*)(x) = (y))
#define	BR_IN8(x)						(*(volatile u8*)(x))
#else
#define	WR_OFF32(x, y)					Xil_Out32(((u32)ptemp + (x)),(y))
#define RD_OFF32(x)						Xil_In32((u32)ptemp + (x))
#define	WR_OFF8(x, y)					Xil_Out8(((u32)ptemp + (x)),(y))
#define RD_OFF8(x)						Xil_In8((u32)ptemp + (x))
#define	BR_OUT32(x, y)					Xil_Out32((u32)(x),(y))
#define	BR_IN32(x)						Xil_In32((u32)(x))
#define	BR_OUT8(x, y)					Xil_Out8((u32)(x),(y))
#define	BR_IN8(x)						Xil_In8((u32)(x))
#endif


#endif /* __BR_REGS_H__ */
