/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * Basic sample test application:
 * 		Generate a sequence of numbers and copy them all to external ram
 * 		@ address CDMA_SA_INIT.
 * 		Next, we use CDMA to copy all data to address CDMA_DA_INIT.
 * 		Finally we print the source and destination data which -if all is ok-
 * 		should match.
 * 		Note: you better use a fast UART rate, otherwise we find problems
 * 		presenting the data out.
 */

#include <stdio.h>
#include "BR_dma.h"
#include "BR_gpio.h"
#include "BR_int.h"
#include "BR_regs.h"
#include "xil_cache.h"
#include "ftdi_245_sync_fifo_if.h"
#include "xuartlite_l.h"

int ParseCharsToValueHex(char *);

#define	CRLF			"\n"
#define	SW_VER			"0.0.0 "
#define	TST_BYTE_SZ		(16) //*1024)					/* CDMA transfer size (BTT) */
#define PRINTF			xil_printf
#define POINTER_TO_ARGS		(2)							/* index of the location of the 'arguments' in the buffer array */
#define BUFFER_SIZE			(20)						/* max number of bytes that can be received in a command entry */
#define TASK				acCommand[0]				/* variable to hold the requested 'task' */
#define	MODE				acCommand[1]				/* variable to hold the 'mode' for the TASK*/

u8 acCommand[BUFFER_SIZE];								/* buffer used to move commands and arguments around */
u8 *acArgs = &acCommand[POINTER_TO_ARGS];				/* pointer to the arguments section in the buffer */
u32 flags = 0;

int main()
{
	u32 i, *d, temp;
	u8 *s;

#ifdef XPAR_MICROBLAZE_USE_ICACHE
	Xil_ICacheEnable();
#endif
#ifdef XPAR_MICROBLAZE_USE_DCACHE
	Xil_DCacheEnable();
#endif

	PRINTF("\nArty v. " SW_VER __DATE__ " " __TIME__"\n\r");
	BR_GPIO_initialize();
	BR_Intc_Initialize();
//	LEDS_ON(0xff);
//	LED_GRP_ON(0);						/* turn all leds off */
//	RGBL_GRP_ON(0);						/* turn all RGB leds off */

	PRINTF("Initializing DMA...");
	BR_DMA_Initialize();

	PRINTF("Testing DMA...\n\r");
	s = (u8*)(DMA_SA_INIT);
	for (i = 0; i < (16*TST_BYTE_SZ); i++)
	{
		*s++ = i;
	}

	BR_DMA_Start_Transfer(16*TST_BYTE_SZ);

	d = (u32*)DMA_DA_INIT;
	for (i = 0; i < TST_BYTE_SZ; i+=4)
	{
		PRINTF("%08x\t%08x\t%08x\t%08x\n\r", *(d+0), *(d+1), *(d+2), *(d+3));
		d+=4;
	}

	xil_printf("Done\n\r>");
//	RGBL_GRP_ON(RGBL1);				/* turn all RGB leds off */
//	LED_GRP_ON(LED6);

	while (1)
	{
		if(flags)
		{
			ParseCharsToValueHex((char*)acArgs);					/* convert text arguments into numbers, number of arguments stored in n */
			switch (TASK)
			{
				case 'f':
					PRINTF("%8x %d %d\n\r", REG_FT_CTRL, (*acArgs), (1<< (*acArgs)));
					BR_OUT32(REG_FT_CTRL, (1<< (*acArgs)));
					break;
				case 'r':
					switch(MODE)
					{
						case 'w':			// write register: rw<add3><add2><add1><add0><val3><val2><val1><val0>
						case 'r':			// read register: rr<add3><add2><add1><add0>
							i = (acArgs[0]<<24) | (acArgs[1]<<16) | (acArgs[2]<<8) | (acArgs[3]);
							temp = BR_IN32(i);
							PRINTF("Reg x%08x:x%08x" CRLF ">", i, temp);
							break;
						default:
							break;
					}
				default:
					break;
//				case 'l':
//					LEDS_ON(1 << *acArgs);
//					break;
			}
			PRINTF ("#%08x : #%08x : #%08x : #%08x\n\r>", BR_IN32(REG_FT_STATUS), BR_IN32(REG_FT_RD), BR_IN32(REG_FT_WR), BR_IN32(REG_FT_CTRL));
			flags = 0;
		}
//		else
//		{
//			temp = BTN_PRESS_ANY;
//			if (temp)
//			{
//				PRINTF("%d\n\r", temp);
//				for(i = 0; i < 800000; i++);
//				while(BTN_PRESS_ANY);
//				switch (temp)
//				{
//					case BTN0:
//						BR_OUT32(REG_FT_CTRL, (1<<0));
//						break;
//
//					case BTN1:
//						BR_OUT32(REG_FT_CTRL, 0);
//						break;
//
//					case BTN2:
//						BR_OUT32(REG_FT_RD, 0xacabad05);
//						break;
//
//					case BTN3:
//						BR_OUT32(REG_FT_WR, 0xadefec10);
//						break;
//				}
//				PRINTF ("#%08x : #%08x : #%08x : #%08x\n>", BR_IN32(REG_FT_STATUS), BR_IN32(REG_FT_RD), BR_IN32(REG_FT_WR), BR_IN32(REG_FT_CTRL));
//			}
//		}
	}

	Xil_DCacheDisable();
	Xil_ICacheDisable();
	return 0;
}



/**
	@brief	converts a string of chars representing a vector of hex numbers into a vector of bytes equivalent.
	@param	p contains the address of the first char in the string.
	@retval	the count of numbers comverted.
	@verbatim
			Text entered by a user via the keyboard is in the form of a string of printable chars that represent hex numbers.
			This functions takes that string of chars and extracts the meaningful hex numbers represented in it, then
			it places those numbers (bytes) in an array at the same ram location. Returning the number of numbers
			converted.
	@endverbatim
 */
int ParseCharsToValueHex(char *p)
{
	char *q, d;
	int i, k;
	uint8_t c;

	q = p;
	k = 0;

	while (*p > 0x1f)										/* Will process until a non printable character is found */
	{
		d = 0;												/* initialize the 'number equivalent value' */
		i = 0;												/* initialize the digit counter (assume 2 digits max per number)*/
		do
		{
			d <<= 4;										/* each digit uses the first 4 bits, so on each loop we must push the others */
			c = *p++;										/* read the new char */
			if ((c > 0x29) && (c < 0x3a))					/* char is between 0 and 9 */
			{
				d += (c - 0x30);							/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
			if ((c > 0x60 ) && (c < 0x67))					/* char is between 0xa and 0xf */
			{
				d += (c - 0x60 + 0x9);						/* add the char equivalen value to our total */
				i++;										/* increment our digit counter */
			}
		} while (( i == 0 ) || ((*p > 0x2f) && (i < 2)));	/* repeat: if no char found or: if found a space and less than 2 digits */

		if(i)												/* if the digit counter is non-zero then a value has been found */
		{
			*q++ = d;										/* write the number found to memory */
			k++;											/* increment 'detected numbers counter' */
		}
	}

	return k;
}



