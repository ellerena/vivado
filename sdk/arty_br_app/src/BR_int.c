/**
	@file		BR_int.c
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		Interrupt controller

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/



#include "xintc.h"
//#include "xgpio.h"
#include "xuartlite_l.h"
#include "BR_int.h"
#include "BR_regs.h"

extern u8 acCommand[];
extern u32 flags;

#define	IC_IRQ_MASK				( \
								XPAR_AXI_UARTLITE_0_INTERRUPT_MASK				/* UART console mask */\
								/*| XPAR_AXI_GPIO_1_IP2INTC_IRPT_MASK			GPIO IRQ mask */\
								)

#define	IC_MER_INIT				( XIN_INT_HARDWARE_ENABLE_MASK \
								| XIN_INT_MASTER_ENABLE_MASK )

//__attribute__ ((fast_interrupt)) void GPIO1_Handler(void)
//{
//	xil_printf("Gpio IRQ\n");
//	BR_OUT32(XPAR_GPIO_1_BASEADDR + XGPIO_ISR_OFFSET, XGPIO_IR_CH2_MASK);
//}

__attribute__ ((fast_interrupt)) void UART_lite_handler(void)
{
	if(BR_IN32(REG_ULI_STATUS) & XUL_SR_RX_FIFO_VALID_DATA)
	{
		uint8_t	t;
		static uint8_t *pacCommand = acCommand;		/* pointer used to write characters recvd by UART1 */

		t = BR_IN32(REG_ULI_RXFIFO);				/* read byte from UART */
		xil_printf("%c", t);
		if (t == '\r')								/* <return> ends the uart command entry */
		{
			xil_printf("%c", '\n');
			*pacCommand = 0;
			pacCommand = acCommand;
			flags = 1;								/* rise a flag to alert main() */
		}
		else
		{
			*pacCommand++ = t;							/* store byte in buffer */
		}
	}
}

void BR_Intc_Initialize(void)
{
	u32 *ptemp;

	ptemp = (u32*)INT_BASE_ADDR;
	WR_OFF32(XIN_MER_OFFSET, 0);							/* Stop the controller */
	WR_OFF32(XIN_IER_OFFSET, 0);							/* Disable all IRQs */
	WR_OFF32(XIN_IAR_OFFSET, 0xFFFFFFFF);					/* Clear all pending IRQs */
	WR_OFF32(XIN_IMR_OFFSET, 0x1f);							/* All IRQs in Fast mode */
	WR_OFF32(XIN_ILR_OFFSET, 0x1f);

	WR_OFF32(XIN_IVAR_OFFSET + (4*XPAR_INTC_0_UARTLITE_0_VEC_ID), (u32)(UART_lite_handler));	/* fill IRQ handler vector table */

	WR_OFF32(XIN_MER_OFFSET, IC_MER_INIT);
	WR_OFF32(XIN_IER_OFFSET, IC_IRQ_MASK);

//	BR_OUT32(XPAR_GPIO_1_BASEADDR + XGPIO_IER_OFFSET, XGPIO_IR_CH2_MASK);
//	BR_OUT32(XPAR_GPIO_1_BASEADDR + XGPIO_GIE_OFFSET, XGPIO_GIE_GINTR_ENABLE_MASK);
	BR_OUT32(REG_ULI_CTRL, XUL_CR_ENABLE_INTR);

	microblaze_enable_interrupts();
}


