/**
	@file		BR_gpio.h
	@author		Ellerena Inc.
	@version	0.0.0.0
	@date
	@note		Under development<BR>
	@brief		STM communication module

*************************************************************************
	@copyright

		Copyright (C) 2009-2014, Ellerena, Inc. - All Rights Reserved.

		THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
		THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.

		IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
		INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
		(INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFITS;
		OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
		LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*************************************************************************

*/

#ifndef BR_GPIO_H_
#define BR_GPIO_H_

#include "BR_regs.h"
#include "xgpio_l.h"

/*================================= bit definitions ==================================*/
#define	LED_ALL_OFF					(0)
#define	LED_USB_ACTIVE				(1 << 0)

/*====================================== masks  ======================================*/
/* Push buttons masks */
#define	BTN0						(1 << 0)
#define	BTN1						(1 << 1)
#define	BTN2						(1 << 2)
#define	BTN3						(1 << 3)
#define	BTN_ALL						(BTN0 | BTN1 | BTN2 | BTN3)
#define	BTN_GRP_STATE				(BR_IN32(REG_BTN_DATA))
#define	BTN_PRESS_ANY				(BTN_ALL & BTN_GRP_STATE)

#define	SW0							(1 << 4)
#define	SW1							(1 << 5)
#define	SW2							(1 << 6)
#define	SW3							(1 << 7)
#define	SW_ALL						(SW0 | SW1 | SW2 | SW3)
#define	SW_GRP_STATE				(BR_IN32(REG_UI_BUTTONS))
#define	SW_PRESS_ANY				(SW_ALL & SW_GRP_STATE)

#define	LED4						(1 << 0)
#define	LED5						(1 << 1)
#define	LED6						(1 << 2)
#define	LED7						(1 << 3)
#define	LED_ALL						(LED0 | LED1 | LED2 | LED3)

#define	RGBL0						(0x111 << 4)
#define	RGBL1						(0x111 << 5)
#define	RGBL2						(0x111 << 6)
#define	RGBL3						(0x111 << 7)
#define	RGBL_ALL					(RGBL0 | RGBL1 | RGBL2 | RGBL3)

#define	GLED0						(1 << 4)
#define	GLED1						(1 << 5)
#define	GLED2						(1 << 6)
#define	GLED3						(1 << 7)

#define	RLED0						(GLED0 << 4)
#define	RLED1						(GLED1 << 4)
#define	RLED2						(GLED2 << 4)
#define	RLED3						(GLED3 << 4)

#define	BLED0						(GLED0 << 8)
#define	BLED1						(GLED1 << 8)
#define	BLED2						(GLED2 << 8)
#define	BLED3						(GLED3 << 8)

#define	GLEDS						(GLED0 | GLED1 | GLED2 | GLED3)
#define	RLEDS						(RLED0 | RLED1 | RLED2 | RLED3)
#define	BLEDS						(BLED0 | BLED1 | BLED2 | BLED3)

#define	LEDS_ON(x)					BR_OUT32(REG_UI_LEDS, (x))


#define	BTN_SET_DIR(x)				BR_OUT32(REG_BTN_DIR, (x))
#define	PUSH_SET_DIR(x)				BR_OUT32(REG_PUSH_DIR, (x))
#define	LED_SET_DIR(x)				BR_OUT32(REG_LED_DIR, (x))
#define	LED_WR_DATA32(x)			BR_OUT32(REG_LED_DATA, (x))				/* present data on the LED (8 LSB used only) */
#define	BTN_RD_DATA32				BR_IN32(REG_BTN_DATA)					/* returns content of the DIP switches (only 8 LSB are valid) */
#define	PUSH_RD_DATA32				BR_IN32(REG_PUSH_DATA))					/* returns state of push buttons (bit mask defined above) */

/*==================================== External API ====================================*/
/**
 *	@brief	Routine to initialize the devices that are controlled via GPIO: LCD, LED, Push buttons, DIP switches.
 *			Since on the FPGA side these devices are all handled from the memory space, we
 *			perform a single initialization for them all together.
 */
extern void BR_GPIO_initialize(void);

#endif /* BR_GPIO_H_ */
