module UART_rx( input c, rx, input [7:0] brR, cfR,
		output stbErr, parErr, frmErr, doneS, output [7:0] dOut);
	
	wire halt = stbErr | doneS;
	wire fosf = ~rx;
	
	Ignitor ig1_ (c, halt, fosf,/**/ignP, ignL);	/* External component */
	startbit st1_ (c, ignP, tic, rx,/**/checkn, stbErr);
	BaudRateGen br1_ (c, ignP, ignL, brR,/**/tic, tic2);
	SerialInParallelOut #(8) sp1_ (	c, checkn, tic, rx, cfR[3:0],/**/datRdy, dOut );
	ParityReader pr1_ (	c, checkn, datRdy, tic, rx, cfR[5], cfR[4],/**/doneP, parErr);
	stopbit st2_ (c, doneP, tic2, rx, cfR[7:6], /**/ doneS, frmErr );

endmodule

//Design by Edison Llerena
//UART receiver module. (no parity check yet)
//brR: baud rate sampling speed
//	 19200	  19171.78	5.22E-05	 0.15%	163
//	 38400	  38580.25	2.59E-05	 0.47%	 81
//	 76800	  76219.51	1.31E-05	 0.76%	 41
//	115200	 115740.74	8.64E-06	 0.47%	 27
//	230400	 223214.29	4.48E-06	 3.12%	 14
//	460800	 446428.57	2.24E-06	 3.12%	  7
//	921600	1041666.67	9.60E-07	13.03%	  3

// cfR: config. vector	{StopBit[1:0], OddPar, NoParity, 8bit, 7bit, 6bit, 5bit}
//	1 bit stop -> 01, 1.5 bit stop -> 10, 2 bit stop -> 11
//	Data bits: 8 bit -> 1000, 7 bit -> 0100, 6 bit -> 0010, 5 bit -> 0001 
//02-11-2011
//	Minimum period: 7.262ns (Maximum Frequency: 137.703MHz)
//	Minimum input arrival time before clock: 7.431ns
//	Maximum output required time after clock: 7.281ns
//	Maximum combinational path delay: No path found
