module DigClkMgr #(parameter A=`CLOCK_INPUT_PER_NS, B=`CLOCK_DIVIDE_BY, C=`CLOCK_FX_DIVIDE_BY, D=`CLOCK_FX_MULTIPLY)(
	input gCLK, r,
	output c, c_n, c_div2, cf, cf_n, c_OK
	);

`ifdef SPARTAN_3_DCM							// Spartan 3, xc3s200-4ft256 50MHz
	DCM #(
	.SIM_MODE("SAFE"),						// Simulation: "SAFE" vs. "FAST", see "Synthesis and Simulation Design Guide" for details
	.DFS_FREQUENCY_MODE("LOW"),			// HIGH or LOW frequency mode for frequency synthesis
	.FACTORY_JF(16'hC080),					// FACTORY JF values
`elsif CLOCK_25_17MZ							//	25.175MHz CLK2
`define CLOCK_INPUT_PER_NS		(39.72)
	DCM_SP #(
`elsif SPARTAN_3E_DCM						// Nexis 2, xc3s500e-4fg320 50MHz; Sample Pack S3E xc3s100e-TQ144
	DCM_SP #(
`endif
	.CLKDV_DIVIDE(B),								// Divide by: 1.5,2.0... 7.5,8,9... 16
	.CLKFX_DIVIDE(C),								// integer from 1 to 32
	.CLKFX_MULTIPLY(D),							// integer from 2 to 32
	.CLKIN_DIVIDE_BY_2("FALSE"),				// enable CLKIN divide by two feature
	.CLKIN_PERIOD(A),								// period of input clock
	.CLKOUT_PHASE_SHIFT("NONE"),				// phase shift: NONE, FIXED or VARIABLE
	.CLK_FEEDBACK("1X"),							// clock feedback: NONE, 1X or 2X
	.DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"),	// SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or integer from 0 - 15
	.DLL_FREQUENCY_MODE("LOW"),				// HIGH or LOW frequency mode for DLL
	.DUTY_CYCLE_CORRECTION("TRUE"),			// Duty cycle correction, TRUE or FALSE
	.PHASE_SHIFT(0),								// Amount of fixed phase shift: -255 to 255
	.STARTUP_WAIT("FALSE")						// Delay configuration DONE until DCM LOCK, TRUE/FALSE
   )
	DCM_SP_inst (
		c,												// CLK0, 0 degree DCM CLK output
		c_n,											// CLK180, 180 degree DCM CLK output
		,												// CLK270, 270 degree DCM CLK output
		,												// CLK2x, 2X DCM CLK output
		,												// CLK2x180, 2X 180 degree DCM CLK out
		,												// CLK90, 90 degree DCM CLK output
		c_div2,										// CLKDV, Divided DCM CLK out (CLKIN/CLKDV_DIVIDE)
		cf,											// CLKFX, DCM CLK synthesis out (CLKIN*CLKFX_MULTIPLY/CLKFX_DIVIDE)
		cf_n,											// CLKFX180, synthesized CLKFX phased 180 degrees
		c_OK,											// LOCKED, DCM LOCK status output
		,												// PSDONE, Dynamic phase adjust done output
		,												// STATUS[7..0], 8-bit DCM status bits output
		c,												// CLKFB, DCM clock feedback
		gCLK,											// CLKIN, Clock input (from IBUFG, BUFG or DCM)
		,												// PSCLK, Dynamic phase adjust clock input
		,												// PSEN, Dynamic phase adjust enable input
		,
		,												// PSINCDEC, Dynamic phase adjust increment/decrement
		r												// RST, DCM asynchronous reset input
		);

	 endmodule

