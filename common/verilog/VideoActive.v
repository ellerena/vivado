module VideoActive #	(parameter H=640, V=480)
							(input c, hend, vend, input [9:0] hpix, vpix,
							output vact );

`ifdef OPT640x480
	assign vact = (hpix < H) & (vpix < V);
`else
	wire h = (hpix == H-1);
	wire v = vend | (vpix < V-1);
	reg va = 1'b1;
	assign vact = va;

	always @(posedge c)
		if (h) va <= 1'b0;
			else if (hend) va <= v;
			
`endif

endmodule

