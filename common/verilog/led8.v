module LedBlock( input [1:0] channel_led_sel, input [31:0] led_inputs,
					output reg[7:0] led);

	always @ *
		case (channel_led_sel)
			2'b00	:	led = led_inputs[7:0];
			2'b01	:	led = led_inputs[15:8];
			2'b10	:	led = led_inputs[23:16];
			2'b11	:	led = led_inputs[31:24];
		endcase
		
endmodule

/**
	@brief	Led driver.
	@input	led_inputs: 32 bits representing 4 channels of 8 bits each.
				channel_led_sel: selects which of the 4 channels will go to the leds.
	@output	led_output: 8 bit signals going to the 8 leds.				
*/
