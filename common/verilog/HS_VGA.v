module HS_VGA # (parameter D=640, F=16, R=96, B=48)
					(input c,
					output hs, pre, output reg hend, output [9:0] hpix );

	localparam W = 4;									/* use split counter to optimize speed */
	reg [W-1:0] px0;									/* pre counter */
	reg [9:W] px1;										/* post counter */
	assign hpix = {px1, px0};						/* {[9..4][3..0]} horizontal step counter */

`ifdef OPT640x480
	wire syn1 = (hpix[9:4] < 6'b101001);		/* true while < 656 */
	wire syn2 = (hpix[9:4] > 6'b101110);		/* true while > 736 */
	assign pre = {hpix[9:8], hpix[4:1]};
`else
	wire syn1 = (hpix < (D+F));					/* true while < 656 */
	wire syn2 = (hpix > (D+F+R-1));				/* true while > 751 */
	assign pre = (hpix > (D+F+R+B-3));			/* true while > 797 */
`endif

	assign hs =  syn1 | syn2;						/* pulse low between 656 and 736 */

	always @(posedge c) begin
		if (hend) px0 <= 0;							/* pre-counter */
		else px0 <= px0 + 1;

		if (hend) px1 <= 0;
		else if (px0 == 4'hf) px1 <= px1+1;		/* post-counter */

		if (hend) hend <= 0;
		else if(pre) hend <= 1'b1;					/* hend pulse generator */
	end

	initial begin
			px0 <= 0; px1 <= 0; hend <= 0;
		end
endmodule

/**
Engineer: Edison Llerena					Create Date:   17:01:44 08/17/2011
Description: VGA Sync Generator
History
19-09-2011	Counter optimized max speed 279.720MHz
*/