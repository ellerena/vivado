module clkdividor #(parameter N=6) (input c, output o);

	reg [3:0] pre = 0;
	reg [N-1:4] post = 0;
	
	assign o = post[N-1];

	always @(posedge c) begin
		pre <= pre + 1;
		if (pre == 4'b1111) post <= post +1;
	end
endmodule

// Engineer: Edison Llerena
// Create Date:    23:01:03 10/26/2010 
// Module Name:    clkgen 
// Revision 0.01 - File Created
//	 1	->	 25.00	MHz	(40.00ns)
//	 2	->	 12.50	MHz	(80.00ns)
//	 3	->	  6.25	MHz	(160.00ns)
//	 4	->	  3.13	MHz	(320.00ns)
//	 5	->	  1.56	MHz	(640.00ns)
//	 6	->	781.25	kHz	(1.28us)
//	 7	->	390.63	kHz	(2.56us)
//	 8	->	195.31	kHz	(5.12us)
//	 9	->	 97.66	kHz	(10.24us)
//	10	->	 48.83	kHz	(20.48us)
//	11	->	 24.41	kHz	(40.96us)
//	12	->	 12.21	kHz	(81.92us)
//	13	->	  6.10	kHz	(163.84us)
//	14	->	  3.05	kHz	(327.68us)
//	15	->	  1.53	kHz	(655.36us)
//	16	->	762.94	Hz	(1.31ms)
//	17	->	381.47	Hz	(2.62ms)
//	18	->	190.73	Hz	(5.24ms)
//	19	->	 95.37	Hz	(10.49ms)
//	20	->	 47.68	Hz	(20.97ms)
//
//	Minimum period: 3.966ns (Maximum Frequency: 252.143MHz)
//	Maximum output required time after clock: 7.241ns