module Ignitor(	input c, r, go, output reg oP, oL );

always @ (posedge c) begin
	if (r) oL <= 0;
	else if (go) oL <= 1'b1;
	
	if (oL) oP <= 0;
	else if (go) oP <= 1'b1;
	end
	
	initial begin
		oP = 0; oL = 0;
		end
endmodule

//Design by Edison Llerena
//r: resets outputs to 0
//go: generates a pulse (oP) and a permanent high level (oL)
//02-11-2011	Maximum Frequency: 392.619MHz
//Minimum input arrival time before clock: 2.724ns
//Maximum output required time after clock: 7.165ns
