`timescale 1ns / 1ps
/////////////////////////////////////////////////////////////////////////////////
// Engineer: Edison Llerena
// Create Date:    07/11/2018
// Module Name:    fast_counter
// Revision 0.01 - File Created
//   Minimum period: 4.118ns (Maximum Frequency: 242.836MHz)
//   Minimum input arrival time before clock: 2.841ns
//   Maximum output required time after clock: 5.421ns
/////////////////////////////////////////////////////////////////////////////////

module fifo_1clk #(parameter B=8 /* bits wide */, W=4  /* address bits */)
      (input c, r, rd, wr, input [B-1:0] din,
       output empty, full, output [B-1:0] dout);

   reg [B-1:0] a_reg [2**W-1:0];     // register array
   reg [W-1:0] w_pt,  r_pt;
   reg f = 1'b0, e = 1'b1;
   wire [W-1:0] w_pt_nx = w_pt + 1'b1, r_pt_nx = r_pt + 1'b1;
   wire e_nx = w_pt == r_pt_nx, f_nx = r_pt == w_pt_nx;
   wire we = wr & ~f, re = rd & ~e;

   assign full = f;
   assign empty = e;
   assign dout = a_reg[r_pt];        // register file read operation

   always @(posedge c)               // register file write operation
      if (we)
         a_reg[w_pt] <= din;

   always @(posedge c) begin         // register for read and write pointers
      w_pt = r ? {W{1'b0}} : (we ? w_pt_nx : w_pt);
      r_pt = r ? {W{1'b0}} : (re ? r_pt_nx : r_pt);
      e = we ? 1'b0 : (re ? e_nx : e);
      f = re ? 1'b0 : (we ? f_nx : f);
   end

   initial begin
      w_pt = {W{1'b0}};
      r_pt = {W{1'b0}};
   end

endmodule

////////////////////////////////////////////////////////////////////////////////////
`ifdef __FIFO_TST__
`define CKPER_NS      (1e9/50e6)   /* 20ns period */
`define CH_OFFSET    (1)           /* small offset to simulate rise/fall delay */

module T_module;
   reg [15:0] _clk;
/***************************** UUT ****************************/
   parameter B = 8, W = 3;
   reg c, r, rd, wr;
   reg [B-1:0] i;
   wire emp, full;
   wire [B-1:0] o;

   fifo_1clk #(B, W) _u0 (c, r, rd, wr, i, emp, full, o);
/********************* Initial conditions *********************/
   initial begin
      {c, r, rd, wr} = {4'b0};
      i = 0;
      _clk = 0;
      fork
         forever #(`CKPER_NS/2) c = ~c;               /* base clock */
         #`CH_OFFSET forever _clk= #(`CKPER_NS) ((_clk + 1));
         #(2*(`CKPER_NS)+`CH_OFFSET) forever #(1*(`CKPER_NS)) i = i + 1; //$random;
      join
   end

/********************* Stimulus Signals ***********************/
   always @ (_clk)
   begin
      case (_clk)
           2: r = 1;
           3: r = 0;
          10: wr = 1;
          15: rd = 1;
          19: wr = 0;
          20: rd = 0;
          25: rd = 1;
          27: wr = 1;
          40: wr = 0;
          50: rd = 0;
      endcase      
   end      
endmodule
`endif
