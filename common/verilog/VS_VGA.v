module VS_VGA #(parameter D=480, F=10, R=2, B=33)
					(input c, hend, pre,
					output vs, vend, output reg [9:0] vpix );
	reg u;

`ifdef OPT640x480
	wire l = vpix[9] & vpix[3] & vpix[2];
	assign vs = ~vpix[8] | ~vpix[7] | ~vpix[6] | ~vpix[5] | vpix[4] | ~vpix[3] | vpix[2] | ~vpix[1];	
`else
	wire l = (vpix > (D+F+R+B-2));
	wire syn1 = (vpix < (D+F));
	wire syn2 = (vpix > (D+F+R-1));
	assign vs =  syn1 | syn2;
`endif

	assign vend = u;

	always @(posedge c) begin
		if (u) vpix <= 0;
			else if (hend) vpix <= vpix + 1;
			
		if (u) u<=0;
			else if (pre) u <= l;				/* vend pulse generator */
	end

	initial begin
		vpix <= 0; u <= 0;
	end
endmodule

// Engineer: Edison Llerena					Create Date:   17:01:44 08/17/2011
// Description: VGA Sync Generator
// History
// 19-09-2011	Counter optimized max speed 250.627MH