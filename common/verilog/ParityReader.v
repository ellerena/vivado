module ParityReader(	input c, hold, datRdy, tic, i, oddPar, noPar,
											output reg done, output parErr);

	reg oldPar;
	wire newPar = i ^ oldPar;
	wire read = tic & ~done;
	wire skip = noPar & datRdy;
	assign parErr = oldPar ^ oddPar;
	
	always @ (posedge c) begin
		if (hold) done <= 0;
			else if (skip) done <= 1;
				else if (tic) done <= datRdy;
			
		if (hold) oldPar <= 0;
			else if (read) oldPar <= newPar;

	end
	
	initial begin
		done = 0;
		end
endmodule

//module ParityReader(	input c, hold, tic, noPar, evenPar, i,
//											output reg done, par);
//
//	wire skip = run & noPar;
//	wire read = run & tic & ~done;
//	
//	always @ (posedge c) begin
//		if (read) par <= i;
//
//		if (r) done <= 0;
//			else if (skip) done <= 1'b1;
//				else if (read) done <= 1'b1;
//		
//		if (hold) oddPar <= 0;
//			else if (run) oddPar <= nxtPar;
//	end
//	
//	initial begin
//		par = 0; done = 0;
//		end
//endmodule
//Design by Edison Llerena
//r: reset done = 0
//run: start parity process
//tic: sync signal
//i: input signal (parity bit)
//noPar: Don't read parity (from configuration vector)
//done: process completed
//par: Parity Bit value
//02-11-2011
//	Minimum period: 4.119ns (Maximum Frequency: 270.636MHz)
//	Minimum input arrival time before clock: 4.415ns
//	Maximum output required time after clock: 7.241ns
