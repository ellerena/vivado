module DisplayTextIF #(parameter RBITS=`RED_BITS, GBITS=`GREEN_BITS, BBITS=`BLUE_BITS)
							(input c_div2, c_n,
							input w, input[11:0]vid_buf_A, input [7:0] vid_buf_D,
							input [7:0] shadecolr, txtcolr, bgcolr,
							output hs, vs, frameEnd,
							output [RBITS-1:0] r, output [GBITS-1:0] g, output[BBITS-1:0] b,
							output [9:0] hpix, vpix);

	wire vact, cursor, shade, pre_hs, pre_vs, pre_vact;
	wire [7:0] char_code, pixel8;
	
	VideoSync vsy(c_div2, pre_hs, pre_vs, pre_vact,, frameEnd, hpix, vpix);
	VideoRam2P vr2p(c_n, w, vid_buf_A, {vpix[8:4], hpix[9:3]} /* mem_addrR */, vid_buf_D, char_code);
	FontRom fr({char_code[6:0], vpix[3:0]}, pixel8);
	VideoPen #(`RED_BITS, `GREEN_BITS, `BLUE_BITS)
				vp	(c_div2, vact, shade_, cursor, hpix[2:0], pixel8, shadecolr, txtcolr, bgcolr, r, g, b );

`ifdef USE_VGA_CURSOR
	VideoCursor vc(c_div2, frameEnd, vid_buf_A, {vpix[8:4], hpix[9:3]}, vpix[3:2], cursor);
`else
	assign cursor = 1'b0;							/* disable cursor */
`endif

`ifdef USE_VGA_SHADER
	// Temporal //
	assign shade_ = shade | char_code[7];  // normal: remove shade_ and replace with shade
	// Temporal End //
	VideoShader vs1({vpix[8:4], hpix[9:6]},  vid_buf_A[11:3], shade );
`else
	assign shade = 1'b0;							/* turn shader permanently off */
`endif

`ifdef USE_VGA_HV_BUFFER
	OutputBuffer #(3) ob(c_div2, {pre_hs, pre_vs, pre_vact}, {hs, vs, vact});
`else													/* don't buffer the outputs */
	assign hs = pre_hs;
	assign vs = pre_vs;
	assign vact = pre_vact;
`endif
	
endmodule

/**
	@brief	Display interface (Text mode).
	@input	c_div2, c_n: input clocks where freq_c_ = 2*freq_c
				w: write pulse, used to request a write into video ram
				vid_buf_A: video ram address where data is to be stored
				vid_buf_D: data value to be stored in vid_buf_A address in video ram
				shadecolor: bold text color (8bit)
				txtcolor: text foreground color (8bit)
				bgcoloc: text background color (8bit).
	@output	hs: horizontal sync signal
				vs: vertical sync signal
				frameEnd: end of frame tick (used to clock other external modules)
				r, g, b: red, green and blue outputs.
				hpix, vpix: horizontal and vertical pixel clock				
*/

