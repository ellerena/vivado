`timescale 1ns / 1ps
`define CKPER_NS		(1e9/50e6)		/* 20ns period */
`define CH_OFFSET      (1)          /* small offset to simulate rise/fall delay */

module cod2bin(input [3:0]i, output [3:0] o);
reg [3:0] o_;
assign o = o_;

	always @ *
		case (i)
4'h0: o_ <= 4'h0;
4'h1: o_ <= 4'h1;
4'h2: o_ <= 4'h2;
4'h5: o_ <= 4'h3;
4'hB: o_ <= 4'h4;
4'h6: o_ <= 4'h5;
4'hD: o_ <= 4'h6;
4'hA: o_ <= 4'h7;
4'h4: o_ <= 4'h8;
4'h9: o_ <= 4'h9;
4'h3: o_ <= 4'hA;
4'h7: o_ <= 4'hB;
4'hF: o_ <= 4'hC;
4'hE: o_ <= 4'hD;
4'hC: o_ <= 4'hE;
4'h8: o_ <= 4'hF;
		endcase
endmodule

module T_module;
	reg [15:0] _clk, _chk;
	wire _f0;

   reg c, r, cap;
//   reg [31:0] pm;
//   reg [7:0] addR;
   wire [11:0] o, o_;
//   wire [31:0] o;
//   pmodCapture32 _pmod( c, r, cap, pm, addR, cnt, o );
	fast_counter _fc(c, r, cap, o_);
	cod2bin co0 (o_[3:0], o[3:0]);
	cod2bin co1 (o_[7:4], o[7:4]);
   cod2bin co2 (o_[11:8], o[11:8]);
  
	assign _f0 = (o == _chk) ? 0:1;

/********************* Initial conditions *********************/
	initial begin
		{c, r, cap} = {3'b100};
//      addR <= 0; pm <= 32'hfeef;
      _clk = 0;
      _chk = 0;
      fork
         forever #(`CKPER_NS/2) c = ~c;
         #`CH_OFFSET forever _clk= #(`CKPER_NS) ((_clk + 1));
			#60 forever _chk= #20 _chk + 1;
//         #`CH_OFFSET forever #(4*(`CKPER_NS)) cap = ~cap; 
//         #(2*(`CKPER_NS)+`CH_OFFSET) forever #(8*(`CKPER_NS)) pm = $random;
//         #(6*(`CKPER_NS)+`CH_OFFSET) forever #(8*(`CKPER_NS)) addR = addR + 1;
      join
	end

/********************* Stimulus Signals ***********************/
	always @ (_clk)
	begin
		case (_clk)
         1: r = 1;
         2: r = 0;
			3: cap = 1;
		endcase      
	end		
endmodule
