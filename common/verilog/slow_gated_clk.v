/**
 Engineer: Eddie Llerena
 Create Date:    21:11:05 07/15/2002 
 Description:    Divides a clock into a slower one by
                 division and gating.
*/

module slow_gated_clk #(parameter DIVIDE_BY = 5)
                       (input clk, output sg_clk);

reg [$clog2(DIVIDE_BY)-1:0] k;               /* our main counter */

wire gate_enable = (k > (DIVIDE_BY - 2));    /* value of k when gate is enabled */
wire [$clog2(DIVIDE_BY)-1:0] zeromine = 0;   /* trick: reset value for k */

always @(posedge clk)                   /* counter, counts up until */
   k <= gate_enable ? zeromine: k + 1'h1;    /* <gate_enable>, then resets */

BUFGCE bufgce_i0 (.I(clk), .CE(gate_enable), .O(sg_clk));

initial k <= zeromine;

endmodule

/////////////////////////////////////////////////////////////////////////////////////
//`define __TST_sg_clk__
`ifdef __TST_sg_clk__
`timescale 1us / 1ns

module T_module;

   reg c;                      /* input signals */
   wire o;                     /* output signals */

   slow_gated_clk #(9) _slk (c, o); /* uut */

/********************* Initial conditions *********************/
   always #5 c = ~c;           /* generate main clk */
	initial c = 1'b0;

endmodule
`endif
