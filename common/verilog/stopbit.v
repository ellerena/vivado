module stopbit(	input c, doneP, tic, i, input [1:0] len,
								output done, output reg frmErr );
								
	reg [2:0] k;
	assign done = (k=={1'b1, len});
	
	always @ (posedge c) begin
		if (~doneP) k <= 3'b010;
			else if (tic & ~done) k <= k+1;
				
		if (~doneP) frmErr <= 0;
			else if (frmErr) frmErr <= 1;
				else if (~done & tic & doneP) frmErr <= ~i;
	end
	
	initial begin
		frmErr = 0;
		end
endmodule

//module stopbit(	input c, r, doneP, tic, tic2, i, input [2:0] len,
//								output reg done, frmErr );
//								
//	reg exec;
//	reg [2:0] vec;
//	
//	wire rota = tic2 & exec;
//	wire evec = rota | r;
//	wire [2:0] mux = rota ? {vec[1:0], 1'b0} : len;
//	
//	always @ (posedge c) begin
//		if (done) exec <= 0;
//			else if (tic) exec <= doneP;
//			
//		if (i) frmErr <= 1'b0;
//			else if (exec) frmErr <= 1'b1;
//			
//		if (frmErr) vec <= 0;
//			else if (evec) vec <= mux;
//		
//		if (vec[2]) done <= 1'b0;
//			else if (exec) done <= 1'b1;
//	end
//	
//	initial begin
//		done = 0; frmErr = 0;
//		end
//endmodule

//Design by Edison Llerena
//r: resets the process
//doneP: initiates stop bit verification
//tic, tic2: from bad rate generator
//i: data signal input to receiver
//len: length of stop bit as per config. vector
//	1 bit stop -> 100, 1.5 bit stop -> 110, 2 bit stop -> 111
//done: process complete.
//frmErr: stop bit verification failed
//02-11-2011
//	Minimum period: 3.851ns (Maximum Frequency: 259.673MHz)
//	Minimum input arrival time before clock: 4.021ns
//	Maximum output required time after clock: 7.281ns