module dbouncer #(parameter W=12, M=1000000) ( input i, c, output o );
	reg [W-1:0] k;
	reg st, nx;

	assign o = st;
	
	always @(posedge c)
		begin
			if (nx) k <= k - 1;
			else k <=M;
			st <= nx; 
		end
	
	always @(*)
		case(st)
			0:	nx = i;
			1:	nx = k ? 1 : 0;
		endcase

endmodule
