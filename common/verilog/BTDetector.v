module BTDetector #(parameter A=4, B=4) ( input c, i, t, output reg tic );

	reg [B-1:0] cnt;
	
	wire a = (cnt == A);
	
	always @ (posedge c)
		begin
			if (!i) cnt <= 0;
			else if (t) cnt <= cnt + 1;
			
			if (tic) tic <= 0;
			else if (t) tic <= a;
		end
		
		initial begin
			cnt <= A; tic <=0;
			end
		
endmodule

/*
Engineer: Edison Llerena					Create Date:   17:01:44 08/17/2011
Description: Button Press Detector
History
22-09-2011 383.142MHz
19-09-2011	302.755MHz

	@brief	Button detector: detect and de-bounces the press of a button.
				If i is true (button pressed) for A periods of t, then outputs
				a 1 clock duration pulse (tick). If i remains true (long press) after
				A+(2^B) periods of t then a new pulse is generated.

	@input	i: is the input signal (from a button)
				t: acts as a event counter used to count how long hast the
					input been active.
				o: outputs 1 tick (1 clock duration) if the input
					i has been active for 2^A clock cycles.

A: number in range [1, (2^B)-1] that determines how long must the input remain pressed
		before it is considered a true event.
B: determines the time a button must be depressed to be considered "long press"

*/