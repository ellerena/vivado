`timescale 1ns / 1ps

module VideoCursor( input c, frameEnd, input [11:0] extA, intA, input [1:0] vlast,
										output cursor);

	reg [4:0] conta = 0;	//conta: controls blinking speed (in frames.)

		always @ (posedge(c))
			if (frameEnd) conta <= conta + 1;
	
	assign cursor = (extA == intA) & &vlast & conta[4];

endmodule
