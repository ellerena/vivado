module Seg7Module(input seg7_off, input[1:0] seg7_rotator, channel_sel, input [79:0] i,
						output [3:0] anode_o, output [6:0] segment_o);

	wire [4:0] code;
	wire [19:0] codi;
										
	seg7sel s7s_ (channel_sel, i, codi); 
	seg7mux	s7m_ (seg7_off, codi, seg7_rotator, anode_o, code);
	seg7hex s7h_ (code, segment_o);

endmodule

/**
	@author	Edison Llerena - 27-10-2010

	@brief	4 channel 7 segment display module.
	@input	channel_sel: selects one of the channels to be used for display.
				seg7_rotator: rotating signal that sequentially selects each of
				the four 7-segment displays (00>01>10>11..) (note: defines refresh rate).
				i: is a group of four 20bit signals, one of which will be used
					as source for display.
				off: turns display on/off
	@output	anode_o: anode control signals, each signal enables one of the 
							four 7-segment displays.
				segment_o: 7-segment signals.
*/
