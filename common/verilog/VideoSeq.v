module VideoSeq(	input c, _cf, hend, vend, vact, input [2:0] hpix, input [3:0] vpix, 
									output reg newc, output [11:0] addr);

	reg [11:0] new, old;

	wire lastv = (vpix == 4'hf);
	wire lasth = vact & hpix[2] & hpix[1] & hpix[0];
	wire sline= hend & ~lastv;
	wire nline= hend & lastv;
	wire go = lasth | sline;
	
	assign addr = new;
	
	always @(posedge _cf) begin
		if (vend) new <= 0;
			else if (go)
						if (sline) new <= old;
						else new <= new + 1;
		
		if (vend) old <= 0;
			else if (nline) old <= new;
	end
	
	always @(posedge c)
			if(lastv) newc <= lasth;
		
	initial begin
		new <= 0; old <=0; newc<= 0;
		end
endmodule

// Edison Llerena - 12:10:30 10/30/2010
// Generates address to access text video ram
// History
// 26-09-2011 248.324MHz