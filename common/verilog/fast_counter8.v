/////////////////////////////////////////////////////////////////////////////////
// Engineer: Edison Llerena
// Create Date:    07/11/2018
// Module Name:    fast_counter
// Revision 0.01 - File Created
/////////////////////////////////////////////////////////////////////////////////
module fast_cnt8 #(parameter W=16) (input c, r, e, output [(W-1):0] o);
/////// ver 1; 415.628 MHz
localparam SEED = 16'hb4f0;
reg [11:0] nb0, nb1;
reg [3:0] no0, no1;
reg pre, nx1;

wire tr1 = (nb0[3]);
assign o = {nb1[3:0], no1, nb0[3:0], no0};

always @ (posedge c) begin
   nb0 <= r ? SEED[15:4] : e ? {nb0[10:0], no0[3]} : nb0;
	no0 <= r ? SEED[3:0]  : e ? {no0[2:0], nb0[11]} : no0;
	nb1 <= r ? SEED[15:4] : nx1 ? {nb1[10:0], no1[3]} : nb1;
   no1 <= r ? SEED[3:0] : nx1 ? {no1[2:0], nb1[11]} : no1;
end

always @ (posedge c or posedge nb0[11])
   pre <= nb0[11] ? 1'h0 : e ? tr1 : pre;

always @ (posedge c)
   nx1 <= pre;

endmodule

