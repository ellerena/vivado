module seg7mux( input seg7_off, input [19:0] i, input [1:0] channel,
					output [3:0] anode_o, output reg [4:0] seg_5_o);

reg [3:0] anode_pre;
assign anode_o = anode_pre | {4{seg7_off}};		/* allows to turn off all the seg 7 */

	always @(*)
		case(channel)
			2'b00: begin
							seg_5_o <= i[4:0];
							anode_pre <= 4'b1110;
							end
			2'b01: begin
							seg_5_o <= i[9:5];
							anode_pre <= 4'b1101;
							end
			2'b10: begin
							seg_5_o <= i[14:10];
							anode_pre <= 4'b1011;
							end
			2'b11: begin
							seg_5_o <= i[19:15];
							anode_pre <= 4'b0111;
							end
		endcase
endmodule

/**
	@author	Edison Llerena - 12:10:30 10/30/2010 
	@brief	Mux to select and signal 1-of-4 output displays.
	@input	channel: a bit code indicating which output display will be enabled. 
				i: a 20bit code composed of 4 separate 5bit codes. Each code is used for a separate
				channel.
				off: turns the display completely on/off
	@output	anode_o: anode_o 4bit decode that will enable one single output channel.
				seg_5_o: anode_o 5 bit code which is the selected subset of i.
*/