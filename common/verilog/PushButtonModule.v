module PushButtonModule # (parameter A=4, B=4, C=4 /* 4 buttons */, D=4'b1101)
							( input c, vend, input [C-1:0] bt,
								output [C-1:0] b );

		genvar x;
	generate
		for (x=0; x < C; x = x+1) 
		begin: boton
			if (D[x]) BTDetector #(A, B) u_ (c, bt[x], vend, b[x]);
			else assign b[x] = bt[x];
	  end
	endgenerate
	
endmodule


/**

	@brief	PushButtonModule, controls the use of the board's push buttons.
				uses a generator to create the circuitry for each push buttons.
				C: number of buttons to monitor.
				D: bit mask where a 1 creates a BTDetector, a 0 makes a direct wire.
				A, B: are parameters passed to the BTDetector
	@input	vend: is a pulsing signal used to count the duration of a button press
				bt: push buttons inputs.
	@output	b: is a vector where each line represents the state of its correspondent
					push button
*/
