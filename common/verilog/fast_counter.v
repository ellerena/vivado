/////////////////////////////////////////////////////////////////////////////////
// Engineer: Edison Llerena
// Create Date:    07/11/2018
// Module Name:    fast_counter
// Revision 0.01 - File Created
/////////////////////////////////////////////////////////////////////////////////

module fast_counter (input c, r, e, output [11:0] o);
//////// ver 2; 402.091 MHz
localparam SEED=16'hb4f0;
(* KEEP = "TRUE" *) reg [11:0] nb0, nb1, nb2;
reg [3:0] no0, no1, no2;
reg sel2, pre, nx1, nx2;
assign o = {no2, no1, no0};

wire tr1 = (nb0[3]);
wire tr2 = (nb1[9]|no1[1]);

always @ (posedge c or posedge r) begin
	nb0 <= r ? SEED[15:4] : e ? {nb0[10:0], no0[3]} : nb0;
	no0 <= r ? SEED[3:0] : e ? {no0[2:0], nb0[11]} : no0;
	nb1 <= r ? SEED[15:4] : nx1 ? {nb1[10:0], no1[3]} : nb1;
	no1 <= r ? SEED[3:0] : nx1 ? {no1[2:0], nb1[11]} : no1;
	nb2 <= r ? SEED[15:4] : nx2 ? {nb2[10:0], no2[3]} : nb2;
	no2 <= r ? SEED[3:0] : nx2 ? {no2[2:0], nb2[11]} : no2;
   end

always @ (posedge c or posedge nb1[11])
   sel2 <= nb1[11] ? 1'h1 : e ? tr2 : sel2;

always @ (posedge c or posedge nb0[11])
   pre <= nb0[11] ? 1'h0 : e ? tr1 : pre;

always @ (posedge c)
   nx1 <= pre;

always @ (posedge c or posedge sel2)
   nx2 <= sel2 ? 1'h0 : pre;

endmodule
