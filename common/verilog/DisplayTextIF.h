`ifdef SIMUL					/* for simulation and debugging purposes */
`define VIDEOSYNC_A	10
`define VIDEOSYNC_B	2
`define VIDEOSYNC_C	3
`define VIDEOSYNC_D	2
`define VIDEOSYNC_E	8
`define VIDEOSYNC_F	1
`define VIDEOSYNC_G	2
`define VIDEOSYNC_H	1
`elsif RES_800x600					/* for 800x600 @ 72 Hz VGA display */
`define VIDEOSYNC_A	800
`define VIDEOSYNC_B	56
`define VIDEOSYNC_C	120
`define VIDEOSYNC_D	64
`define VIDEOSYNC_E	600
`define VIDEOSYNC_F	37
`define VIDEOSYNC_G	6
`define VIDEOSYNC_H	23
`else								/* for 640x480 @ 60Hz VGA display */
`define VIDEOSYNC_A	640
`define VIDEOSYNC_B	16
`define VIDEOSYNC_C	96
`define VIDEOSYNC_D	48
`define VIDEOSYNC_E	480
`define VIDEOSYNC_F	10
`define VIDEOSYNC_G	2
`define VIDEOSYNC_H	33
`endif