module BusAllInOneOut	#	( parameter W = 32 )
												(	input [W-1:0] parE, input [8*W-1:0] busi,
													output [7:0] buso );

	genvar x;
	generate
		for (x=0; x < W; x = x+1) 
		begin: buffers
			 Tbuffer u_ (parE[x], busi[8*x+7:8*x], buso);
		end
	endgenerate

endmodule

module Tbuffer (input e, input [7:0] i, output [7:0] o);

	assign o = e ? i : 8'bzzzzzzzz;

endmodule

/**
*/
