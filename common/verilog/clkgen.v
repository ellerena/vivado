/////////////////////////////////////////////////////////////////////////////////
// Engineer: Edison Llerena
// Create Date:    23:01:03 10/26/2010 
// Module Name:    clkgen 
// Revision 0.01 - File Created
/////////////////////////////////////////////////////////////////////////////////

module clkgen #(parameter W=16, O=2) (input clk, input r, output [O-1:0] o);

	reg [3:0] pre;
	reg [W-5:0] post;
	always @(posedge clk, posedge r)
		begin
			if(r)
				begin
					pre <= 0;
					post <=0;
				end
			else
				begin
					pre <= pre + 1;
					if (pre == 4'b1111) post <= post +1;
				end
		end
	assign o = post[W-5: W-4-O];
endmodule
