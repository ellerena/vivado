module ParamRecord(	input c2, c, b, wrt , vend, newc, input [3:0] mloc, input [7:0] datai,
											output reg w, output [6:0] datao);
	
	reg f;
	
	assign datao[6:4] = 3'h3;
	assign datao[3:0] = mloc[0] ? datai[3:0]:datai[7:4];
	
	always @ (posedge c)
		if (vend) f<= wrt;

	always @ (posedge c2)
		if (f & b ) w <= mloc[3] & mloc[2] & mloc[1] & newc;	
		
	initial begin
		f <= 0; w<=0;
		end
endmodule

// Create Date:    16:44:58 08/17/2011 
// Module Name:    ParamRecord 
// History:
// 27-09-2011
//   Minimum period: No path found
//   Minimum input arrival time before clock: 2.825ns
//   Maximum output required time after clock: 4.283ns
//   Maximum combinational path delay: 6.376ns
//