module MixedModeClkMgr #(parameter A=`CLOCK_INPUT_PER_NS)
						( input gCLK, r,
						output c, c_n, c_div2, c_div64, c_div64_n, c_Ok);

	wire cfb;

//							// PLLE2_BASE: Base Phase Locked Loop (PLL)
//							// 7 Series
//							// Xilinx HDL Libraries Guide, version 2012.2
//	PLLE2_BASE #(
//	.BANDWIDTH("OPTIMIZED"),	// OPTIMIZED, HIGH, LOW
//	.CLKFBOUT_MULT(2),			// Multiply value for all CLKOUT, (2-64)
//	.CLKFBOUT_PHASE(0.0),		// Phase offset in degrees of CLKFB, (-360.000-360.000).
//	.CLKIN1_PERIOD(A),			// Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
//								// CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
//	.CLKOUT0_DIVIDE(1),
//	.CLKOUT1_DIVIDE(1),
//	.CLKOUT2_DIVIDE(2),
//	.CLKOUT3_DIVIDE(128),
//	.CLKOUT4_DIVIDE(128),
//	.CLKOUT5_DIVIDE(1),
//								// CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
//	.CLKOUT0_DUTY_CYCLE(0.5),
//	.CLKOUT1_DUTY_CYCLE(0.5),
//	.CLKOUT2_DUTY_CYCLE(0.5),
//	.CLKOUT3_DUTY_CYCLE(0.5),
//	.CLKOUT4_DUTY_CYCLE(0.5),
//	.CLKOUT5_DUTY_CYCLE(0.5),
//								// CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
//	.CLKOUT0_PHASE(0.0),
//	.CLKOUT1_PHASE(180.0),
//	.CLKOUT2_PHASE(0.0),
//	.CLKOUT3_PHASE(0.0),
//	.CLKOUT4_PHASE(180.0),
//	.CLKOUT5_PHASE(0.0),
//	.DIVCLK_DIVIDE(4),			// Master division value, (1-56)
//	.REF_JITTER1(0.0),			// Reference input jitter in UI, (0.000-0.999).
//	.STARTUP_WAIT("FALSE")		// Delay DONE until PLL Locks, ("TRUE"/"FALSE")
//	)
//	PLLE2_B_0 (
//								// Clock Outputs: 1-bit (each) output: User configurable clock outputs
//	.CLKOUT0(c),
//	.CLKOUT1(c_n),
//	.CLKOUT2(c_div2),
//	.CLKOUT3(c_div64),
//	.CLKOUT4(c_div64_n),
////	.CLKOUT5(CLKOUT5),
//								// Feedback Clocks: 1-bit (each) output: Clock feedback ports
//	.CLKFBOUT(cfb),				// 1-bit output: Feedback clock
//								// Status Port: 1-bit (each) output: PLL status ports
//	.LOCKED(c_Ok),				// 1-bit output: LOCK
//								// Clock Input: 1-bit (each) input: Clock input
//	.CLKIN1(gCLK),				// 1-bit input: Input clock
//								// Control Ports: 1-bit (each) input: PLL control ports
//	.PWRDWN(1'b0),				// 1-bit input: Power-down
//	.RST(r),					// 1-bit input: Reset
//								// Feedback Clocks: 1-bit (each) input: Clock feedback ports
//	.CLKFBIN(cfb)			// 1-bit input: Feedback clock
//	);


	MMCME2_BASE #(
	.BANDWIDTH("OPTIMIZED"),	// Jitter programming (OPTIMIZED, HIGH, LOW)
	.CLKFBOUT_MULT_F(12.0),		// Multiply value for all CLKOUT (2.000-64.000).
	.CLKFBOUT_PHASE(0.0),		// Phase offset in degrees of CLKFB (-360.000-360.000).
	.CLKIN1_PERIOD(A),			// Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
								// CLKOUT0_DIVIDE - CLKOUT6_DIVIDE: Divide amount for each CLKOUT (1-128)
	.CLKOUT1_DIVIDE(12),
	.CLKOUT2_DIVIDE(24),
	.CLKOUT3_DIVIDE(128),
	.CLKOUT4_DIVIDE(1),
	.CLKOUT5_DIVIDE(1),
	.CLKOUT6_DIVIDE(1),
	.CLKOUT0_DIVIDE_F(1),		// Divide amount for CLKOUT0 (1.000-128.000).
								// CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for each CLKOUT (0.01-0.99).
	.CLKOUT0_DUTY_CYCLE(0.5),
	.CLKOUT1_DUTY_CYCLE(0.5),
	.CLKOUT2_DUTY_CYCLE(0.5),
	.CLKOUT3_DUTY_CYCLE(0.5),
	.CLKOUT4_DUTY_CYCLE(0.5),
	.CLKOUT5_DUTY_CYCLE(0.5),
	.CLKOUT6_DUTY_CYCLE(0.5),
								// CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
	.CLKOUT0_PHASE(0.0),
	.CLKOUT1_PHASE(0.0),
	.CLKOUT2_PHASE(0.0),
	.CLKOUT3_PHASE(0.0),
	.CLKOUT4_PHASE(0.0),
	.CLKOUT5_PHASE(0.0),
	.CLKOUT6_PHASE(0.0),
	.CLKOUT4_CASCADE("FALSE"),	// Cascade CLKOUT4 counter with CLKOUT6 (FALSE, TRUE)
	.DIVCLK_DIVIDE(2),			// Master division value (1-106)
	.REF_JITTER1(0.0),			// Reference input jitter in UI (0.000-0.999).
	.STARTUP_WAIT("FALSE")		// Delays DONE until MMCM is locked (FALSE, TRUE)
	)
	MMCME2_BASE_inst (
							// Clock Outputs: 1-bit (each) output: User configurable clock outputs
	.CLKOUT1(c),			// 1-bit output: CLKOUT0
	.CLKOUT1B(c_n),			// 1-bit output: Inverted CLKOUT0
	.CLKOUT2(c_div2),		// 1-bit output: CLKOUT1
	.CLKOUT3(c_div64),		// 1-bit output: CLKOUT2
	.CLKOUT3B(c_div64_n),	// 1-bit output: Inverted CLKOUT2
							// Status Ports: 1-bit (each) output: MMCM status ports
	.LOCKED(c_Ok),			// 1-bit output: LOCK
							// Clock Inputs: 1-bit (each) input: Clock input
	.CLKIN1(gCLK),			// 1-bit input: Clock
							// Control Ports: 1-bit (each) input: MMCM control ports
	.PWRDWN(1'b0),			// 1-bit input: Power-down
	.RST(r),				// 1-bit input: Reset
							// Feedback Clocks: 1-bit (each) input: Clock feedback ports
	.CLKFBOUT(cfb),
	.CLKFBIN(cfb)			// 1-bit input: Feedback clock
	);

endmodule
