module ParamSelect(	input c, ad, da, adl_dau, input[4:0] addri,
										output [4:0] addro, output [7:0]datao, output reg[31:0] parA );

	reg [4:0] addr;
	reg [7:0] data[31:0];

	initial $readmemh("par32x8.hex", data, 5'h00, 5'h1f); 

	assign addro = addr;
	assign datao = data[addr];

	always @ (posedge c) begin
		if (ad)
			if (adl_dau) addr <= addri;
				else addr <= addr + 1;
				
		if (da) data[addr] <= datao + (adl_dau ? 8'h01: 8'hff);	
		
		case (addr)
			5'h00 : parA <= 32'h00000001;
			5'h01 : parA <= 32'h00000002;
			5'h02 : parA <= 32'h00000004;
			5'h03 : parA <= 32'h00000008;
			5'h04 : parA <= 32'h00000010;
			5'h05 : parA <= 32'h00000020;
			5'h06 : parA <= 32'h00000040;
			5'h07 : parA <= 32'h00000080;
			5'h08 : parA <= 32'h00000100;
			5'h09 : parA <= 32'h00000200;
			5'h0A : parA <= 32'h00000400;
			5'h0B : parA <= 32'h00000800;
			5'h0C : parA <= 32'h00001000;
			5'h0D : parA <= 32'h00002000;
			5'h0E : parA <= 32'h00004000;
			5'h0F : parA <= 32'h00008000;
			5'h10 : parA <= 32'h00010000;
			5'h11 : parA <= 32'h00020000;
			5'h12 : parA <= 32'h00040000;
			5'h13 : parA <= 32'h00080000;
			5'h14 : parA <= 32'h00100000;
			5'h15 : parA <= 32'h00200000;
			5'h16 : parA <= 32'h00400000;
			5'h17 : parA <= 32'h00800000;
			5'h18 : parA <= 32'h01000000;
			5'h19 : parA <= 32'h02000000;
			5'h1A : parA <= 32'h04000000;
			5'h1B : parA <= 32'h08000000;
			5'h1C : parA <= 32'h10000000;
			5'h1D : parA <= 32'h20000000;
			5'h1E : parA <= 32'h40000000;
			5'h1F : parA <= 32'h80000000;
	endcase
		
	end

/*		integer i;
		initial begin
		for (i=0; i<32; i=i+1) data[i] = 8'd127;
		data[29] = 8'd194; data[30] = 8'd255; data[32] = 8'd3; addr <= 0;
		end*/
endmodule

// Create Date:    16:44:58 08/17/2011 
// Module Name:    ParamSelect
// if ad then if adl loads the address addri, otherwise increasses addr
// if dl then if dau increases the value stored in data[addr], otherwise decreases it
// Description: 174.398MHz