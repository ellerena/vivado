`timescale 1ns / 1ps
`include "BoardIF.h"

module BoardIF( input [7:0] flags,
				input [7:0] switch_input,
				input pbClk, pbStr, input[3:0] pb_input,
				input[19:0] s7ch3, s7ch2, s7ch1, s7ch0, input[1:0] seg7_rotator,
				input[7:0] led_channel_3, led_channel_2, led_channel_1, led_channel_0,
				output [7:0] switch_state,
				output [3:0] pb_state,
				output [6:0] seg7_o, output [3:0] seg7_an_o,
				output [7:0] led_output
				);

	wire seg7_off = flags[`SEG7OFF];
	wire [1:0] seg7_channel = flags[`CHAN_S7_MSB:`CHAN_S7_LSB];
	wire [1:0] led_channel = flags[`CHAN_LED_MSB:`CHAN_LED_LSB];

	PushButtonModule #(4, 4, 4, 4'b1111) pbm(pbClk, pbStr, pb_input, pb_state);
	Seg7Module s7(seg7_off, seg7_rotator, seg7_channel, {s7ch3, s7ch2, s7ch1, s7ch0}, seg7_an_o, seg7_o);
	LedBlock ld (led_channel, {led_channel_3, led_channel_2, led_channel_1, led_channel_0}, led_output);
	assign switch_state = switch_input;
	
endmodule

/**
	@brief	Generic Board Interface, controls and monitors the board's
				generic i/o which are: 4 push buttons, 8 switches, 8 LEDs
				and 4 7-segment displays.

	@input	flags: a byte containing flags that control module operation.
				switch_input: 8 switches.
				pb_input: 4 push buttons
				

	@output	seg7_o: 7bit code signal for the 7-segments.
				seg7_an_o: anode selector for the 7-segments.
				led_o: led driving signals (8 bit).
				pb_state: 4 bit code representing the state of the 4 push buttons
				switch_state: 8 bit code representing the state of the 8 switches
				
*/