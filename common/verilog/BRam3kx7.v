module VideoRam2P( input c, w, input [11:0] addrW, addrR, input [7:0] datai,
						output reg [7:0] char_code );

	reg [7:0] ram[4095:0];

	(* RAM_STYLE="{AUTO | BLOCK |  BLOCK_POWER1 | BLOCK_POWER2}" *)
	initial $readmemh(`VIDEO_BUF_FILE, ram, 12'h0, 12'd4095); 
	
	always @ (posedge c) begin
			char_code <= ram[addrR];
			if (w) ram[addrW] <= datai;
		end
endmodule


/**
	@brief	Video ram (text mode) containing the character codes.
	@input	w: pulse requesting to write datai into address addrW.
				addrW: 12bit address where new data is written.
				addrR: 12bit address of data being read.
				datai: 8bit new data value to be stored @ addrW.
	@output	char_code, is the current value stored @ addrR.
*/