module Variable #	(	parameter A=0)
						( input c, parE, input [7:0] dataN,
						output reg [7:0] var );

	always @ (posedge c)
		if (parE) var <= dataN;

	initial var <= A;
endmodule

/**

*/
