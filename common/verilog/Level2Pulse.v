module Level2Pulse #(parameter L=1, S=0)(input c, i, output o);

   reg x = 0;

   if (S == 0) begin
      assign o = L ? ~x & i : x & ~i;
      always @ (posedge c) x <= i;
   end
   else begin
      reg a;
      assign o = x;

      always @(posedge c) begin
         x <= a ? 1'b0 : i;
         a <= i;
      end
   end

endmodule

// Edge Detector.
// S = 0: 
// Asynchronous edge detection with reset of next immediate clock
//	when input i takes value L <1 or 0>, output rises (asynchronous) and
// remains high until next clock edge, it returns to low then.
// S <> 0:
// Synchronous edge detection. If input i has value L <1 or 0>
// during clock edge, the output is set to high for one clock cycle.
// It returns to low on the next clock edge.
//	L = logic, 1 = pulse when (i) goes to 1; 0 = pulse when (i) goes to 0
