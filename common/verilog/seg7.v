//
module seg7( input [19:0]i, input [1:0]d, output [3:0]a, output [6:0]s );
	wire [4:0] w0;
	seg7mux seg7mux_ (i, d, a, w0);
	seg7hex seg7hex_ (w0, s);
endmodule
