// rise_edge
// Edison Llerena - 12:10:30 10/30/2010 
// Gens a pulse of duration=<1/2> clocks when i is togled high and l (lock) is 0.
// Revision 0.01 - File Created

//module rise_edge #(parameter cycle=2) (input i, l, c, output o);
//   localparam	IDL = 2'b00, ONE = 2'b11, TWO = 2'b10, LCK = 2'b01;
//   reg [1:0] now, nxt;
//
//   assign o = now[1];	
//
//   always @(posedge c)
//      now <= nxt;
//
//   always @(*)
//      case(now)
//         IDL:	nxt = (i & ~l)? ONE : IDL;
//         ONE:	nxt = cycle;
//         LCK:	nxt = i ? LCK : IDL;
//         TWO:	nxt = LCK;
//      endcase
//endmodule

/*
   2020 refactored version
   detects a rise on signal <i> then
   generates a locked high output.
   To unlock use a synchronous r pulse
*/
module rise_edge (input c, r, i, output o);

   reg o_;
   assign o = o_;

   always @ (posedge c)
      o_ <= r ? 1'b0 : ( o | i );

   initial o_ <= 1'b0;

endmodule
