module ParamFormat( input c, run, binary, input [7:0] input_value,
										output done, output [31:0] output_value );
	wire [11:0] output_bcd;	
	wire [31:0] bin = { 3'b0, input_value[7], 3'b0, input_value[6], 3'b0, input_value[5], 3'b0, input_value[4],
								3'b0, input_value[3], 3'b0, input_value[2], 3'b0, input_value[1], 3'b0, input_value[0] };
	wire [31:0] dec = { 20'h0, output_bcd};
	
	assign output_value = binary ? bin : dec;

	Bin2BCD b2b_ (c, run, input_value, done, output_bcd);
	
endmodule

/**
	@brief	received an 8bit input value and outputs the
				same value either as a binary number or as BCD.
	@input	run: a pulse to start the data conversion process
				binary: select the output format as binary or bcd
				input_value: the 8bit input value to process
	@outpout	done: for BCD conversion, signal level indicating conversion completed
				output_value: result (arranged in 32 bit format)
*/
