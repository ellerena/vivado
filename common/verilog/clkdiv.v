`timescale 1ns / 1ps
/*
	Company: 
	Engineer: Edison Llerena
	Description: generates a 1 clock tick each DIV clock periods.
 
 Create Date:    18:59:37 03/28/2018 
*/

module clkdiv #(parameter DIV=27)( input clk, output reg clkdiv );

	parameter N = log2(DIV);

   reg [N-1:0] cnt;

   always @(posedge clk) begin
      if (cnt == (DIV-1)) 
         begin
            cnt <= {N{1'b0}};
            clkdiv <= 1'b1;
         end
      else
         begin
            cnt <= cnt + 1;
            clkdiv <= 1'b0;
         end
   end

   initial begin
      cnt <= 0;
      end

	function integer log2;
		input integer value;
		begin
			value = value-1;
			for (log2=0; value>0; log2=log2+1)
				value = value>>1;
		end
	endfunction

endmodule
