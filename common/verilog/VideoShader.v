`timescale 1ns / 1ps

module VideoShader( input [8:0] memS, memA,
										output bold );

	assign bold = (memS == memA);

endmodule
