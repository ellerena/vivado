module ClockDetectOnce (input c, r, i, output reg o);

reg [1:0] cnt = 0;

    always @ (posedge i or posedge r)
        if(r) cnt <= 0;
        else if (i) cnt <= cnt + 1;

	always @ (posedge c)
	   if(r) o <= 0;
	   else if(cnt[1] & cnt[0]) o <= 1;

endmodule

/*
    Clock signal detector.
    i: this is the clock signal to be detected
    o: 1 if i is a living clock.
    When r is active, output is reset.
    When i ticks 2 times it outputs a 1
*/