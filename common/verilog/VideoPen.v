module VideoPen #(parameter RBITS=3, GBITS=3, BBITS=3)
					( input c, vact, shd, cursor, input [2:0] hpix, input [7:0] pen8, shadeColor, txtColor, bgColor,
					output [RBITS-1:0] r, output [GBITS-1:0] g, output[BBITS-1:0] b );
	reg pen;
	
	assign r = {(vact & shd & shadeColor[7]) | (vact & pen & txtColor[7]) | (vact & ~pen & ~shd & bgColor[7]) | cursor,
					(vact & shd & shadeColor[6]) | (vact & pen & txtColor[6]) | (vact & ~pen & ~shd & bgColor[6]) | cursor,
					(vact & shd & shadeColor[5]) | (vact & pen & txtColor[5]) | (vact & ~pen & ~shd & bgColor[5]) | cursor};
	assign g = {(vact & shd & shadeColor[4]) | (vact & pen & txtColor[4]) | (vact & ~pen & ~shd & bgColor[4]) | cursor,
					(vact & shd & shadeColor[3]) | (vact & pen & txtColor[3]) | (vact & ~pen & ~shd & bgColor[3]) | cursor,
					(vact & shd & shadeColor[2]) | (vact & pen & txtColor[2]) | (vact & ~pen & ~shd & bgColor[2]) | cursor};
	assign b = {(vact & shd & shadeColor[1]) | (vact & pen & txtColor[1]) | (vact & ~pen & ~shd & bgColor[1]) | cursor,
					(vact & shd & shadeColor[0]) | (vact & pen & txtColor[0]) | (vact & ~pen & ~shd & bgColor[0]) | cursor};

	always @ (posedge c)
		case(hpix[2:0])
			3'b000:	pen = pen8[7];
			3'b001:	pen = pen8[6];
			3'b010:	pen = pen8[5];
			3'b011:	pen = pen8[4];
			3'b100:	pen = pen8[3];
			3'b101:	pen = pen8[2];
			3'b110:	pen = pen8[1];
			3'b111:	pen = pen8[0];
		endcase
endmodule

/**
Create Date:    16:44:58 08/17/2011 
Module Name:    Dev_001 

	@brief	this circuit is the actual pen that draws the characters
				on the screen.
	@input	vact: this signal signals when we're in the video active time.
				shd: indicates that current character is shaded.
				cursor: indicates that current character has the focus.
				hpix: horizontal pixel counter used to know which pixel in the font are we drawing.
				pen8: 8 bit pattern that indicates which pixels are on/off.
				shadeColor: color used for shading
				txtColor: color used for text (foreground)
				bgColor: color used for background.
	@output	r, g, b: red, green and blue outputs.
*/