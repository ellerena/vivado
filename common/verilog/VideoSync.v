module VideoSync( input c,
						output hs, vs, vact, hend, vend, output [9:0] hpix, vpix);

	parameter A=`VIDEOSYNC_A;
	parameter B=`VIDEOSYNC_B;
	parameter C=`VIDEOSYNC_C;
	parameter D=`VIDEOSYNC_D;
	parameter E=`VIDEOSYNC_E;
	parameter F=`VIDEOSYNC_F;
	parameter G=`VIDEOSYNC_G;
	parameter H=`VIDEOSYNC_H;
	
	wire pre;

	HS_VGA #(A, B, C, D) hs_(c, hs, pre, hend, hpix);
	VS_VGA #(E, F, G, H) vs_(c, hend, pre, vs, vend, vpix);
	VideoActive #(A, E) va_(c, hend, vend, hpix, vpix, vact);

endmodule

// Create Date:    16:44:58 08/17/2011 
// Description: Generador de sincronismo para seniales de video
// hs:	horizontal sync, vs: vertical sync
// vact: video active
// hend: end of horizontal line (tick before new line begins), duration: 1 tick
// vend: end of vertical (before new page begins), duration: 1 horizontal line
//History:
//20-09-2011 217.723MHz
//250.627