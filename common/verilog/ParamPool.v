module ParamPool # (	parameter V=32)
						( input c, input [7:0] dataN, input [(V-1):0] parE,
							output [(V-1)*8+7:0] var);

   genvar x;
   generate
      for (x=0; x < V; x = x+1) 
      begin: variables
         Variable #((x==31) ? 14 : (x==30) ?255:(x==29) ? `PARAMPOOL_H : 0) u_ (c, parE[x], dataN, var[x*8+7:x*8]);
      end
   endgenerate

endmodule

/**

*/
