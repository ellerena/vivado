module Bin2BCD( input c, run, input [7:0] input_bin,
								output done, output [11:0] output_bcd );

	reg [3:0] bcd1, bcd0;
	reg [1:0] bcd2;
	reg [3:0] seq;
	
	assign output_bcd = {{2'b00, bcd2}, bcd1, bcd0};
	assign done = ~seq[3];
	wire e = seq[3];
	wire sum0 = bcd0[3] | (bcd0[2] & (bcd0[0] | bcd0[1]));
	wire sum1 = bcd1[3] | (bcd1[2] & (bcd1[0] | bcd1[1]));
	wire [3:0] bcd0_ = bcd0 + (sum0 ? 2'b11 : 0);
	wire [3:0] bcd1_ = bcd1 + (sum1 ? 2'b11 : 0);

	always @ (posedge c)
		if (run)
			begin
				seq <= 4'b1111;
				bcd0 <= 0;
				bcd1 <= 0;
				bcd2 <= 0;
			end
		else if (e)
			begin
				seq <= seq - 1;
				bcd0 <= {bcd0_[2:0], input_bin[seq[2:0]]};
				bcd1 <= {bcd1_[2:0], bcd0_[3]};
				bcd2 <= {bcd2[0], bcd1_[3]};
			end	

	initial begin
		bcd0 = 4; bcd1 = 2; bcd2 = 3; seq = 12;
	end			
endmodule


/**
	@brief	performs conversion of an input value in binary format (1byte)
				into its bcd equivalent value (3 characters of 4 bits each)
	@input	c: main clock
				run: start pulse (1 clk duration)
				input_bin: input value in binary format <8'bxxxxxxxx>.
	@output	done: signal level meaning conversion completion
				output_bcd: result value in BCD <4'd, 4'd, 4'd>
*/
