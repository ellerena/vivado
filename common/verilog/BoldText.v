module BoldText(	input [3:0] hcol, input [4:0] vrow, addr,
									output bold);
									
	assign bold = (vrow == {addr[3], ~addr[3], addr[2:0]}) & (hcol == {1'b0, addr[4], 2'b11});	

endmodule
