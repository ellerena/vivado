/* Board Interface FLAGS */

`define	CHAN_S7_MSB		7
`define	CHAN_S7_LSB		6
`define	CHAN_LED_MSB	5
`define	CHAN_LED_LSB	4
`define	SEG7OFF			2

`define	BI_LED_CH_0		{2'b00}
`define	BI_LED_CH_1		{2'b01}
`define	BI_LED_CH_2		{2'b10}
`define	BI_LED_CH_3		{2'b11}

/* define 7-segment character codes */
`define	_0_			5'h00
`define	_I_			5'h01
`define	_2_			5'h02
`define	_3_			5'h03
`define	_4_			5'h04
`define	_5_			5'h05
`define	_6_			5'h06
`define	_7_			5'h07
`define	_8_			5'h08
`define	_9_			5'h09
`define	_A_			5'h0A
`define	_b_			5'h0B
`define	_c_			5'h0C
`define	_d_			5'h0D
`define	_E_			5'h0E
`define	_F_			5'h0F
`define	_h_			5'h10
`define	_J_			5'h11
`define	_L_			5'h12
`define	_n_			5'h13
`define	_P_			5'h14
`define	_r_			5'h15
`define	_t_			5'h16
`define	_u_			5'h17
`define	_V_			5'h18
`define	_H_			5'h19
`define	_y_			5'h1A
`define	DASH			5'h1B
`define	_o_			5'h1C
`define	BR_L			5'h1D
`define	BR_R			5'h1E
`define	EQUAL			5'h1F
