module SerialInParallelOut #(parameter W=8) (input c, hold, tic, i, input [3:0] len,
						output datRdy, output [W-1:0] data );
	reg [W:0] p;
	assign data = p[W:1];
	assign datRdy = p[0];
	assign run = tic & ~datRdy;
		
	always @ (posedge c) begin
		if (run) p[W:W-3] <= (hold ? len : {i, p[W:W-2]});

		if (hold) p[W-4:0] <= 0;
			else if (run) p[W-4:0] <= p[W-3:1];
		end
	
	initial p = 9'd0;
endmodule

//module SerialInParallelOut #(	parameter W=8)
//														(	input c, r, run, tic, i, input [2:0] len,
//															output datRdy, output [W-1:0] data );
//	reg [W:0] p;
//	assign data = p[W:1];
//	assign datRdy = p[0];
//	wire read = run & tic & ~datRdy;
//
//	always @ (posedge c) begin
//		if (r | read) p[W:W-2] <= (r ? len : {i, p[W:W-1]});
//		if (r)	p[W-3:0] <= 0;
//			else	if (read) p[W-3:0] <= p[W-2:1];
//	end	
//endmodule

//Design by Edison Llerena
//r: resets data vector to 0 and loads the bit number vector
//s: # of data bits: 100=8 bit, 010=7 bit, 001=6 bit
//15-11-2012 XC3S200-4FT256
//   Minimum period: 4.079ns (Maximum Frequency: 245.158MHz)
//   Minimum input arrival time before clock: 4.248ns
//   Maximum output required time after clock: 7.241ns
//   Maximum combinational path delay: No path found
