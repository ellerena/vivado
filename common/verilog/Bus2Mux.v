module Bus2Mux (	input [4:0] addr, input [255:0] busi,
									output reg [7:0] buso );
										
	always @ *
		case(addr)
			5'h00 : buso = busi[7:0];
			5'h01 : buso = busi[15:8];
			5'h02 : buso = busi[23:16];
			5'h03 : buso = busi[31:24];
			5'h04 : buso = busi[39:32];
			5'h05 : buso = busi[47:40];
			5'h06 : buso = busi[55:48];
			5'h07 : buso = busi[63:56];
			5'h08 : buso = busi[71:64];
			5'h09 : buso = busi[79:72];
			5'h0A : buso = busi[87:80];
			5'h0B : buso = busi[95:88];
			5'h0C : buso = busi[103:96];
			5'h0D : buso = busi[111:104];
			5'h0E : buso = busi[119:112];
			5'h0F : buso = busi[127:120];
			5'h10 : buso = busi[135:128];
			5'h11 : buso = busi[143:136];
			5'h12 : buso = busi[151:144];
			5'h13 : buso = busi[159:152];
			5'h14 : buso = busi[167:160];
			5'h15 : buso = busi[175:168];
			5'h16 : buso = busi[183:176];
			5'h17 : buso = busi[191:184];
			5'h18 : buso = busi[199:192];
			5'h19 : buso = busi[207:200];
			5'h1A : buso = busi[215:208];
			5'h1B : buso = busi[223:216];
			5'h1C : buso = busi[231:224];
			5'h1D : buso = busi[239:232];
			5'h1E : buso = busi[247:240];
			5'h1F : buso = busi[255:248];
	endcase
endmodule
