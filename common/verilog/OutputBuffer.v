`timescale 1ns / 1ps
module OutputBuffer #(parameter W=3)( input c, input [W-1:0] i, output reg [W-1:0] o);

	always @(posedge c) o <= i;

endmodule
