module seg7sel(	input [1:0] seg_sel, input [79:0] i,
								output reg[19:0] o );
								
	always @ *
		case (seg_sel)
			2'b00	:	o = i[19:0];
			2'b01	:	o = i[39:20];
			2'b10	:	o = i[59:40];
			2'b11 : o = i[79:60];
		endcase

endmodule

/**
	@author	Edison Llerena - 27-10-2010
	@brief	combinational circuit that selects and sends a 20bit code output
				out of a 60bit input.
	@input	seg_sel: selection code
				i: a block of 80 signals which are grouped into four 20bit signals
					one of which may be passed to the output by the selector.
	@output	o: 20bit code that may be one of the four 20bit input groups.
*/
