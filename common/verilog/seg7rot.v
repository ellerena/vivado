module seg7rot2#(	parameter A=2) 
								(	input c, e, go, input[(2**A)-1:0] d, input[5*(2**A)-1:0] i,
									output reg[3:0] dp, output reg[19:0] o);
	reg ign, fin, newp;
	reg [4:0] newd;
	reg[(A-1):0] k;
	assign e1 = e & ign;
	
	always @(posedge c) begin
		if (go) k<= 0;
			else if (e1) k <= k+1;
		if (e1) o <= {newd, o[19:5]};
		if (go) dp <= 4'b1000;
			else if (e1) dp <= {newp, dp[3:1]};
		if (fin) fin <= 0;
			else if (e1) fin <= dp[0];
		if (fin) ign <= 0;
			else if (go) ign <= 1'b1;

	end	

	always @ * begin
			case (k)
				2'b00	:	newd <= i[4:0];
				2'b01	:	newd <= i[9:5];
				2'b10	:	newd <= i[14:10];
				2'b11	:	newd <= i[19:15];
			endcase
			case (k)
				2'b00	:	newp <= d[0];
				2'b01	:	newp <= d[1];
				2'b10	:	newp <= d[2];
				2'b11	:	newp <= d[3];
			endcase
	end
	initial begin
		dp <= 0; o <= 0; k <= 0;
		end
endmodule

//	Minimum period: 3.610ns (Maximum Frequency: 277.037MHz)
//	Minimum input arrival time before clock: 4.333ns
//	Maximum output required time after clock: 4.310ns


//module seg7rot2 #(	parameter A=3) 
//								(	input c, e, go, input[1:0] cnt, d, input[9:0] i,
//									output reg[3:0] dp, output reg[19:0] o)
//	reg[A-1:0] k;
//	Ignitor ign (	c,	fin, go, oP, oL );
//	Level2Pulse #(0) l2p (c, k[A-1], fin);
//	assign e1 = e & cnt[1] & cnt[0] & oL;
//	assign e2 = oP | fin;
//	wire[4:0] newd = fin ? i[9:5] : i[4:0];
//	wire newp = fin ? d[1]:d[0];
//	
//	always @(posedge c) begin
//		if (e1) k <= k+1;
//		if (e2) o <= {newd, o[19:5]};
//		if (e2) dp = {newp, dp[3:1]};
//	end	
//	
//	initial begin
//		dp <= 0; o <= 0; k <= 0;
//		end
//endmodule
//
////	XX3S500E-4FG320
////	Minimum period: 3.920ns (Maximum Frequency: 255.102MHz)
////	Minimum input arrival time before clock: 3.603ns
////	Maximum output required time after clock: 4.310ns