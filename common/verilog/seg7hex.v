module seg7hex ( input [4:0] seg7_5bit_i, output reg [6:0] seg7_o);
	always @(*)
		case(seg7_5bit_i)
			5'h00: seg7_o = `S7_0;
			5'h01: seg7_o = `S7_I;
			5'h02: seg7_o = `S7_2;
			5'h03: seg7_o = `S7_3;
			5'h04: seg7_o = `S7_4;
			5'h05: seg7_o = `S7_5;
			5'h06: seg7_o = `S7_6;
			5'h07: seg7_o = `S7_7;
			5'h08: seg7_o = `S7_8;
			5'h09: seg7_o = `S7_9;
			5'h0A: seg7_o = `S7_A;
			5'h0B: seg7_o = `S7_b;
			5'h0C: seg7_o = `S7_c;
			5'h0D: seg7_o = `S7_d;
			5'h0E: seg7_o = `S7_E;
			5'h0F: seg7_o = `S7_F;
			5'h10: seg7_o = `S7_h;
			5'h11: seg7_o = `S7_J;
			5'h12: seg7_o = `S7_L;
			5'h13: seg7_o = `S7_n;
			5'h14: seg7_o = `S7_P;
			5'h15: seg7_o = `S7_r;
			5'h16: seg7_o = `S7_t;
			5'h17: seg7_o = `S7_u;
			5'h18: seg7_o = `S7_V;
			5'h19: seg7_o = `S7_H;
			5'h1A: seg7_o = `S7_y;
			5'h1B: seg7_o = `S7DASH;
			5'h1C: seg7_o = `S7_o;
			5'h1D: seg7_o = `S7BR_L;
			5'h1E: seg7_o = `S7BR_R;
			5'h1F: seg7_o = `S7EQUAL;
		endcase
endmodule

/**
	@author	Edison Llerena - 27-10-2010
	@brief	7 segments driver. Converts a 5bit input code into a
				7 segment code suitable for display.
	@input	seg7_5bit_i: 5 bit code
	@output	seg7_o: 7 segment output as --> (MSB) a, b, c, d, e, f, g (LSB)
*/
