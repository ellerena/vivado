
//`include "..\common\verilog\BoardIF.h"
//`include "..\common\verilog\DisplayTextIF.h"
//`include "..\common\verilog\ft232h\ft232fifo.h"

`define SPARTAN_3_DCM
`define CLOCK_INPUT_PER_NS		(20.0)
`define CLOCK_DIVIDE_BY			(2)		/* 2..16 */
`define CLOCK_FX_MULTIPLY		(2)		/* 1..32 */
`define CLOCK_FX_DIVIDE_BY		(32)		/* 2..32  CLKFX = CLKIN * CLOCK_FX_MULTIPLY / CLOCK_FX_DIVIDE_BY */



