`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Eddie Llerena
// Create Date:    16:04:07 03/20/2018 
// Design Name: 
// Module Name:    pb_base 
// Revision: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////
module pb_base(input clk, pb0, rxd, brtxsma, input [3:0] j2, j3,
               input[31:0] brtxen,
               output fla_we_b, fla_oe_b, fla_ce_b,
               output reg [6:0] led, output txd);

// KCPSM3 interface
   wire [9:0] address;
   wire [17:0] instruction;
   wire [7:0] port_id;
   wire [7:0] out_port;
   reg [7:0] in_port;
   wire write_strobe, read_strobe, interrupt_ack;
   reg interrupt;

// Instruction ROM ******************************************
   instr _prg (address, instruction, clk);

// UART module **********************************************
   reg [9:0] baud_count;
   reg en_16_x_baud;
   wire write_to_uart = write_strobe & port_id[0];  // port_id[0] = txd enable
   wire tx_full, tx_half_full;
   reg read_from_uart;
   wire [7:0] rx_data;
   wire rx_data_present, rx_full, rx_half_full;

   clkdiv #(27) _clkdv(clk, clkdv);   // Generate clkdv = 16x115200 (for 115200 @ 50MHz clk) 

	kcpsm3 _cpu(address, instruction, port_id, write_strobe,
            out_port, read_strobe, in_port, 1'b0 /*interrupt*/,
            interrupt_ack, 1'b0 /*reset*/, clk) ;

   uart_tx _txd(
      .data_in(out_port),
    	.write_buffer(write_to_uart),
    	.reset_buffer(1'b0),
    	.en_16_x_baud(clkdv),
    	.serial_out(txd),
    	.buffer_full(tx_full),
    	.buffer_half_full(tx_half_full),
    	.clk(clk));

   uart_rx _rxd(
      .serial_in(rxd),
      .data_out(rx_data),
      .read_buffer(read_from_uart),
      .reset_buffer(1'b0),
      .en_16_x_baud(clkdv),
      .buffer_data_present(rx_data_present),
      .buffer_full(rx_full),
      .buffer_half_full(rx_half_full),
      .clk(clk));

// BigRed TX Enable Verifier module
   wire [7:0] pmo, pmk;
   wire brtxsma_ = (brtxsma | (port_id[4] & write_strobe));
   reg pmrst;
   reg [7:0] pma;

   pmodCapture8 _pmo8( .c(clk), .r(pmrst), .capture(brtxsma_),
                     .pm(brtxen), .addR(pma),
                     .cnt(pmk), .o(pmo));

// Flash module ********************************************
   assign {fla_we_b, fla_oe_b, fla_ce_b} = 3'h7;

// Output port *********************************************

   always @ (posedge clk)
      begin
         read_from_uart <= read_strobe & port_id[0];           // port_id[0] = rxd request
         if (write_strobe & port_id[1]) led <= out_port[6:0];  // port_id[1] = led value
         if (write_strobe & port_id[2]) pma <= out_port;       // pmo data port (read & reset)
         if (write_strobe & port_id[3]) pmrst <= out_port[0];  // port_id[3] = br tx  counter
      end

// Input Port Decoder **************************************
   always @ *
      case(port_id[3:0])
         4'd0:                        /* read machine status */
               in_port = {2'b0, rx_data_present, rx_half_full,
			rx_full, tx_half_full, tx_full, pb0};
         4'd1:                        /* read UART RX data */
               in_port = rx_data;
         4'd2:                        /* read {j3, j2} data */
               in_port = {j3, j2};
         4'd3:
               in_port = {j3, j2};
         4'd4:
               in_port = pmo;
         4'd8:
               in_port = pmk;
         default:
               in_port = pmo;               
      endcase

      
   initial begin
      led <= 7'b1000001;
      end

endmodule
