`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:02:39 05/18/2018 
// Design Name: 
// Module Name:    pmodCapture8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module pmodCapture8( input c, r, capture,
                     input [31:0] pm, input [7:0] addR,
                     output [7:0] cnt, output reg [7:0] o );
    wire w;
    reg cap_sync;
    wire [31:0] o_;
    wire [7:2] cnt_;
    
    assign cnt = {2'd0, cnt_};

    always @ *
        case (addR[1:0])
            2'd0: o <= o_[7:0];
            2'd1: o <= o_[15:8];
            2'd2: o <= o_[23:16];
            2'd3: o <= o_[31:24];
        endcase

    always @ (posedge c)
        cap_sync <= capture;

    Level2Pulse #(1) _l2p(c, cap_sync, w);      // domain cross sync
    pmodCapture #(64, 6) _pmc(c, r, w, pm, addR[7:2], cnt_, o_ );

endmodule

