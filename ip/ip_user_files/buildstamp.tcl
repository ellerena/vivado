set fecha [pwd]
set mydate [clock seconds]
puts $fecha/buildstamp.vh
set fp [open $fecha/buildstamp.vh w]
puts $fp "`define DATESTAMP [clock format $mydate -format %m%d%Y]"
puts $fp "`define TIMESTAMP [clock format $mydate -format %H%M]"
close $fp
