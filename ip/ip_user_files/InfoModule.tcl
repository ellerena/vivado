puts "---> Updating Project InfoModule"
cd [get_property DIRECTORY [current_project]]

set mydate [clock seconds]
#set git_hash [exec git log -1 --pretty=0x%h]
set ob [get_ips -filter {NAME =~*InfoModule*}]

set_property CONFIG.DATESTAMP 0x[clock format $mydate -format %m%d%Y] $ob
set_property CONFIG.TIMESTAMP 0x[clock format $mydate -format %H%M] $ob
#set_property CONFIG.HASHSTAMP $git_hash $ob

puts "build [get_property CONFIG.HASHSTAMP $ob]: [get_property CONFIG.DATESTAMP $ob] [get_property CONFIG.TIMESTAMP $ob]"
