#execute from same location as main script file.

set objset [get_files *bd]
foreach obj $objset {
open_bd_design $obj
write_bd_tcl -force "./src/bd/[current_bd_design].tcl"
close_bd_design [current_bd_design] 
exec cp "$obj" "./src/bd/."
}

write_project_tcl -no_copy_sources -use_bd_files -force -target_proj_dir "workdir" {./main.tcl}

puts "Re-construction files done"
