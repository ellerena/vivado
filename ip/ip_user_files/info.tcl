puts "---> Updating Project InfoModule"

set cktick [clock seconds]
set mydate [clock format $cktick -format %m%d%Y]
set mytime [clock format $cktick -format %H%M]
set git_hash [exec git log -1 --pretty=%h]

if {[info exists argv]} {
  puts "Writing to [lindex $argv 0]"
} else {
  puts "argv (file name) not defined, using ./hdl/info.v"
  set argv ../hdl/info.v
}

set outf [open [lindex $argv 0] w]
puts $outf "// Global definition file"
puts $outf "`define B_DATE 32'h$mydate"
puts $outf "`define B_TIME 32'h$mytime"
puts $outf "`define B_COMMIT 32'h$git_hash"
close $outf

puts "File [file normalize $argv] saved."
