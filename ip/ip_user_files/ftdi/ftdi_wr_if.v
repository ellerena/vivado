`timescale 1ns / 1ps

module ftdi_wr_if #( parameter FIFO_WIDTH = 8, FIFO_DEPTH = 2048) (
	input cin, cout,
	input rst, read,
	input rxf_n, txe_n,
	input [FIFO_WIDTH-1:0] ftin,
	output wr_n, rd_n, oe_n,
	output ff_empty, ff_full,
	output [FIFO_WIDTH-1:0] ff_out,
	output dir, oe1, oe2, oe3,
	output [15:0] wptr, rptr
	);

assign 	dir = 1'b0, oe1 = 1'b0, oe2 = 1'b0, oe3 = 1'b0;

fifo_sync #( FIFO_WIDTH, FIFO_DEPTH ) ff_1(
	.cin(cin), .cout(cout),
	.rst(rst), .read(read),
	.wr_n(rd_n),
	.ff_in(ftin),
	.ff_out(ff_out),
	.ff_empty(ff_empty), .ff_full(ff_full),
	.wptr(wptr), .rptr(rptr)
	);

ftdi_flow_ctrl ctrl_1(
	.clk(cin),
	.mem_full(ff_full),
	.rxf_n(rxf_n), .txe_n(txe_n),
	.oe_n(oe_n), .rd_n(rd_n), .wr_n(wr_n)
	);

endmodule

