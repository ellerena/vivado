`timescale 1ns / 1ps

module fifo_sync #( parameter FIFO_WIDTH = 8, FIFO_DEPTH = 2048) (
		input cin, cout, rst, read, wr_n, input [FIFO_WIDTH-1:0] ff_in,
		output [FIFO_WIDTH-1:0] ff_out, output ff_empty, ff_full,
		output [15:0] wptr, rptr
		);

reg [3:0] wr0, rd0;
reg [7:0] wr1, rd1;
wire[11:0] rd_ptr = {rd1, rd0}, wr_ptr = {wr1, wr0};
 
reg[FIFO_WIDTH-1:0] ff_mem[FIFO_DEPTH-1 : 0];

assign ff_out = ff_mem[rd_ptr];
assign wptr = {4'b0, wr1, wr0};
assign rptr = {4'b0, rd1, rd0};
assign ff_empty = (rd_ptr == wr_ptr);					/* signals fifo is empty (async) */
assign ff_full = wr_ptr[11];							/* signals fifo is full (async) */

always @(posedge cin)									/* writes data into the fifo memory buffer */
	ff_mem[wr_ptr] <= wr_n ? ff_mem[ wr_ptr ] : ff_in;

always @ (posedge cin or posedge rst)					/* pointers to next write address */
begin
	wr0 <= rst ? 0 : wr0 + !wr_n;

	if(rst) wr1 <= 0;
	else if (wr0 == 4'hf) wr1 <= wr1 + !wr_n;	
end

always @ (posedge cout or posedge rst)					/* pointer to next read address */
begin
	rd0 <= rst ? 0 : rd0 + read;

	if(rst) rd1 <= 0;
	else if (rd0 == 4'hf) rd1 <= rd1 + read;	
end

endmodule
