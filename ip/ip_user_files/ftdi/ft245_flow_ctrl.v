`timescale 1ns / 1ps

module ftdi_flow_ctrl(
		input clk,
		input mem_full,
		input rxf_n, txe_n,
		output reg oe_n, rd_n,
		output wire wr_n
		);

wire read_halt = (mem_full | rxf_n);

assign wr_n = 1'b1;			/* TODO: implement signal wr_n for writing to FTDI */

always @ (posedge clk or posedge read_halt)		/* Controls reading data from FTDI */
	begin
		oe_n <= read_halt ? 1'b1 : 1'b0;		/* generate signal OE# */
		rd_n <= read_halt ? 1'b1 : oe_n;		/* generate signal RD# */
	end
	
endmodule

