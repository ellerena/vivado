`timescale 1ns / 1ps
/*
	Engineer: Eddie Llerena
	Create Date:    21:04:54 01/18/2019 
	Description: 

	Dependencies: 

	Revision: 
*/

module clamper ( input c, trg, input [7:0] lim, output sma, cla );

reg [7:0] cnt;
reg ams, alc, oP;
wire fin = (8'hff == cnt);
wire cls = (lim == cnt);

assign sma = ams, cla = alc;

always @ (posedge c) begin
	ams <= fin ? 1'b0 : oP ? oP : ams;
	alc <= fin ? 1'b0 : cls ? 1'b1 : alc;
	cnt <= sma ? cnt + 1'b1 : cnt;
	end

always @ (posedge c or posedge sma) begin
	oP <= sma ? 1'b0 : trg;
	end

initial begin
	cnt = 8'b0; {ams, alc, oP} = 3'h0;
	end

endmodule
