`timescale 1ns / 1ps
/*
// Engineer: Eddie Llerena   Create Date: 11/21/2018 01:22:13 PM
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
*/

module TimeInterval(input c, r, start, stop, output tirun, output [15:0] interval);

   wire [11:0] ntrvl;

   assign tirun = (start & ~stop);
   assign interval = {4'b0, ntrvl};

   fast_counter _fc0(c, r, tirun, ntrvl);

endmodule
