
`timescale 1 ns / 1 ps

	module wire1_rx_v1_0 #
	(
		// Users to add parameters here
      parameter MEASUREMENT_TRESH    = 32'h0E100014,
      parameter W1_CLK_DIVIDE_BY     = 125,

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
      input wire w1_fclk,   /* 1-wire fast clock, will be gated to 200kHz for DHT11 */
      input wire w1_di,     /* 1-wire data signal input (part of bidirectional) */
      output wire w1_do,    /* 1-wire data signal output (part of bidirectional) */
      output wire w1_T,     /* 1-wire data output enable (part of bidirectional) */
      output wire w1_dr,    /* 1-wire data ready, read complete! (may use as irq) */
      output wire w1_error, /* 1-wire timer expired & sensor fail (use as irq) */      
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
// Instantiation of Axi Bus Interface S00_AXI
	wire1_rx_v1_0_S00_AXI # ( 
      .MEASUREMENT_TRESH(MEASUREMENT_TRESH),
      .W1_CLK_DIVIDE_BY(W1_CLK_DIVIDE_BY),
      .C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) wire1_rx_v1_0_S00_AXI_inst (
      .w1_fclk(w1_fclk),   /* 1-wire clock, expected 200kHz for DHT11 */
      .w1_di(w1_di),       /* 1-wire data signal input (part of bidirectional) */
      .w1_do(w1_do),       /* 1-wire data signal output (part of bidirectional) */
      .w1_T(w1_T),         /* 1-wire data output enable (part of bidirectional) */
      .w1_dr(w1_dr),       /* 1-wire 'data ready' output signal (may use as irq) */
      .w1_error(w1_error), /* 1-wire timer expired & sensor fail (use as irq) */
      .S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
