//////////////////////////////////////////////////////////////////////////////////
// Engineer:  Eddie Llerena
// 
// Create Date:    19:31:00 07/12/2013 
// Module Name:    wire1_rx
// Description:   1-wire master receiver interface
//    designed for the DHT11 temperature and humidity sensor
//    this sensor uses pulse length modulation so it requires
//    careful design considerations.
//    To obtain reasonable discrimination, a 200kHz clock is
//    expected, hence signal delays, particularly from combinatorial
//    logic, are not an issue.
//
//    1 Wire format: 1...<preamb 0><preamb 1><data:40>
//////////////////////////////////////////////////////////////////////////////////
module wire1_rx #(parameter BITS=40) 
      ( input c, start, i, input [31:0] tresh, output [39:0] o, output awake, error, done);

/**
   Main registers defined here
*/
reg [11:0] k;                      /* main counter, used to measure pulse lengths */
reg [(BITS + 2):0] data;           /* data shifter: <done:1, x:1, awake:1, data :40> */

/*
   Wire signals defined here
*/
wire [11:0] tresTO = tresh[28:16];  /* treshold value to detect time out (sensor hang) */
wire [11:0] tresBit = tresh[7:0];   /* treshold value to determine bit 1 or 0 */
wire timOut = (k > tresTO);         /* time out signal, high when counter exceeds tresTO */
wire bitVal = (k > tresBit);        /* bit value signal 1 or 0 */
wire inActive = (kEna & i);         /* input signal, valid only when module is running */
wire start_tick;                    /* pulse to start the FSM */
wire kRes;                          /* k counter reset request */
wire kEna = (start & (~done) & (~error)); /* k counter enable signal */

/* these modules generate a 1 clk tick when the input togles high */
Level2Pulse _l2p0 (c, start, start_tick);      /* pulse to start the FSM */
Level2Pulse #(0) _l2p2 (c, inActive, kRes);    /* pulse when input toggles low */

rise_edge _re0 (c, start_tick, timOut, error); /* generates <error> if k exceeds tresTO */

/* assigns used to export internal
   signals to the module outputs */
assign done = data[BITS + 2];
assign awake = data[BITS];
assign o = data[(BITS-1):0];

/* define registers and counters */
always @(posedge c) begin
   if (kRes)         /* k counter, used to measure time intervals */
      k <= 12'h0;
   else if (kEna)
      k <= k + 1'b1;

   if (start_tick)   /* data: shift register to capture the sensor data */
      data <= {{(BITS+2){1'b0}}, 1'b1};
   else if (kRes)
      data <= {data[BITS+1:0], bitVal};

end

initial begin
   k <= 12'h0;
   end
endmodule

/////////////////////////////////////////////////////////////////////////////////////
//`define __TST_WIRE1_RX__
`ifdef __TST_WIRE1_RX__
`timescale 1us / 1ns

module T_module;
   localparam U25 = 25, U50 = 50, U75 = 75, U80 = 80, U100 = 100;
 
   /* input signals */
   reg c, start, i;

   /* output signals */
   wire [39:0] o;
   wire awake, error, done;

   /* uut */
   wire1_rx _w1rx ( c, start, i, 32'h0E100006, o, awake, error, done);

/********************* Initial conditions *********************/
   always #2.5 c = ~c;           /* generate main clk */

	initial {c, start, i} = 3'b001;

/********************* Stimulus Signals ***********************/
   initial begin
   #U25  assign start = 1;
   #U75  assign i = 0;
   #U80  assign i = 1;
   #U80  assign i = 0;
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */ /* ab54daad56 -> 6AB55B2AD5*/
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U25  assign i = 0;   /* 0 */   
   #U50  assign i = 1;  #U75  assign i = 0;   /* 1 */
   #U50  assign i = 1;  #U75  assign i = 0;
   end

endmodule
`endif

/*
Timing Summary:
---------------
Speed Grade: -2

   Minimum period: 3.580ns (Maximum Frequency: 279.326MHz)
   Minimum input arrival time before clock: 4.637ns
   Maximum output required time after clock: 4.346ns
   Maximum combinational path delay: No path found
*/

