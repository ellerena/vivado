`timescale 1ns / 1ps
/*
  Engineer: Eddie Llerena
  Create Date: 08/06/2020 07:09:40 PM
  Module Name: ft245fsm
  Revision:
  Revision 0.01 - File Created
  Additional Comments:
*/

module ft245fsm(
    /* ftdi interface */
    input clk, rst, rxne_n /* receiver has data */, txnf_n /* transmitter not full */,
    output reg oe_n, rd_n, wr_n,
    /* rx fifo */
    input full,
    output ffwr,
    /* tx fifo */
    input empty,
    output ffrd
    );

wire rdst = (~rxne_n) & (~full);      /* condition for RD state */
wire wrst = (~txnf_n) & (~empty);     /* condition for WR state */
reg ffwr_pre;
assign ffwr = ffwr_pre & & (~rxne_n);
assign ffrd = ~wr_n;
                                      /*** read state, from FTDI to fifo ***/
always @ (posedge clk) begin
  if (rdst) begin                                /* read process */
    {oe_n, rd_n, wr_n} <= {1'b0, oe_n, 1'b1};    /* ftdi side: read out */
    {ffwr_pre} <= {~oe_n};                       /* fifo side: write in */
  end
  else if (wrst) begin                           /* write process */
    {oe_n, rd_n, wr_n} <= {1'b1, 1'b1, 1'b0};    /* ftdi side: write out */
    {ffwr_pre} <= {1'b0};                        /* fifo side: read in */
  end
  else begin                                     /* idle process */
    {oe_n, rd_n, wr_n} <= {1'b1, 1'b1, 1'b1};    /* ftdi side: idle */
    {ffwr_pre} <= {1'b0};                        /* fifo side: idle */
  end
end
    
endmodule
