`timescale 1ns / 1ps
/*
  Engineer: Eddie Llerena
  Create Date: 08/06/2020 07:09:40 PM
  Module Name: ft245fsm
  Revision:
  Revision 0.01 - File Created
  Additional Comments:
*/

module ft245Axi(
    /* Axi side */
    input clkaxi, rstaxi, axRxrden, axTxwren,
    output axRxEmpty, axTxFull,
    input [7:0] ffTxdat,
    output [7:0] ffRxdat,
    /* Ftdi side */
    input clkft, rxf_n /* receiver has data */, txe_n /* transmitter not full */,
    input [7:0] din,
    output oe_n, rd_n, wr_n,
    output [7:0] dout,
    output d2, d1
    );

  wire ffRxwren, ffRxFull, ffTxEmpty, ffTxrden;
  assign d2 = ffTxEmpty, d1 = ffTxrden;

  ft245fsm ft245_(
    /* ftdi interface */
    .clk(clkft), .rst(rstaxi), .rxne_n(rxf_n), .txnf_n(txe_n),
    .oe_n(oe_n), .rd_n(rd_n), .wr_n(wr_n),
    /* rx fifo */
    .full(ffRxFull),
    .ffwr(ffRxwren),
    /* tx fifo */
    .empty(ffTxEmpty),
    .ffrd(ffTxrden)
    );

    /* RX fifo */
  fifo_generator_0 ffrx(
  /* ftdi fsm side */
  .wr_clk(clkft),
  .wr_rst(rstaxi),
  .din(din),
  .wr_en(ffRxwren),
  .full(ffRxFull),
  /* AXI side */
  .rd_clk(clkaxi),
  .rd_rst(rstaxi),
  .rd_en(axRxrden),
  .dout(ffRxdat),
  .empty(axRxEmpty)
  );

    /* TX fifo */
  fifo_generator_0 fftx(
  /* ftdi fsm side */
  .rd_clk(clkaxi),
  .rd_rst(rstaxi),
  .rd_en(ffTxrden),
  .dout(dout),
  .empty(ffTxEmpty),
  /* AXI side */
  .wr_clk(clkft),
  .wr_rst(rstaxi),
  .din(ffTxdat),
  .wr_en(axTxwren),
  .full(axTxFull)
  );


endmodule
