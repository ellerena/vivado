`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: Eddie Llerena
// Create Date:    17:22:40 06/30/2019 
// Timing Summary:
// ---------------
// Speed Grade: -2
//
//   Minimum period: 3.374ns (Maximum Frequency: 296.384MHz)
//   Minimum input arrival time before clock: 4.079ns
//   Maximum output required time after clock: 5.662ns
//   Maximum combinational path delay: No path found
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module snif_spi( input c, r, spi_cs, spi_do, spi_di, spi_clk, rd,
                 output rdy, output [31:0] o);

   reg dc;
   reg [6:0] mo, mi;
   reg [7:0] tr = 8'h1;
   reg [14:0] fr;
   reg [31:0] o_;
   
   wire [31:0] o_w;

   assign o = {o_w[31:16], emp, o_w[14:0]};
   assign rdy = ~emp;

   always @(negedge spi_clk) begin              /* MOSI & MISO capture */
      mo  <= !spi_cs ? {mo[5:0], spi_do} : mo;  /* capture MOSI 1st 7 bits */
      mi  <= !spi_cs ? {mi[5:0], spi_di} : mi;  /* capture MISO 1st 7 bits */
      o_ <= tr[7] ? {mo, spi_do, mi, spi_di, emp, fr} : o_;
   end

   always @(negedge spi_clk or posedge spi_cs) begin
      tr <= spi_cs ? 8'h1 : {tr[6:0], tr[7]};   /* rotating counter */
      dc <= spi_cs ? 1'b0 : tr[7];              /* data captured */
   end

   always @(posedge c)                          /* frame counter */
      if (r)
         fr <= 15'h0;
      else if (fr_t)
         fr <= fr + 1'b1;

   Level2Pulse #(1, 1) _l2p0(c, spi_cs, fr_t);     /* frame counter trigger */
   Level2Pulse #(1, 1) _l2p1(c, dc, wr);           /* fifo write trigger */
   fifo_1clk #(32, 3) _ff0(c, r, rd, wr, o_, emp, /*full*/, o_w);

   initial begin
      fr = 0; mo = 0; mi = 0; o_ = 0; dc = 0;
   end

endmodule

/////////////////////////////////////////////////////////////////////////////////////
//`define __SNIF_SPI_TST__
`ifdef __SNIF_SPI_TST__
`define CKPER_NS		(1e9/50e6)   /* 20ns period */
`define CH_OFFSET    (1)          /* small offset to simulate rise/fall delay */

module T_module;
	reg [15:0] _clk;
   reg clk_;
   reg cson = 0;

   reg c, r, cs, mo, mi, rd;
	wire irq, clk, ne;
   wire [31:0] o;

   assign clk = cson ? clk_ : 1'b0;
   snif_spi _u0(c, r, cs, mo, mi, clk, rd, irq, ne, o);

/********************* Initial conditions *********************/
	initial begin
		{c, r, cs, mo, mi, rd} = {6'b001000};
      _clk = 0;
      clk_ = 0;
      fork
         forever #(`CKPER_NS/2) c = ~c;
         forever #(5*`CKPER_NS/2) clk_ = ~clk_;
         #`CH_OFFSET forever _clk= #(`CKPER_NS) ((_clk + 1));
         #(2*(`CKPER_NS)+`CH_OFFSET) forever #(8*(`CKPER_NS)) mo = $random;
         #(2*(`CKPER_NS)+`CH_OFFSET) forever #(8*(`CKPER_NS)) mi = $random;
      join
	end

/********************* Stimulus Signals ***********************/
	always @ (_clk)
	begin
		case (_clk)
           2: r = 1;
           3: r = 0;
           4: cs = 0;
          10: cs = 1;
          20: cs = 0;
          25: cson = 1'b1;
         419: rd = 1;
         424: rd = 0;
         525: cson = 1'b0;
         530: cs = 1;
         550: cs = 0;
         555: cson = 1'b1;
         845: cson = 1'b0;
         850: cs = 1;
		endcase      
	end		
endmodule
`endif
