`timescale 1ns / 1ps
`define CKPER_NS		(1e9/50e6)   /* 20ns period */
`define CH_OFFSET    (1)          /* small offset to simulate rise/fall delay */

module T_module;
	reg [15:0] _clk;
   reg clk_;
   reg cson = 0;

   reg c, r, cs, mo, mi;
	wire irq, clk;
   wire [31:0] o;

   assign clk = cson ? clk_ : 1'b0;
   snif_spi _u0(c, r, cs, mo, mi, clk, irq, o);

/********************* Initial conditions *********************/
	initial begin
		{c, r, cs, mo, mi} = {5'b00100};
      _clk = 0;
      clk_ = 0;
      fork
         forever #(`CKPER_NS/2) c = ~c;
         forever #(5*`CKPER_NS/2) clk_ = ~clk_;
         #`CH_OFFSET forever _clk= #(`CKPER_NS) ((_clk + 1));
         #(2*(`CKPER_NS)+`CH_OFFSET) forever #(8*(`CKPER_NS)) mo = $random;
         #(2*(`CKPER_NS)+`CH_OFFSET) forever #(8*(`CKPER_NS)) mi = $random;
      join
	end

/********************* Stimulus Signals ***********************/
	always @ (_clk)
	begin
		case (_clk)
           2: r = 1;
           3: r = 0;
           4: cs = 0;
          10: cs = 1;
          20: cs = 0;
          25: cson <= 1'b1;
         625: cson <= 1'b0;
         630: cs = 1;
		endcase      
	end		
endmodule
