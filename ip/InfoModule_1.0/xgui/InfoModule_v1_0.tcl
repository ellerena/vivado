# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  set C_S00_AXI_DATA_WIDTH [ipgui::add_param $IPINST -name "C_S00_AXI_DATA_WIDTH" -parent ${Page_0} -widget comboBox]
  set_property tooltip {Width of S_AXI data bus} ${C_S00_AXI_DATA_WIDTH}
  set C_S00_AXI_ADDR_WIDTH [ipgui::add_param $IPINST -name "C_S00_AXI_ADDR_WIDTH" -parent ${Page_0}]
  set_property tooltip {Width of S_AXI address bus} ${C_S00_AXI_ADDR_WIDTH}
  ipgui::add_param $IPINST -name "C_S00_AXI_BASEADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S00_AXI_HIGHADDR" -parent ${Page_0}

  ipgui::add_param $IPINST -name "VERSIONSTAMP"
  ipgui::add_param $IPINST -name "DATESTAMP"
  ipgui::add_param $IPINST -name "TIMESTAMP"
  ipgui::add_param $IPINST -name "HASHSTAMP"

}

proc update_PARAM_VALUE.DATESTAMP { PARAM_VALUE.DATESTAMP } {
	# Procedure called to update DATESTAMP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DATESTAMP { PARAM_VALUE.DATESTAMP } {
	# Procedure called to validate DATESTAMP
	return true
}

proc update_PARAM_VALUE.HASHSTAMP { PARAM_VALUE.HASHSTAMP } {
	# Procedure called to update HASHSTAMP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HASHSTAMP { PARAM_VALUE.HASHSTAMP } {
	# Procedure called to validate HASHSTAMP
	return true
}

proc update_PARAM_VALUE.TIMESTAMP { PARAM_VALUE.TIMESTAMP } {
	# Procedure called to update TIMESTAMP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TIMESTAMP { PARAM_VALUE.TIMESTAMP } {
	# Procedure called to validate TIMESTAMP
	return true
}

proc update_PARAM_VALUE.VERSIONSTAMP { PARAM_VALUE.VERSIONSTAMP } {
	# Procedure called to update VERSIONSTAMP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VERSIONSTAMP { PARAM_VALUE.VERSIONSTAMP } {
	# Procedure called to validate VERSIONSTAMP
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to update C_S00_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to validate C_S00_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to update C_S00_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_S00_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_BASEADDR { PARAM_VALUE.C_S00_AXI_BASEADDR } {
	# Procedure called to update C_S00_AXI_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_BASEADDR { PARAM_VALUE.C_S00_AXI_BASEADDR } {
	# Procedure called to validate C_S00_AXI_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_HIGHADDR { PARAM_VALUE.C_S00_AXI_HIGHADDR } {
	# Procedure called to update C_S00_AXI_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_HIGHADDR { PARAM_VALUE.C_S00_AXI_HIGHADDR } {
	# Procedure called to validate C_S00_AXI_HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.DATESTAMP { MODELPARAM_VALUE.DATESTAMP PARAM_VALUE.DATESTAMP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DATESTAMP}] ${MODELPARAM_VALUE.DATESTAMP}
}

proc update_MODELPARAM_VALUE.TIMESTAMP { MODELPARAM_VALUE.TIMESTAMP PARAM_VALUE.TIMESTAMP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TIMESTAMP}] ${MODELPARAM_VALUE.TIMESTAMP}
}

proc update_MODELPARAM_VALUE.VERSIONSTAMP { MODELPARAM_VALUE.VERSIONSTAMP PARAM_VALUE.VERSIONSTAMP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VERSIONSTAMP}] ${MODELPARAM_VALUE.VERSIONSTAMP}
}

proc update_MODELPARAM_VALUE.HASHSTAMP { MODELPARAM_VALUE.HASHSTAMP PARAM_VALUE.HASHSTAMP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HASHSTAMP}] ${MODELPARAM_VALUE.HASHSTAMP}
}

