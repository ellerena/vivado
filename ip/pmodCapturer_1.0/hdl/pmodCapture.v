`timescale 1ns / 1ps
/*
// Company: 
// Engineer: Edison Llerena
// 
// Create Date: 04/10/2018 12:28:53 PM
// Design Name: 
// Module Name: pmodCapture
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: Simple capture module, when enabled (e) it registers all 4
//                pmod inputs into a single 32 bit output(o)
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
*/

module pmodCapture #(parameter D=256, W=8, N=1)
							(input c, r, w,
                      input [(N*32 -1):0] pm, input [W-1:0] addR,
                      output [W-1:0] cnt, output [(N*32 -1):0] o );

	(* RAM_STYLE="{AUTO | BLOCK |  BLOCK_POWER1 | BLOCK_POWER2}" *)

genvar x;
generate
	for (x=0; x < N; x = x+1) 
		begin: inst
			wire [31:0] WIRE_x = pm[(31+32*x):(32*x)];
			reg [31:0] RAM_x[D-1:0] /* synthesis syn_ramstyle="block_ram" */; 
			reg [31:0] o_x;
			assign o[(31+32*x):(32*x)] = o_x;

			always @ (posedge c)
				if (w) RAM_x[cnt] <= WIRE_x;

			always @ (posedge c)
				o_x <= RAM_x[addR];

         initial o_x <= 0;
	  end
endgenerate

   reg [W-1:0] cnt_;
   assign cnt = cnt_;

   always @ (posedge c)
      cnt_ <= r ? {W{1'b0}} : w ? cnt_ + 1'b1 : cnt_;
            
   initial cnt_ <= 0;
 
endmodule
