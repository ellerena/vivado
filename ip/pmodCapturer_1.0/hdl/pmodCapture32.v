`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/10/2018 12:34:15 PM
// Design Name: 
// Module Name: pmodCapture32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module pmodCapture32 #(parameter N=1)( input c, r, capture,
                        input [(N*32 - 1):0] pm, input[7:0] addR,
                        output [7:0] cnt, output [(N*32 - 1):0] o );
    wire w;
    reg cap_sync;
    
    always @ (posedge c)
        cap_sync <= capture;
    
    Level2Pulse #(1) _l2p(c, cap_sync, w);      // domain cross sync
    pmodCapture #(256,8,N) _pmc(c, r, w, pm, addR, cnt, o );

endmodule
